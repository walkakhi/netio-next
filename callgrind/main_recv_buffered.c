#include <stdio.h>
#include <time.h>

#include "netio/netio.h"
#include "felixtag.h"
#include <valgrind/callgrind.h>

//#define TEST_IT
#ifdef TEST_IT
# define DEBUG_LOG( ... ) do { printf("[recv@%s:%3d] ", __FILE__, __LINE__); printf(__VA_ARGS__); printf("\n"); fflush(stdout); } while(0)
#else
# define DEBUG_LOG( ... )
#endif

#define NUM_PAGES (256)
#define WATERMARK (63*1024)
#define PAGESIZE (64*1024)

struct {
  char* hostname;
  unsigned port;

  struct netio_context ctx;
  struct netio_buffered_listen_socket socket;
  struct netio_buffered_socket_attr attr;
} config;

struct {
    struct netio_timer timer;
    struct timespec t0;
    uint64_t messages_received;
    uint64_t bytes_received;
} statistics;

// Forward declarations
void on_connection_established(struct netio_buffered_recv_socket*);
void on_connection_closed(struct netio_buffered_recv_socket*);
void on_msg_received(struct netio_buffered_recv_socket*, void* data, size_t len);
void on_stats(void* ptr);

// Callbacks
void on_init(void *ptr)
{
  printf("on_init\n");
  config.attr.num_pages = NUM_PAGES;
  config.attr.pagesize = PAGESIZE;
  config.attr.watermark = WATERMARK;
  config.attr.timeout_ms = 1000;

  netio_buffered_listen_socket_init(&config.socket, &config.ctx, &config.attr);
  netio_buffered_listen(&config.socket, config.hostname, config.port);
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_msg_received = on_msg_received;
  clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
  statistics.timer.cb = on_stats;
  netio_timer_start_ms(&statistics.timer, 1000);
}

void on_connection_established(struct netio_buffered_recv_socket* socket)
{
  printf("on_connection_established\n");
  CALLGRIND_START_INSTRUMENTATION;
}

void on_connection_closed(struct netio_buffered_recv_socket* socket) {
  printf("on_connection_closed\n");
  netio_terminate(&config.ctx.evloop);
}

void on_msg_received(struct netio_buffered_recv_socket* socket, void* data, size_t len)
{
  statistics.messages_received++;
  statistics.bytes_received += len;
}

void on_stats(void* ptr)
{
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
  double seconds = t1.tv_sec - statistics.t0.tv_sec
                   + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
  printf("data rate: %2f Gb/s   message rate: %2f kHz\n",
         statistics.bytes_received*8/1024./1024./1024./seconds,
         statistics.messages_received/1000./seconds);
  statistics.bytes_received = 0;
  statistics.messages_received = 0;
  statistics.t0 = t1;
}

int main(int argc, char** argv)
{
  if(argc != 3) {
    fprintf(stderr, "Usage: %s <hostname> <port>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_timer_init(&config.ctx.evloop, &statistics.timer);
  netio_run(&config.ctx.evloop);
  CALLGRIND_STOP_INSTRUMENTATION;
  return 0;
}
