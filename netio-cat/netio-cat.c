#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
//#include <argp.h>
#include <getopt.h>
#include <stdarg.h>
#include <unistd.h>

#include "netio.h"

#define TRUE  1
#define FALSE 0

#define SUCCESSFUL_PARSING   200
#define FAILURE_PARSING      400

#define NETIO_CAT     "netio-cat"
#define CMD_RECV      "recv"
#define CMD_SEND      "send"
#define CMD_SUBSCRIBE "subscribe"
#define CMD_PUBLISH   "publish"

#define MAX_MSG_SIZE (1024)

//const char *argp_program_version = NETIO_CAT" 0.1";
//const char *argp_program_bug_address = "<https://its.cern.ch/jira/projects/FLXUSERS>";

/* Program documentation. */
static char doc[] = "The 'Swiss Army Knife' for NetIO communication.\n\n"
"Usage : ./netio-cat [COMMAND] [OPTIONS SEND RECV | OPTIONS PUBLISH SUBSCRIBE]\n\n"
"COMMAND can be any of '"CMD_SEND"', '"CMD_RECV"', '"CMD_PUBLISH"', or '"CMD_SUBSCRIBE"'.\n"
"'"CMD_SEND"' uses a buffered point-to-point connection to send data to a remote endpoint.\n"
"'"CMD_RECV"' uses a buffered point-to-point connection to receive data from a '"CMD_SEND"' command.\n"
"'"CMD_PUBLISH"' uses a buffered pub socket to publish data under a given tag.\n"
"'"CMD_SUBSCRIBE"' uses a buffered sub socket to subscribe to published data.\n\n"
"OPTIONS can be any of:\n"
"--hostname          -h  <IPv4>               \n\t\t  Listen on/connect to the given hostname. If this is not provided then send/recv/publish will use 127.0.0.1 by default \n\n"
"--remote-hostname   -r  <IPv4>               \n\t\t  Connect to server as client. If this is not provided then the sender/subscriber will use 127.0.0.1 by default \n\n"
"--port              -p  <port_number>        \n\t\t  Source port/destination port of the control socket. If this is not provided then the server/client will connect to 53100 by default \n\n"
"--tag               -t  <tag_number>         \n\t\t  The tag under which the server publishes data. If this is not provided then the server will use 1 by default \n\n"
"--first-tag         -f  <tag_number>         \n\t\t  The lower end of the publisher-specific tag range to which you can connect. If this is not provided then the subscriber will use  1 by default\n\n"
"--last-tag          -l  <tag_number>         \n\t\t  The higher end of the publisher-specific tag range to which you can connect. If this is not provided then the subscriber will use 3 by default\n\n"
"--delay             -d  <delay_ms>           \n\t\t  Time delay in ms between messages to be published. If this is not porvided then the publisher/subscriber will use 1ms by default\n\n"
"--expected-messages -e  <expected_messages>  \n\t\t  The number of messages expected by a specific subscriber (expected_messages>=0). If this is not provided the it will be use 0 by default\n\n"
"--verbose           -v                       \n\t\t  Enable verbose output. If it is not specified the program will use verbosity. \n\n"
"--help                                       \n\t\t  Display the usage of the tool\n\n"
;

/* The options we understand. */
static struct option long_options[] = {
	{"hostname",          required_argument,       0,  'h' },
	{"delay",             required_argument,       0,  'd' },
	{"port",              required_argument,       0,  'p' },
	{"verbose",           no_argument,             0,  'v' },
	{"remote-hostname",   required_argument,       0,  'r' },
        {"tag",               required_argument,       0,  't' },
	{"first-tag",         required_argument,       0,  'f' },
        {"last-tag",          required_argument,       0,  'l' },
	{"help",              no_argument,             0,   0  },
        {"expected-messages", required_argument,       0,  'e' },
	{0,                        0,                  0,   0  }
};

struct {
  struct netio_context ctx;
  struct netio_buffered_send_socket send_socket;
  struct netio_buffered_listen_socket listen_socket;
  struct netio_publish_socket pub_socket;
  struct netio_subscribe_socket sub_socket;
  struct netio_timer timer;
  struct netio_timer subscribe_timer;
  struct netio_buffered_socket_attr socket_attr;
  struct netio_event_context stdin_evctx;
} app;

struct {
  const char* command;
  const char* hostname;
  const char* remote_hostname;
  unsigned port;
  netio_tag_t tag;
  netio_tag_t first_tag;
  netio_tag_t  last_tag;
  enum { SEND, RECV, PUBLISH, SUBSCRIBE, NO_CMD } mode;
  int verbose;
  char* pubsub_data;
  size_t pubsub_datasize;
  int subrequest_sent;
  int use_fid;
  uint64_t expected_messages;
  unsigned delay;
} config;

struct {
    struct timespec t0;
    uint64_t messages_received;
    uint64_t bytes_received;
    int subscribed;
    uint64_t total_messages_received;
} statistics;

int is_unsigned_number(char* my_number)
{
  for(int i = 0; my_number[i]!='\0'; i++)
  	if(!isdigit(my_number[i])) return FALSE;
  return TRUE;
}

int is_valid_ip_address(char *ip_address)
{
    struct sockaddr_in sock_addr;
    int is_ip_address = inet_pton(AF_INET, ip_address, &(sock_addr.sin_addr));
    return is_ip_address != 0 ? TRUE : FALSE;
}

// LOGGING /////////////////////////////////////////////////////////////////////

void vlog(const char *format, ...)
{
  if(!config.verbose) {
    return;
  }

  va_list args;
  va_start(args, format);

  char msg[1024];
  vsnprintf(msg, 1024, format, args);
  printf("> %s\n", msg);

  va_end(args);
  fflush(stdout);
}

void fail(const char* format, ...) {
  va_list args;
  va_start(args, format);

  fprintf(stderr, NETIO_CAT": error: ");
  vfprintf(stderr, format, args);
  fprintf(stderr, "\n");
  exit(1);
}

// CLI /////////////////////////////////////////////////////////////////////////

/* Parse multiple options. */
unsigned parse_opt (int argc, char ** argv)
{
  char* end;
  if(argc < 2){
    vlog("'"NETIO_CAT"' NO ARGS RECEIVED\n");
    return FAILURE_PARSING;
  }

  config.command = argv[1];

  int c;
  int digit_optind = 0;

  while (TRUE) {
    int this_option_optind = optind ? optind : 1;
    int option_index = 0;

    c = getopt_long(argc, argv, "h:d:p:vr:t:f:l:e:", long_options, &option_index);
    if (c == -1)
      break;

    switch (c) {

      case 0:
            config.command = long_options[option_index].name;
            break;

      case 'h':
            if(is_valid_ip_address(optarg))
            {
              config.hostname = optarg;
            }
            else
            {
              fail("%s is not a valid IPv4 address", optarg);
              return FAILURE_PARSING;
            }
            break;

      case 'd':
            if(is_unsigned_number(optarg))
            {
              config.delay = strtol(optarg, &end, 0);
            }
            else
            {
              fail("%s is not a number or a valid number", optarg);
              return FAILURE_PARSING;
            }
            break;

      case 'r':
            if(is_valid_ip_address(optarg))
            {
              config.remote_hostname = optarg;
            }
            else
            {
              fail("%s is not a valid IPv4 address", optarg);
              return FAILURE_PARSING;
            }
            break;

      case 'p':
            if(is_unsigned_number(optarg))
            {
              config.port = strtol(optarg, &end, 0);
            }
            else
            {
              fail("%s is not a number or a valid number", optarg);
              return FAILURE_PARSING;
            }
            break;

      case 't':
            if(is_unsigned_number(optarg))
            {
              config.tag = strtol(optarg, &end, 0);
            }
            else
            {
              fail("%s is not a number or a valid number", optarg);
              return FAILURE_PARSING;
            }
            break;

      case 'f':
            if(is_unsigned_number(optarg))
            {
              config.first_tag = strtol(optarg, &end, 0);
            }
            else
            {
              fail("%s is not a number or a valid number", optarg);
              return FAILURE_PARSING;
            }
            break;

      case 'l':
            if(is_unsigned_number(optarg))
            {
              config.last_tag = strtol(optarg, &end, 0);
            }
            else
            {
              fail("%s is not a number or a valid number", optarg);
              return FAILURE_PARSING;
            }
            break;

      case 'e':
            if(is_unsigned_number(optarg))
            {
              config.expected_messages = strtol(optarg, &end, 0);
            }
            else
            {
              fail("%s is not a number or a valid number", optarg);
              return FAILURE_PARSING;
            }
            break;
        }
    }

   return SUCCESSFUL_PARSING;
}

void cli_parse(int argc, char **argv)
{
  // set default values
  config.hostname = "127.0.0.1";
  config.remote_hostname = "127.0.0.1";
  config.port = 53100;
  config.tag = 1;
  config.first_tag = 1;
  config.last_tag = 3;
  config.verbose = 1;
  config.delay = 1;
  config.use_fid = (config.first_tag & 0x1000000000000000) > 0L;
  config.expected_messages = 0;
  config.pubsub_datasize = 128;
  config.pubsub_data = (char*)malloc(config.pubsub_datasize);

  unsigned config_check = parse_opt(argc, argv);

  if(config_check == SUCCESSFUL_PARSING)
  {
    if(strcasecmp(config.command, CMD_SEND) == 0) {
      config.mode = SEND;
    } else if(strcasecmp(config.command, CMD_RECV) == 0) {
      config.mode = RECV;
    } else if(strcasecmp(config.command, CMD_PUBLISH) == 0) {
      config.mode = PUBLISH;
    } else if(strcasecmp(config.command, CMD_SUBSCRIBE) == 0) {
      config.mode = SUBSCRIBE;
    } else {
      config.mode = NO_CMD;
    }
  } else {
     fail("Usage should be '"CMD_SEND"', '"CMD_RECV"', '"CMD_PUBLISH"', or '"CMD_SUBSCRIBE"' [OPTIONS SEND RECV | OPTIONS PUBLISH SUBSCRIBE] \n");
     printf("Usage should be '"CMD_SEND"', '"CMD_RECV"', '"CMD_PUBLISH"', or '"CMD_SUBSCRIBE"' [OPTIONS SEND RECV | OPTIONS PUBLISH SUBSCRIBE] \n");
    }
}


// CALLBACKS ///////////////////////////////////////////////////////////////////

void on_init()
{
  vlog("initializing event loop");
  app.socket_attr.num_pages = 16;
  app.socket_attr.pagesize = 65536;
  app.socket_attr.watermark = app.socket_attr.pagesize - MAX_MSG_SIZE;
}


// BUFFERED SEND SOCKET ////////////////////////////////////////////////////////

void send_on_connection_established(struct netio_buffered_send_socket* socket)
{
  vlog("connection established");
}

void send_on_stdin(int fd, void* data)
{
  char buf[MAX_MSG_SIZE+1];
  ssize_t bytes = read(fd, buf, MAX_MSG_SIZE);
  if(bytes > 0) {
    if(buf[bytes-1]=='\n') { bytes--; }
    buf[bytes] = '\0';
    vlog("sending message of size %d: '%s'", bytes, buf);
    netio_buffered_send(&app.send_socket, buf, bytes);
    netio_buffered_flush(&app.send_socket);
  } else if(bytes < 0) {
    fail("could not read from stdin (%s)", strerror(errno));
  }
}

void send_on_init()
{
  on_init();
  vlog("initializing send socket");
  netio_buffered_send_socket_init(&app.send_socket, &app.ctx, &app.socket_attr);
  app.send_socket.cb_connection_established = send_on_connection_established;
  netio_buffered_connect(&app.send_socket, config.hostname, config.port);
  app.stdin_evctx.fd = STDIN_FILENO;
  app.stdin_evctx.cb = send_on_stdin;
  netio_register_read_fd(&app.ctx.evloop, &app.stdin_evctx);
}


// BUFFERED RECV SOCKET ////////////////////////////////////////////////////////

void recv_on_connection_established(struct netio_buffered_recv_socket* socket) {
  vlog("connection established");
}

void recv_on_connection_closed(struct netio_buffered_recv_socket* socket) {
  vlog("connection closed");
}

void recv_on_msg_received(struct netio_buffered_recv_socket* socket, void* data, size_t size) {
  printf(">>> msg received, size=%ld\n", size);
  for(size_t i=0; i<size; i++) {
    putchar(((char*)data)[i]);
  }
  puts("");
  fflush(stdout);
}

void recv_on_init() {
  on_init();
  vlog("initializing recv socket");
  netio_buffered_listen_socket_init(&app.listen_socket, &app.ctx, &app.socket_attr);
  app.listen_socket.cb_connection_established = recv_on_connection_established;
  app.listen_socket.cb_connection_closed = recv_on_connection_closed;
  app.listen_socket.cb_msg_received = recv_on_msg_received;
  netio_buffered_listen(&app.listen_socket, config.hostname, config.port);
}



// PUBLISH ////////////////////////////////////////////////////////////////////

void publish_on_subscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  vlog("remote subscribed to tag 0x%lx\n", tag);
}

void publish_connection_established(struct netio_publish_socket* socket)
{
  vlog("connection to subscriber established\n");
}

void publish_connection_closed(struct netio_publish_socket* socket)
{
  vlog("connection to subscriber closed\n");
}

void publish_timer(void* ptr)
{
  netio_buffered_publish(&app.pub_socket, config.tag, config.pubsub_data, config.pubsub_datasize, 0, NULL);
  netio_buffered_publish_flush(&app.pub_socket, 0, NULL);
}

void publish_on_init()
{
  on_init();
  vlog("Opening publish socket on %s:%d\n", config.hostname, config.port);
  printf("Opening publish socket on %s:%d\n", config.hostname, config.port);
  netio_publish_socket_init(&app.pub_socket, &app.ctx, config.hostname, config.port, &app.socket_attr);
  app.pub_socket.cb_subscribe = publish_on_subscribe;
  app.pub_socket.cb_connection_established = publish_connection_established;
  app.pub_socket.cb_connection_closed = publish_connection_closed;
  for(unsigned i = 0; i < config.pubsub_datasize; i++)
  {
     config.pubsub_data[i] = 'x';
  }
  vlog("Starting to publish every %d milliseconds\n", config.delay);
  netio_timer_start_ms(&app.timer, config.delay);
}

// SUBSCRIBE //////////////////////////////////////////////////////////////////////

void subscribe_connection_closed(struct netio_subscribe_socket* socket)
{
  vlog("connection to publisher closed from %s:%d\n", socket->remote_hostname, socket->remote_port);
  statistics.subscribed = 0;
  config.subrequest_sent = 0;
  // try to reconnect periodically
  netio_timer_start_s(&app.subscribe_timer, 1);
}

void subscribe_connection_established(struct netio_subscribe_socket* socket) {
  vlog("connection established to %s:%d\n", socket->remote_hostname, socket->remote_port);
  statistics.subscribed = 1;
  config.subrequest_sent = 0;
  netio_timer_stop(&app.subscribe_timer);
}

void subscribe_error_connection_refused(struct netio_subscribe_socket* socket)
{
  vlog("connection refused for subscribing from %s:%d\n", socket->remote_hostname, socket->remote_port);
  config.subrequest_sent = 0;
  // retry to connect periodically
  netio_timer_start_s(&app.subscribe_timer, 1);
}

void subscribe_on_stats(void* ptr) {
  if (!statistics.subscribed) return;
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
  double seconds = t1.tv_sec - statistics.t0.tv_sec
                   + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
  printf("data rate: %2f Gb/s   message rate: %2f kHz\n",
         statistics.bytes_received*8/1024./1024./1024./seconds,
         statistics.messages_received/1000./seconds);
  statistics.bytes_received = 0;
  statistics.messages_received = 0;
  statistics.t0 = t1;
}

void subscribe_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  /*// printf("msg tag=%lu size=%lu\n", tag, size);
  printf(">>> msg received, size=%ld\n", size);
  for(size_t i=0; i<size; i++) {
    putchar(((char*)data)[i]);
  }
  puts("");
  fflush(stdout);
  */
  statistics.total_messages_received++;
  if ((statistics.total_messages_received > config.expected_messages) && (config.expected_messages > 0)) {
    printf("Expected messages received\n");
    netio_terminate(&app.ctx.evloop);
    exit(42);
  }

  statistics.messages_received++;
  statistics.bytes_received += size;
}

void attempt_subscribe()
{
  if (!config.subrequest_sent) {
    vlog("attempting to subscribe to remote on %s:%d\n", app.sub_socket.remote_hostname, app.sub_socket.remote_port);
    for(netio_tag_t tag = config.first_tag; tag <= config.last_tag; tag += config.use_fid ? 0x10000 : 1) {
      vlog("subscribing to tag 0x%lx\n", tag);
      netio_subscribe(&app.sub_socket, tag);
      config.subrequest_sent = 1;
    }
  }
}

void subscribe_on_init()
{
  struct netio_buffered_socket_attr attr;
  on_init();
  printf("Opening subscribe socket from %s on %s:%d\n", config.hostname, config.remote_hostname, config.port);
  netio_subscribe_socket_init(&app.sub_socket, &app.ctx, &attr, config.hostname, config.remote_hostname, config.port);
  app.sub_socket.cb_connection_closed = subscribe_connection_closed;
  app.sub_socket.cb_connection_established = subscribe_connection_established;
  app.sub_socket.cb_error_connection_refused = subscribe_error_connection_refused;
  app.sub_socket.cb_msg_received = subscribe_msg_received;

  netio_timer_init(&app.ctx.evloop, &app.timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
  app.timer.cb = subscribe_on_stats;
  netio_timer_start_s(&app.timer, config.delay);

  netio_timer_init(&app.ctx.evloop, &app.subscribe_timer);
  app.subscribe_timer.cb = attempt_subscribe;
  // attempt to subscribe
  attempt_subscribe();
}
// MAIN ////////////////////////////////////////////////////////////////////////

int main(int argc, char** argv)
{
  cli_parse(argc, argv);

  netio_init(&app.ctx);

  switch(config.mode) {
    case SEND:
      app.ctx.evloop.cb_init = send_on_init;
      break;
    case RECV:
      app.ctx.evloop.cb_init = recv_on_init;
      break;
    case PUBLISH:
      app.ctx.evloop.cb_init = publish_on_init;
      netio_timer_init(&app.ctx.evloop, &app.timer);
      app.timer.cb = publish_timer;
      break;
    case SUBSCRIBE:
      app.ctx.evloop.cb_init = subscribe_on_init;
      break;
    case NO_CMD:
      printf("%s", doc);
      return 0;
    default:
      fail("command '%s' not yet implemented", config.command);
  }

  netio_run(&app.ctx.evloop);
  return 0;
}
