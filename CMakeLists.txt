cmake_minimum_required(VERSION 3.4.3)

set(PACKAGE netio-next)
set(PACKAGE_VERSION 0.0.1)

include(FELIX OPTIONAL RESULT_VARIABLE FELIX_BUILD)

include_directories(netio)

#python
#find_package(PythonLibs ${pythonlibs_version} EXACT REQUIRED)
include_directories(${PYTHON_INCLUDE_DIR})

if(FELIX_BUILD)
    felix_add_external(libfabric ${libfabric_version} ${BINARY_TAG})
    felix_add_external(logrxi ${logrxi_version} ${BINARY_TAG})
    felix_add_external(catch ${catch_version} ${BINARY_TAG})
endif(FELIX_BUILD)

add_definitions(-O3 -Wall -Wno-pedantic -Wno-unused-variable -DDEFAULT_DEBUG_LEVEL=3)

#Debug logging: global -DDEBUG or file-specific e.g. -DDEBUG_IO, -DDEBUG_EV, -DDEBUG_CM...
#add_definitions(-DDEBUG_CM -DLOG_USE_COLOR)

option(ASANCHECK "Add Address Sanitizer checks" OFF)
if(ASANCHECK)
    message("--   netio-next: compiling with Address Sanitizer")
    add_definitions(-fno-omit-frame-pointer -fsanitize=address,undefined,leak)
    add_link_options(-fsanitize=address,undefined,leak)
endif(ASANCHECK)
unset(ASANCHECK CACHE)

if(NOT FELIX_BUILD)
    set(NETIO_SOURCES src/*.c)
    tdaq_add_library(netio-lib ${NETIO_SOURCES})
    target_compile_options(netio-lib PRIVATE ${FELIX_COVERAGE_OPTS})
    target_link_libraries(netio-lib fabric m ${FELIX_COVERAGE_LIBS})
endif(NOT FELIX_BUILD)

if(FELIX_BUILD)
#
# Library
#
set(NETIO_SOURCES src/*.c)
tdaq_add_library(netio-lib NOINSTALL ${NETIO_SOURCES})
target_compile_options(netio-lib PRIVATE ${FELIX_COVERAGE_OPTS})
target_link_libraries(netio-lib fabric m ${FELIX_COVERAGE_LIBS})


#
# Netio-tcp prototyping
#
tdaq_add_executable(tcp_client_epoll examples/tcp_client_epoll.c)
target_link_libraries(tcp_client_epoll)

tdaq_add_executable(tcp_server_epoll examples/tcp_server_epoll.c)
target_link_libraries(tcp_server_epoll)

tdaq_add_executable(tcp_generic examples/tcp_generic.c)
target_link_libraries(tcp_generic)

#
# Examples
#
option(CALLGRIND "Compile examples for Callgrind analysis" OFF)

if(CALLGRIND)
    message("--   netio-next: compiling examples for Callgrind analysis")
    include_directories(/cvmfs/sft.cern.ch/lcg/releases/LCG_101/valgrind/3.16.1/x86_64-centos7-gcc11-opt/include)

    tdaq_add_executable(send callgrind/main_send.c ${NETIO_SOURCES})
    target_link_libraries(send felix-tag netio-lib)

    tdaq_add_executable(recv callgrind/main_recv.c ${NETIO_SOURCES})
    target_link_libraries(recv felix-tag netio-lib rt)

    tdaq_add_executable(send-buffered callgrind/main_send_buffered.c ${NETIO_SOURCES})
    target_link_libraries(send-buffered felix-tag netio-lib)

    tdaq_add_executable(recv-buffered callgrind/main_recv_buffered.c ${NETIO_SOURCES})
    target_link_libraries(recv-buffered felix-tag netio-lib rt)

    tdaq_add_executable(publish callgrind/main_publish.c ${NETIO_SOURCES})
    target_link_libraries(publish felix-tag netio-lib)

    tdaq_add_executable(subscribe callgrind/main_subscribe.c ${NETIO_SOURCES})
    target_link_libraries(subscribe felix-tag netio-lib)

    tdaq_add_executable(send-buffered examples/main_send_buffered.c)
    target_link_libraries(send-buffered felix-tag netio-lib)

    tdaq_add_executable(unbuf_publish callgrind/main_unbuffered_publish.c ${NETIO_SOURCES})
    target_link_libraries(unbuf_publish felix-tag netio-lib)

    tdaq_add_executable(unbuf_subscribe callgrind/main_unbuffered_subscribe.c ${NETIO_SOURCES})
    target_link_libraries(unbuf_subscribe felix-tag netio-lib)
else()
    tdaq_add_executable(send examples/main_send.c)
    target_link_libraries(send felix-tag netio-lib)

    tdaq_add_executable(recv examples/main_recv.c)
    target_link_libraries(recv felix-tag netio-lib rt)

    tdaq_add_executable(send-buffered examples/main_send_buffered.c)
    target_link_libraries(send-buffered felix-tag netio-lib)

    tdaq_add_executable(recv-buffered examples/main_recv_buffered.c)
    target_link_libraries(recv-buffered felix-tag netio-lib rt)

    tdaq_add_executable(publish examples/main_publish.c)
    target_link_libraries(publish felix-tag netio-lib)

    tdaq_add_executable(subscribe examples/main_subscribe.c)
    target_link_libraries(subscribe felix-tag netio-lib)

    tdaq_add_executable(unbuf_publish examples/main_unbuffered_publish.c)
    target_link_libraries(unbuf_publish felix-tag netio-lib)

    tdaq_add_executable(unbuf_subscribe examples/main_unbuffered_subscribe.c)
    target_link_libraries(unbuf_subscribe felix-tag netio-lib)

    tdaq_add_executable(send_tcp examples/main_send_tcp.c)
    target_link_libraries(send_tcp netio-lib)

    tdaq_add_executable(recv_tcp examples/main_recv_tcp.c)
    target_link_libraries(recv_tcp netio-lib rt)

    tdaq_add_executable(sendfast_tcp examples/main_sendfast_tcp.c)
    target_link_libraries(sendfast_tcp netio-lib)

    tdaq_add_executable(recvfast_tcp examples/main_recvfast_tcp.c)
    target_link_libraries(recvfast_tcp netio-lib rt)

    tdaq_add_executable(test_tcp examples/main_test_tcp.c)
    target_link_libraries(test_tcp netio-lib)

endif(CALLGRIND)
unset(CALLGRIND CACHE)

tdaq_add_executable(examples-helloworld examples/main_helloworld.c)
target_link_libraries(examples-helloworld felix-tag netio-lib)

tdaq_add_executable(examples-signal examples/main_signal.c)
target_link_libraries(examples-signal felix-tag netio-lib pthread)

tdaq_add_executable(examples-timer examples/main_timer.c)
target_link_libraries(examples-timer felix-tag netio-lib)

tdaq_add_executable(dual_publish examples/main_dual_publish.c)
target_link_libraries(dual_publish felix-tag netio-lib)

tdaq_add_executable(pubflood examples/main_pubflood.c)
target_link_libraries(pubflood felix-tag netio-lib)

tdaq_add_executable(pubfloodmt examples/main_pubflood_mt.c)
target_link_libraries(pubfloodmt felix-tag netio-lib pthread)

tdaq_add_executable(unbuf_pubflood examples/main_unbuffered_pubflood.c)
target_link_libraries(unbuf_pubflood felix-tag netio-lib)

tdaq_add_executable(pileup examples/main_pileup.c)
target_link_libraries(pileup felix-tag netio-lib)

tdaq_add_executable(subscribe_unsubscribe examples/main_subscribe_unsubscribe.c)
target_link_libraries(subscribe_unsubscribe felix-tag netio-lib)

tdaq_add_executable(unbuf_subscribe_unsubscribe examples/main_unbuf_subscribe_unsubscribe.c)
target_link_libraries(unbuf_subscribe_unsubscribe felix-tag netio-lib)

tdaq_add_executable(subscribemt examples/main_subscribe_mt.c)
target_link_libraries(subscribemt felix-tag netio-lib pthread)

#tdaq_add_executable(unbuf_pubfloodmt examples/main_unbuffered_pubflood_mt.c)
#target_link_libraries(unbuf_pubfloodmt felix-tag netio-lib pthread)

#tdaq_add_executable(unbuf_subscribemt examples/main_unbuffered_subscribe_mt.c)
#target_link_libraries(unbuf_subscribemt felix-tag netio-lib pthread)

tdaq_add_executable(file_inotify examples/main_file_inotify.c)
target_link_libraries(file_inotify felix-tag netio-lib)

tdaq_add_executable(loopback_buffered examples/main_loopback_buffered.c)
target_link_libraries(loopback_buffered felix-tag netio-lib)

tdaq_add_executable(loopback_unbuffered examples/main_loopback_unbuffered.c)
target_link_libraries(loopback_unbuffered felix-tag netio-lib)

tdaq_add_executable(subscribe_send examples/main_subscribe_send.c)
target_link_libraries(subscribe_send felix-tag netio-lib pthread)

tdaq_add_executable(unbuf_subscribe_send examples/main_unbuf_subscribe_send.c)
target_link_libraries(unbuf_subscribe_send felix-tag netio-lib pthread)

tdaq_add_executable(subscribe_unsubscribe_loop examples/main_subscribe_unsubscribe_loop.c)
target_include_directories(subscribe_unsubscribe_loop PRIVATE src)
target_link_libraries(subscribe_unsubscribe_loop felix-tag netio-lib pthread)

tdaq_add_executable(unbuf_subscribe_unsubscribe_loop examples/main_unbuf_subscribe_unsubscribe_loop.c)
target_include_directories(unbuf_subscribe_unsubscribe_loop PRIVATE src)
target_link_libraries(unbuf_subscribe_unsubscribe_loop felix-tag netio-lib pthread)

tdaq_add_executable(send_loop_buffered examples/main_send_loop_buffered.c)
target_include_directories(send_loop_buffered PRIVATE src)
target_link_libraries(send_loop_buffered felix-tag netio-lib pthread)


#
# Benchmarks
#
tdaq_add_executable(bw_buffered examples/main_bw_buffered.c)
target_link_libraries(bw_buffered netio-lib rt)

tdaq_add_executable(bw_unbuffered examples/main_bw_unbuffered.c)
target_link_libraries(bw_unbuffered netio-lib rt)

tdaq_add_executable(latency examples/main_latency.c)
target_link_libraries(latency netio-lib felix-tag  rt)

#
# Tools (copied to bin)
#
tdaq_add_executable(netio-cat netio-cat/netio-cat.c)
target_link_libraries(netio-cat netio-lib rt)

#
# Unit Tests
#
file(GLOB NETIO_TEST_SOURCES unit_tests/*.cpp)
tdaq_add_executable(netio-next-unit-tests ${NETIO_TEST_SOURCES})
target_include_directories(netio-next-unit-tests PRIVATE src)
target_link_libraries(netio-next-unit-tests Catch2Main Catch2 netio-lib)

tdaq_add_executable(netio-next-unit-hostname unit_tests/test_hostname.cpp)
target_include_directories(netio-next-unit-hostname PRIVATE src)
target_link_libraries(netio-next-unit-hostname Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-hostname netio-next-unit-hostname)

tdaq_add_executable(netio-next-unit-eventloop unit_tests/test_eventloop.cpp)
target_include_directories(netio-next-unit-eventloop PRIVATE src)
target_link_libraries(netio-next-unit-eventloop Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-eventloop netio-next-unit-eventloop)

tdaq_add_executable(netio-next-unit-buf unit_tests/test_buf.cpp)
target_include_directories(netio-next-unit-buf PRIVATE src)
target_link_libraries(netio-next-unit-buf Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-buf netio-next-unit-buf)

tdaq_add_executable(netio-next-unit-pubsub unit_tests/test_buf_pubsub_simple.cpp)
target_include_directories(netio-next-unit-pubsub PRIVATE src)
target_link_libraries(netio-next-unit-pubsub Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-pubsub netio-next-unit-pubsub)

tdaq_add_executable(netio-next-unit-semaphore unit_tests/test_semaphore.cpp)
target_include_directories(netio-next-unit-semaphore PRIVATE src)
target_link_libraries(netio-next-unit-semaphore Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-semaphore netio-next-unit-semaphore)

tdaq_add_executable(netio-next-unit-unbuf unit_tests/test_unbuf.cpp)
target_include_directories(netio-next-unit-unbuf PRIVATE src)
target_link_libraries(netio-next-unit-unbuf Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-unbuf netio-next-unit-unbuf)

tdaq_add_executable(netio-next-unit-pubsub-unbuf-simple src/log.c unit_tests/test_unbuf_pubsub_simple.cpp)
target_include_directories(netio-next-unit-pubsub-unbuf-simple PRIVATE src)
target_link_libraries(netio-next-unit-pubsub-unbuf-simple Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-pubsub-unbuf-simple netio-next-unit-pubsub-unbuf-simple)

tdaq_add_executable(netio-next-unit-pubsub-unbuf-flood unit_tests/test_unbuf_pubsub_flood.cpp)
target_include_directories(netio-next-unit-pubsub-unbuf-flood PRIVATE src)
target_link_libraries(netio-next-unit-pubsub-unbuf-flood Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-pubsub-unbuf-flood netio-next-unit-pubsub-unbuf-flood)

tdaq_add_executable(netio-next-unit-b-shutdown unit_tests/test_buf_shutdown.cpp)
target_include_directories(netio-next-unit-b-shutdown PRIVATE src)
target_link_libraries(netio-next-unit-b-shutdown Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-b-shutdown netio-next-unit-b-shutdown)

tdaq_add_executable(netio-next-unit-u-shutdown unit_tests/test_unbuf_shutdown.cpp)
target_include_directories(netio-next-unit-u-shutdown PRIVATE src)
target_link_libraries(netio-next-unit-u-shutdown Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-u-shutdown netio-next-unit-u-shutdown)

tdaq_add_executable(netio-next-unit-u-sub-list unit_tests/test_subscription_list.cpp src/subscription_list.c)
target_include_directories(netio-next-unit-u-sub-list PRIVATE src)
target_link_libraries(netio-next-unit-u-sub-list Catch2Main Catch2)
add_test(netio-next-unit-u-sub-list netio-next-unit-u-sub-list)

tdaq_add_executable(netio-next-unit-polled-fids unit_tests/test_polled_fids.cpp)
target_include_directories(netio-next-unit-polled-fids PRIVATE src)
target_link_libraries(netio-next-unit-polled-fids Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-polled-fids netio-next-unit-polled-fids)

tdaq_add_executable(netio-next-unit-socket-list unit_tests/test_socket_list.cpp src/socket_list.c)
target_include_directories(netio-next-unit-socket-list PRIVATE src)
target_link_libraries(netio-next-unit-socket-list Catch2Main Catch2 netio-lib)
add_test(netio-next-unit-socket-list netio-next-unit-socket-list)

#
# Other Tests
#
add_test(netio-next-publish-buffered test/publish_buffered/test_publish_buffered.py)
add_test(netio-next-publish-buffered-timeout test/publish_buffered_timeout/test_publish_buffered_timeout.py)
add_test(netio-next-pubflood-buffered test/pubflood_buffered/test_pubflood_buffered.py)
add_test(netio-next-pubflood-multi-buffered test/pubflood_multi_buffered/test_pubflood_multi_buffered.py)
add_test(netio-next-pubfloodmt-buffered test/pubfloodmt_buffered/test_pubfloodmt_buffered.py)
add_test(netio-next-publish-unbuffered test/publish_unbuffered/test_publish_unbuffered.py)
add_test(netio-next-unsubscribe-buffered test/buffered_unsubscribe/test_buffered_unsubscribe.py)
add_test(netio-next-unsubscribe-unbuffered test/unbuffered_unsubscribe/test_unbuffered_unsubscribe.py)

add_test(netio-next-send-receive-unbuffered test/send_receive_unbuffered/test_send_receive_unbuffered.py)
add_test(netio-next-send-receive-buffered test/send_receive_buffered/test_send_receive_buffered.py)
add_test(netio-next-loopback-unbuffered test/loopback_unbuffered/test_loopback_unbuffered.py)
add_test(netio-next-loopback-buffered test/loopback_buffered/test_loopback_buffered.py)

#add_test(netio-next-fds-send-receive-unbuffered test/fds/test_fds_send_receive_unbuffered.py)
#add_test(netio-next-fds-send-receive-buffered test/fds/test_fds_send_receive_buffered.py)
add_test(netio-next-fds-publish-subscribe-unbuffered test/fds/test_fds_publish_subscribe_unbuffered.py)
add_test(netio-next-fds-publish-subscribe-buffered test/fds/test_fds_publish_subscribe_buffered.py)


#
# TCP/IP
#
add_test(netio-next-publish-buffered-tcp test/publish_buffered/test_publish_buffered.py)
set_property(TEST netio-next-publish-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-publish-buffered-timeout-tcp test/publish_buffered_timeout/test_publish_buffered_timeout.py)
set_property(TEST netio-next-publish-buffered-timeout-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-pubflood-buffered-tcp test/pubflood_buffered/test_pubflood_buffered.py)
set_property(TEST netio-next-pubflood-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-pubflood-multi-buffered-tcp test/pubflood_multi_buffered/test_pubflood_multi_buffered.py)
set_property(TEST netio-next-pubflood-multi-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-pubfloodmt-buffered-tcp test/pubfloodmt_buffered/test_pubfloodmt_buffered.py)
set_property(TEST netio-next-pubfloodmt-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-publish-unbuffered-tcp test/publish_unbuffered/test_publish_unbuffered.py)
set_property(TEST netio-next-publish-unbuffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-unsubscribe-buffered-tcp test/buffered_unsubscribe/test_buffered_unsubscribe.py)
set_property(TEST netio-next-unsubscribe-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-unsubscribe-unbuffered-tcp test/unbuffered_unsubscribe/test_unbuffered_unsubscribe.py)
set_property(TEST netio-next-unsubscribe-unbuffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-send-receive-unbuffered-tcp test/send_receive_unbuffered/test_send_receive_unbuffered.py)
set_property(TEST netio-next-send-receive-unbuffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-send-receive-buffered-tcp test/send_receive_buffered/test_send_receive_buffered.py)
set_property(TEST netio-next-send-receive-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-loopback-unbuffered-tcp test/loopback_unbuffered/test_loopback_unbuffered.py)
set_property(TEST netio-next-loopback-unbuffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-loopback-buffered-tcp test/loopback_buffered/test_loopback_buffered.py)
set_property(TEST netio-next-loopback-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")

#add_test(netio-next-fds-send-receive-unbuffered-tcp test/fds/test_fds_send_receive_unbuffered.py)
#set_property(TEST netio-next-fds-send-receive-unbuffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
#add_test(netio-next-fds-send-receive-buffered-tcp test/fds/test_fds_send_receive_buffered.py)
#set_property(TEST netio-next-fds-send-receive-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-fds-publish-subscribe-unbuffered-tcp test/fds/test_fds_publish_subscribe_unbuffered.py)
set_property(TEST netio-next-fds-publish-subscribe-unbuffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")
add_test(netio-next-fds-publish-subscribe-buffered-tcp test/fds/test_fds_publish_subscribe_buffered.py)
set_property(TEST netio-next-fds-publish-subscribe-buffered-tcp PROPERTY ENVIRONMENT "NETIO_PROTOCOL=tcp")

# non-existing
#add_test(netio-next-pubflood-unbuffered test/pubflood_unbuffered/test_pubflood_unbuffered.py)
#add_test(netio-next-pubflood-unbuffered-tcp test/pubflood_unbuffered/test_pubflood_unbuffered_tcp.py)
#add_test(netio-next-pubflood-multi-unbuffered test/pubflood_multi_unbuffered/test_pubflood_multi_unbuffered.py)
#add_test(netio-next-pubfloodmt-unbuffered test/pubfloodmt_unbuffered/test_pubfloodmt_unbuffered.py)

#
# Python binding, pre-generated CFFI using src/netio_py_build.py
#
tdaq_add_library(netio_py NOINSTALL ${CMAKE_CURRENT_BINARY_DIR}/netio_py.c)
set_source_files_properties(${CMAKE_CURRENT_BINARY_DIR}/netio_py.c PROPERTIES COMPILE_FLAGS -Wno-address-of-packed-member)
set_target_properties(netio_py PROPERTIES PREFIX "" OUTPUT_NAME "netio_py" LINKER_LANGUAGE C)
target_link_libraries(netio_py netio-lib rt)

add_custom_target(netio-include ALL
    COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/netio ${CMAKE_CURRENT_BINARY_DIR}/include/netio)
add_custom_target(netio-python ALL
    COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/python ${CMAKE_CURRENT_BINARY_DIR})
add_custom_target(netio-scripts ALL
    COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/scripts ${CMAKE_CURRENT_BINARY_DIR})
add_custom_target(netio-tests ALL
    COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/test ${CMAKE_CURRENT_BINARY_DIR}/test)

add_custom_command(
    OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/netio_py.c
    COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/src/netio_py_build.sh
    DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/src/netio_py_prefix.txt ${CMAKE_CURRENT_SOURCE_DIR}/src/netio_py_postfix.txt ${CMAKE_CURRENT_SOURCE_DIR}/src/netio_py_filter.sed
)

install(TARGETS netio-lib bw_buffered
    RUNTIME DESTINATION bin
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib)
install(FILES netio/netio.h netio/netio_tcp.h DESTINATION include)

endif(FELIX_BUILD)
