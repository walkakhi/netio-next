#!/usr/bin/env python3
"""
Netio File (Inotify).

Usage:
    file.py
"""
import sys

from docopt import docopt  # noqa: E402

from inotify_simple import INotify, flags, masks

from netio_py import ffi, lib

from felixtag import FELIX_TAG


@ffi.def_extern()
def on_init():
    """On Init."""
    print("on_init")


@ffi.def_extern()
def on_file(fd, data):
    """On Timer."""
    print("on_file", fd, data)
    for event in inotify.read():
        print(event)
        for flag in flags.from_mask(event.mask):
            print('    ' + str(flag))


if __name__ == "__main__":
    """File."""
    args = docopt(__doc__, version=sys.argv[0] + " " + FELIX_TAG)

    print("netio_context")
    context = ffi.new("struct netio_context *")

    print("netio_init")
    lib.netio_init(context)

    print("netio_eventloop and cb")
    evloop = ffi.addressof(context.evloop)
    evloop.cb_init = lib.on_init

    print("inotify setup")
    inotify = INotify()
    # watch_flags = flags.CREATE | flags.DELETE | flags.MODIFY | flags.DELETE_SELF | flags.CLOSE_WRITE | flags.CLOSE_NOWRITE
    watch_flags = masks.ALL_EVENTS
    wd = inotify.add_watch('.', watch_flags)

    use_netio = True
    if use_netio:
        print("netio_register_read and cb")
        event_context = ffi.new("struct netio_event_context *")
        event_context.fd = inotify.fileno()
        lib.netio_register_read_fd(evloop, event_context)
        event_context.cb = lib.on_file

        print("netio_run")
        lib.netio_run(evloop)
    else:
        while True:
            for event in inotify.read():
                print(event)
                for flag in flags.from_mask(event.mask):
                    print('    ' + str(flag))

    print("Never Ends")
