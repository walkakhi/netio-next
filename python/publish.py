#!/usr/bin/env python3
"""
Netio Publish.

Usage:
    publish.py <hostname> <port> <tag>

Arguments:
    <hostname>        Internet hostname to publish from
    <port>            Port number to use
    <tag>             Tag to publish under
"""
import sys

from docopt import docopt  # noqa: E402

from netio_py import ffi, lib

from felixtag import FELIX_TAG


@ffi.def_extern()
def on_publish_subscribe(socket, tag, addr, addrlen):
    """On Subscribe."""
    print("on_subscribe")


@ffi.def_extern()
def on_publish_connection_established(socket):
    """On Connection Established."""
    print("connection to subscriber established")


@ffi.def_extern()
def on_publish_connection_closed(socket):
    """On Connection Closed."""
    print("connection to subscriber closed")


@ffi.def_extern()
def on_publish_buffer_available(socket):
    """On Buffer Available."""
    print("on_buffer_available")


@ffi.def_extern()
def on_init():
    """On Init."""
    print("on_init")
    global context, data, socket, timer

    print("netio_buffered_socket_attr")
    attr = ffi.new("struct netio_buffered_socket_attr *")
    attr.num_pages = 16
    attr.pagesize = 64 * 1024
    attr.watermark = 63 * 1024

    print("netio_publish_socket")
    socket = ffi.new("struct netio_publish_socket *")

    print("netio_publish_socket_init")
    lib.netio_publish_socket_init(socket, context, hostname, port, attr)
    socket.cb_subscribe = lib.on_publish_subscribe
    socket.cb_connection_established = lib.on_publish_connection_established
    socket.cb_connection_closed = lib.on_publish_connection_closed
    socket.cb_buffer_available = lib.on_publish_buffer_available

    data = "foo.bar.baz*****"

    timer.cb = lib.on_timer
    lib.netio_timer_start_ms(timer, 10000)


@ffi.def_extern()
def on_timer(ptr):
    """On Timer."""
    print("on_timer")
    global data, socket

    print("netio_buffered_publish")
    lib.netio_buffered_publish(socket, tag, ffi.new("char[]", data), len(data), 0, ffi.NULL)

    print("netio_buffered_publish_flush")
    lib.netio_buffered_publish_flush(socket, tag, ffi.NULL)


if __name__ == "__main__":
    """Publish."""
    global context, timer

    args = docopt(__doc__, version=sys.argv[0] + " " + FELIX_TAG)

    hostname = args['<hostname>']
    port = int(args['<port>'])
    tag = int(args['<tag>'])

    print("netio_context")
    context = ffi.new("struct netio_context *")

    print("netio_init")
    lib.netio_init(context)

    print("netio_eventloop and cb")
    evloop = ffi.addressof(context.evloop)
    evloop.cb_init = lib.on_init

    print("netio_timer")
    timer = ffi.new("struct netio_timer *")

    print("netio_timer_init")
    lib.netio_timer_init(evloop, timer)

    print("netio_run")
    lib.netio_run(evloop)

    print("Never Ends")
