#!/usr/bin/env python3
"""
Netio Subscribe.

Usage:
    subscribe.py <local_hostname> <remote_hostname> <port> <tag>

Arguments:
    <local_hostname>    Internet hostname to subscribe from
    <remote_hostname>   Internet hostname to subscribe to
    <port>              Port number to use
    <tag>               Tag to subscribe to
"""
import sys

from docopt import docopt  # noqa: E402

from netio_py import ffi, lib

from felixtag import FELIX_TAG


@ffi.def_extern()
def on_subscribe_connection_closed(socket):
    """On Connection Closed."""
    global timer
    print("connection to publisher closed")
    lib.netio_timer_start_s(timer, 1)


@ffi.def_extern()
def on_subscribe_connection_established(socket):
    """On Connection Established."""
    global timer
    print("connection from remote")
    lib.netio_timer_stop(timer)


@ffi.def_extern()
def on_subscribe_error_connection_refused(socket):
    """On Error Connectiorn Refused."""
    print("connection refused for subscribing")


@ffi.def_extern()
def on_subscribe_msg_received(socket, tag, data, size):
    """On Msg Received."""
    print("on_msg_received")
    print("msg tag=", tag, "size=", size)


@ffi.def_extern()
def on_timer(ptr):
    """On Timer."""
    global socket
    print("on_timer")
    print("attempting to subscribe to remote")
    lib.netio_subscribe(socket, tag)


@ffi.def_extern()
def on_init():
    """On Init."""
    global evloop, socket, timer
    print("on_init")

    print("netio_buffered_socket_attr")
    attr = ffi.new("struct netio_buffered_socket_attr *")
    attr.num_pages = 16
    attr.pagesize = 64 * 1024
    attr.watermark = 63 * 1024

    print("netio_subscribe_socket")
    socket = ffi.new("struct netio_subscribe_socket *")

    print("netio_subscribe_socket_init")
    lib.netio_subscribe_socket_init(socket, context, attr, local_hostname, remote_hostname, port)
    socket.cb_connection_closed = lib.on_subscribe_connection_closed
    socket.cb_connection_established = lib.on_subscribe_connection_established
    socket.cb_error_connection_refused = lib.on_subscribe_error_connection_refused
    socket.cb_msg_received = lib.on_subscribe_msg_received

    print("netio_timer")
    timer = ffi.new("struct netio_timer *")

    print("netio_timer_init")
    lib.netio_timer_init(evloop, timer)
    timer.cb = lib.on_timer
    lib.netio_timer_start_s(timer, 1)


if __name__ == "__main__":
    """Subscribe."""
    global context, evloop
    args = docopt(__doc__, version=sys.argv[0] + " " + FELIX_TAG)

    local_hostname = args['<local_hostname>']
    remote_hostname = args['<remote_hostname>']
    port = int(args['<port>'])
    tag = int(args['<tag>'])

    print("netio_context")
    context = ffi.new("struct netio_context *")

    print("netio_init")
    lib.netio_init(context)

    print("netio_eventloop and cb")
    evloop = ffi.addressof(context.evloop)
    evloop.cb_init = lib.on_init

    print("netio_run")
    lib.netio_run(evloop)

    print("Never Ends")
