image: gitlab-registry.cern.ch/atlas-tdaq-felix/gitlab_ci_runner:cc7-netboot-dev

variables:
  PROJECT_NAME: netio-next
  GIT_STRATEGY: clone
  GIT_SUBMODULE_STRATEGY: normal
  GIT_DEPTH: 0

  SSH_OPTIONS: -o StrictHostKeyChecking=no -o GSSAPIAuthentication=yes -o GSSAPIDelegateCredentials=yes
  LOGIN: ${KRB_USERNAME}@lxplus020.cern.ch
  DEV_DST: /eos/project/f/felix/www/dev/dist/software/apps/${CI_BUILD_REF_NAME}
  USER_DST: /eos/project/f/felix/www/user/dist/software/apps/${CI_BUILD_REF_NAME}
  PROD_DST: /eos/project/f/felix/www/user/dist/software/apps
  EOSPROJECT: root://eosproject.cern.ch
  STAGING: /eos/project/f/felix/www/dev/staging/www/netio
  DOCDST: /eos/project/f/felix/www/netio
  COVDST: /eos/project/f/felix/www/netio/coverage
  PY3: /cvmfs/sft.cern.ch/lcg/releases/Python/3.6.5-f74f0/x86_64-centos7-gcc8-opt/bin/python3.6
  LCOV: ../external/lcov/1.15/bin
  BINARY_TAG: x86_64-centos7-gcc11-opt

# LCOV: /cvmfs/sft.cern.ch/lcg/releases/lcov/1.14-b9cb3/x86_64-centos7-gcc8-opt/bin

# netio-unit tests fails to fallover on sockets
default:
  tags:
  - k8s-cvmfs

stages:
- config-build-test
- deploy

# NOTE: compile and run everything one one machine otherwise the paths in the libraries refer to paths where the system was compiled.
# Each runner machine has its own local directory (name).
config-build-test:
  stage: config-build-test
  tags:
  - pc-tbed-felix-p1-05.cern.ch
  - cvmfs
  script:
  - cd ${CI_PROJECT_DIR}
  - source python_env/bin/activate
  - source cmake_tdaq/bin/setup.sh ${BINARY_TAG}
  - python -m flake8
  - cmake_config ${BINARY_TAG} -- -DFELIX_COVERAGE=
  - cd ${BINARY_TAG}
  - cppcheck --error-exitcode=1 --enable=all --std=c++17 -i ../../netio-next/src/netio_py.c --includes-file=../../netio-next/cppcheck.include --suppressions-list=../../netio-next/cppcheck.suppress ../../netio-next/src ../../netio-next/unit_tests/ ../../netio-next/examples/
  - make -j 8
  # NOTE: Run sequentially as some tests interfere with eachother and then fail
  - ctest -R netio-next -j 4 || ctest -R netio-next --rerun-failed --output-on-failure
  # Needs version 1.15 of lcov, not on cvmfs, for gcc11
  # Needs "yum install perl-JSON-PP"
  - ${LCOV}/lcov --capture --base-directory . --directory . --output-file coverage.info --include \*netio-next/src\*
  - sed -i -e 's/,-1$/,0/g' coverage.info
  - ${LCOV}/genhtml coverage.info --output-directory coverage-html
  artifacts:
    expire_in: 1 day
    paths:
    - ${CI_PROJECT_DIR}/${BINARY_TAG}/coverage-html


build-docs:
  image: gitlab-registry.cern.ch/atlas-tdaq-felix/gitlab_ci_runner:cc7-sphinx
  stage: config-build-test
  script:
  - cd ${CI_PROJECT_DIR}/doc
  # yum install python36 doxygen -y
  # ${PY3} -m venv venv
  - pip3 install -r requirements.txt
  # source venv/bin/activate && make html
  - make html
  # source venv/bin/activate && export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/latest/bin/x86_64-linux/:$PATH && make latexpdf
  - export PATH=/cvmfs/sft.cern.ch/lcg/external/texlive/latest/bin/x86_64-linux/:$PATH && make latexpdf
  artifacts:
    expire_in: 1 day
    paths:
    - ${CI_PROJECT_DIR}/doc/_build


deploy:docs:staging:
  image: gitlab-registry.cern.ch/atlas-tdaq-felix/gitlab_ci_runner:cc7-eos
  stage: deploy
  needs: ["build-docs"]
  environment:
    name: staging
    url: https://atlas-project-felix.web.cern.ch/atlas-project-felix/dev/staging/www/netio/${CI_COMMIT_REF_NAME}
  script:
  - cd ${CI_PROJECT_DIR}/doc/_build/html
  - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
  - eos ls ${STAGING}
  - eos mkdir -p ${STAGING}/${CI_COMMIT_REF_NAME}
  - xrdcp -r -f -N . ${EOSPROJECT}/${STAGING}/${CI_COMMIT_REF_NAME}
  - cd ${CI_PROJECT_DIR}/doc/_build/latex
  - xrdcp -r -f -N NetIO.pdf ${EOSPROJECT}/${STAGING}/${CI_COMMIT_REF_NAME}/NetIO-User-Manual.pdf
  only:
  - master

deploy:docs:production:
  image: gitlab-registry.cern.ch/atlas-tdaq-felix/gitlab_ci_runner:cc7-eos
  stage: deploy
  needs: ["build-docs"]
  environment:
    name: production
    url: https://atlas-project-felix.web.cern.ch/atlas-project-felix/netio/${CI_COMMIT_REF_NAME}
  script:
  - cd ${CI_PROJECT_DIR}/doc/_build/html
  - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
  - eos ls ${DOCDST}
  - eos mkdir -p ${DOCDST}/${CI_COMMIT_REF_NAME}
  - xrdcp -r -f -N . ${EOSPROJECT}/${DOCDST}/${CI_COMMIT_REF_NAME}
  - cd ${CI_PROJECT_DIR}/doc/_build/latex
  - xrdcp -r -f -N NetIO.pdf ${EOSPROJECT}/${DOCDST}/${CI_COMMIT_REF_NAME}/NetIO-User-Manual.pdf
  only:
  - master
  - tags

deploy:coverage:
  image: gitlab-registry.cern.ch/atlas-tdaq-felix/gitlab_ci_runner:cc7-eos
  stage: deploy
  needs: ["config-build-test"]
  script:
  - cd ${CI_PROJECT_DIR}/${BINARY_TAG}/coverage-html
  - echo "${KRB_PASSWORD}" | kinit ${KRB_USERNAME}@CERN.CH
  - eos ls ${COVDST}
  - eos mkdir -p ${COVDST}/${CI_COMMIT_REF_NAME}
  - xrdcp -r -f -N . ${EOSPROJECT}/${COVDST}/${CI_COMMIT_REF_NAME}
