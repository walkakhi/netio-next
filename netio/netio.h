#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <fcntl.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <rdma/fabric.h>
#pragma GCC diagnostic ignored "-Wpedantic"
/* ignoring: include/rdma/fi_eq.h:165:17: warning: ISO C++ forbids flexible array member ‘data’ */
#include <rdma/fi_domain.h>
#pragma GCC diagnostic pop
#include <rdma/fi_endpoint.h>
#include <rdma/fi_cm.h>

#define likely(expr)    (__builtin_expect(!!(expr), 1))
#define unlikely(expr)  (__builtin_expect(!!(expr), 0))

#define NETIO_MAX_CQ_ENTRIES (4096)
#define NETIO_DOMAIN_MAX_MR (1024)
#define NETIO_MAX_EPOLL_EVENTS (128)
#define NETIO_EPOLL_TIMEOUT (1000)
#define NETIO_MAX_POLLED_FIDS 64

// Lower values will use more calls to handle CQ, higher values will create lockout
#define NETIO_MAX_CQ_EVENTS (10)
#define NETIO_MAX_CQ_EVENTS_BUFFERED (5)    /* Limit for buffered */

#define NETIO_MAX_ADDRLEN (32)
#define NETIO_MAX_IOV_LEN (28)
#define NETIO_INITIAL_SUBSCRIPTIONS (4096)

// flags for send & publish
#define NETIO_REENTRY (1)

// return values
#define NETIO_STATUS_OK       (0)  /* Success */
#define NETIO_STATUS_TOO_BIG  (1)  /* Message too big for buffer */
#define NETIO_STATUS_AGAIN    (2)  /* Resource currently not available, try again */
#define NETIO_STATUS_PARTIAL  (3)  /* Operation completed partially, try again */
#define NETIO_STATUS_ERROR    (4)  /* Operation failed */
#define NETIO_STATUS_OK_NOSUB (5)  /* Message sent to a publish socket that has no subscribers */


#define NETIO_SUBSCRIBE   (1) /* subscription actions */
#define NETIO_UNSUBSCRIBE (0)

#define NETIO_ERROR_MAX_IOV_EXCEEDED  (-10)

typedef uint64_t netio_tag_t;
enum netio_fabric_mode {NETIO_MODE_LIBFABRIC=1111, NETIO_MODE_TCP=2222};
enum resource_type {NETIO_TIMER, NETIO_SIGNAL, NETIO_CQ, NETIO_EQ, NETIO_TCP};
enum socket_type {BSEND, USEND, BRECV, URECV, BSUB, USUB, BPUB, UPUB, BLISTEN, ULISTEN, NOSOCKET, TESTSOCKET};

struct netio_eventloop;
struct netio_context;
struct netio_recv_socket;
struct netio_buffer;
struct netio_buffered_recv_socket;

struct polled_fids_data
{
  int fd;
  void* socket;
  void (*cb)(int, void*);
};
struct polled_fids
{
  struct fid_fabric* fabric;
  struct fid** fid_set;
  struct polled_fids_data* data;
  unsigned int count;
  unsigned int size;
};

void init_polled_fids(struct polled_fids* pfids, int initial_size);
void print_polled_fids(struct polled_fids* pfids);
void add_polled_fid(struct polled_fids* pfids, struct fid_fabric* fab, struct fid* fid, int fd, void* socket, void (*cb)(int,void*));
void remove_polled_fid(struct polled_fids* pfids, int fd);

struct open_fd_data
{
  int fd;
  void* object;
  enum resource_type rtype;
  enum socket_type stype;
};

struct open_fds
{
  struct open_fd_data* data;
  unsigned int count;
  unsigned int size;
};

void init_openfds(struct open_fds* fds, int initial_size);
void print_openfds(struct open_fds* fds);
void add_open_fd(struct open_fds* fds, int fd, enum resource_type rtype, enum socket_type stype, void* object);
void remove_open_fd(struct netio_eventloop* ev, int fd);
int check_open_fd_exists(struct open_fds* fds, int fd);

//delete socket cbs
void close_send_socket(void* ptr);
void close_recv_socket(void* ptr);
void close_buffered_send_socket(void* ptr);
void close_buffered_recv_socket(void* ptr);
void close_listen_socket(void* ptr);
void close_buffered_listen_socket(void* ptr);
void netio_stop(void* ptr);
void netio_connection_shutdown(void* ptr);


struct netio_event_context
{
    int fd;
    void* data;
    void (*cb)(int,void*);
};

struct netio_signal
{
  void* data;
  int epollfd;
  void (*cb)(void*);
  struct netio_event_context ev_ctx;
};

struct netio_unbuffered_socket_attr
{
  size_t num_buffers;
  size_t buffer_size;
};

struct netio_buffered_socket_attr
{
  unsigned num_pages;
  size_t pagesize;
  size_t watermark;
  unsigned long timeout_ms;
};

struct closed_fds{
  int count;
  int fds[3*NETIO_MAX_POLLED_FIDS]; //during one event up to 3 FDs can be closed, so the array needs to be larger than the number of events
};


struct netio_eventloop
{
  int epollfd;
  void (*cb_init)(void*);
  int is_running;
  void* data;
  struct polled_fids pfids;
  struct open_fds openfds;
  struct epoll_event* events;
  struct netio_signal stop_signal;
  struct closed_fds closedfds;
};

struct deferred_subscription
{
  netio_tag_t tag;
  struct deferred_subscription* next;
};

struct netio_timer
{
  void* data;
  void (*cb)(void*);
  struct netio_event_context ev_ctx;
};

struct signal_data
{
  void* ptr;
  struct netio_signal* signal;
  struct netio_eventloop* evloop;
};

struct netio_semaphore
{
  void* data;
  void (*cb)(void*);
  unsigned threshold;
  unsigned current;
};

struct netio_context
{
  struct netio_eventloop evloop;
};

struct netio_domain {
  struct fid_fabric *fabric;
  struct fid_domain *domain;

  uint64_t req_key;
  uint16_t reg_mr;
  struct fid** mr;

  int nb_sockets;
};

struct netio_send_socket
{
  struct netio_context* ctx;
  struct fi_info* fi;
  struct netio_domain* domain;
  struct fid_eq *eq;
  struct fid_ep *ep;
  struct fid_cq* cq;
  struct fid_cq* rcq;
  size_t cq_size;
  enum { UNCONNECTED, CONNECTED, DISCONNECTED} state;
  struct netio_recv_socket* recv_socket; //Receive socket that corresponds to the same connection

  void* usr; /* some reserved data for user data, e.g. other socket types */
  struct netio_unbuffered_publish_socket* unbuf_pub_socket;
  struct deferred_subscription* deferred_subs;

  int fi_errno;
  char* fi_message;
  int epollfd;

  int eqfd;
  struct netio_event_context eq_ev_ctx;

  int cqfd;
  struct netio_event_context cq_ev_ctx;

  enum netio_fabric_mode tcp_fi_mode;
  void *message_request_header;        //MJ new idea: "void *list_head" poins to the first (dynamically allocated) message request descriptor. Additional descriptors are added to a linked list
  struct netio_signal tcp_signal;

  void (*cb_connection_established)(struct netio_send_socket*);
  void (*cb_connection_closed)(struct netio_send_socket*);
  void (*cb_internal_connection_closed)(struct netio_send_socket*);
  void (*cb_send_completed)(struct netio_send_socket*, uint64_t key);
  void (*cb_error_connection_refused)(struct netio_send_socket*);
};


struct netio_socket_list
{
    void* addr;
    size_t addrlen;
    int port;
    void* socket;
    struct netio_socket_list* next;
};

struct netio_socket_list* find_socket(struct netio_socket_list* list, void* socket);
struct netio_socket_list* find_socket_by_address(struct netio_socket_list* list, void* addr, size_t addrlen, int port);
struct netio_socket_list* add_socket(struct netio_socket_list** list, int type);
struct netio_socket_list* add_socket_with_address(struct netio_socket_list** list, int type, void* addr, size_t addrlen, int port);
int remove_socket(struct netio_socket_list** list, void* socket);
int free_socket_list(struct netio_socket_list** list);
int is_socket_list_empty(struct netio_socket_list* list);
int print_sockets(struct netio_socket_list* list);


struct netio_listen_socket
{
  struct netio_context* ctx;
  struct fi_info* fi;
  struct fid_fabric *fabric;
  struct fid_eq *eq;
  struct fid_pep *pep;

  void* usr; /* some reserved data for user data, e.g. other socket types */
  struct netio_socket_list* recv_sockets;
  enum netio_fabric_mode tcp_fi_mode;
  uint16_t port;
  int recv_sub_msg;

  int fi_errno;
  char* fi_message;

  int eqfd;
  struct netio_event_context eq_ev_ctx;
  struct netio_unbuffered_socket_attr attr;

  void (*cb_connection_established)(struct netio_recv_socket*);
  void (*cb_connection_closed)(struct netio_recv_socket*);
  void (*cb_msg_received)(struct netio_recv_socket*, struct netio_buffer*, void*, size_t);
  void (*cb_msg_imm_received)(struct netio_recv_socket*, struct netio_buffer*, void*, size_t, uint64_t);
  void (*cb_error_bind_refused)(struct netio_listen_socket*);
};

struct netio_recv_socket
{
  struct netio_context* ctx;
  struct netio_listen_socket* lsocket;

  struct fid_domain *domain;
  struct fid_ep *ep;
  struct fid_eq *eq;
  struct fid_cq* cq;
  struct fid_cq* tcq;
  size_t cq_size;
  uint64_t req_key;
  uint16_t reg_mr;
  struct fid** mr;

  struct netio_buffer** sub_msg_buffers;
  void* usr; /* some reserved data for user data, e.g. other socket types */
  struct netio_buffer* recv_buffers;

  enum netio_fabric_mode tcp_fi_mode;
  struct netio_signal tcp_signal;
  void *message_request_header;

  int eqfd;
  struct netio_event_context eq_ev_ctx;

  int cqfd;
  struct netio_event_context cq_ev_ctx;
};

struct netio_buffer
{
  void* data;
  size_t size;
  struct fid_mr* mr;
  int to_send;
};

struct netio_buffer_stack
{
  struct netio_buffer** buffers;
  struct netio_buffer** buffer_addresses; //To bookkeep buffer addresses
  size_t num_buffers;
  size_t available_buffers;
};

struct netio_buffered_listen_socket
{
  struct netio_listen_socket listen_socket;

  unsigned num_pages;
  size_t pagesize;

  void* usr; /* some reserved data for user data, e.g. other socket types */

  void (*cb_connection_established)(struct netio_buffered_recv_socket*);
  void (*cb_connection_closed)(struct netio_buffered_recv_socket*);
  void (*cb_msg_received)(struct netio_buffered_recv_socket*, void*, size_t);
};

struct netio_buffered_send_socket
{
  struct netio_send_socket send_socket;
  struct netio_buffer_stack buffers;
  struct netio_buffer* current_buffer;
  int busy;
  size_t pos;
  size_t buffersize;
  unsigned num_pages;
  size_t watermark;

  struct netio_timer flush_timer;
  unsigned long timeout_ms;

  struct netio_publish_socket* pub_socket;
  void* usr; /* some reserved data for user data, e.g. other socket types */
  struct netio_signal signal_buffer_available;

  void (*cb_connection_established)(struct netio_buffered_send_socket*);
  void (*cb_connection_closed)(struct netio_buffered_send_socket*);
  void (*cb_error_connection_refused)(struct netio_buffered_send_socket*);
};

struct netio_buffered_recv_socket
{
  struct netio_recv_socket recv_socket;
  struct netio_buffered_listen_socket* lsocket;
  struct netio_buffer* pages;
  void* usr; /* some reserved data for user data, e.g. other socket types */
  unsigned num_pages;
};


struct netio_subscription
{
  netio_tag_t tag;
  void* socket;
  int again;
};

struct netio_subscription_table
{
    struct netio_socket_list* socket_list; /* contains all open sockets */
    struct netio_subscription* subscriptions; /* array with subscriptions */
    size_t size;                        /* size of the arrays */
    size_t num_subscriptions;           /* number of subscriptions */
    uint64_t ts;
};

struct netio_subscription_cache
{
  netio_tag_t tag;
  struct netio_subscription_table* table;
  unsigned idx_start;
  unsigned count;
  uint64_t ts;
};

struct netio_subscription_message
{
    netio_tag_t tag;
    char addr[NETIO_MAX_ADDRLEN];
    size_t addrlen;
    uint16_t port;
    uint8_t action;
};

struct netio_publish_socket
{
    struct netio_context* ctx;
    struct netio_subscription_table subscription_table;
    struct netio_buffered_socket_attr attr;
    struct netio_listen_socket lsocket;
    enum netio_fabric_mode tcp_fi_mode;

    void* usr; /* some reserved data for user data, e.g. other socket types */

    void (*cb_subscribe)(struct netio_publish_socket*, netio_tag_t, void*, size_t);
    void (*cb_unsubscribe)(struct netio_publish_socket*, netio_tag_t, void*, size_t);
    void (*cb_connection_established)(struct netio_publish_socket*);
    void (*cb_connection_closed)(struct netio_publish_socket*);
    void (*cb_buffer_available)(struct netio_publish_socket*);
};

enum netio_subscribe_state { NONE, INITIALIZED };

struct netio_subscribe_socket
{
  struct netio_context* ctx;
  struct netio_send_socket socket;
  netio_tag_t tags_to_subscribe[NETIO_INITIAL_SUBSCRIPTIONS]; // TODO
  size_t total_tags;
  enum netio_subscribe_state state;
  enum netio_fabric_mode tcp_fi_mode;

  char const* remote_hostname;
  unsigned remote_port;

  struct netio_subscription_message msg;
  struct netio_buffer buf;

  struct netio_subscription_message msgs[NETIO_INITIAL_SUBSCRIPTIONS];
  struct netio_buffer bufs[NETIO_INITIAL_SUBSCRIPTIONS];

  struct netio_buffered_socket_attr attr;
  struct netio_buffered_listen_socket recv_socket;

  void* usr; /* some reserved data for user data, e.g. other socket types */

  void (*cb_connection_established)(struct netio_subscribe_socket*);
  void (*cb_connection_closed)(struct netio_subscribe_socket*);
  void (*cb_msg_received)(struct netio_subscribe_socket*, netio_tag_t, void*, size_t);
  void (*cb_buf_received)(struct netio_subscribe_socket*, struct netio_buffer*, size_t);
  void (*cb_error_connection_refused)(struct netio_subscribe_socket*);
};


struct netio_unbuffered_publish_socket; // Forward declaration
struct netio_completion_object
{
  struct netio_semaphore sem;
  struct netio_unbuffered_publish_socket* socket;
  uint64_t key;
  struct {
    netio_tag_t tag;
    uint64_t usr; // header-field used for user-defined headers
  } header;
  uint8_t usr_size; // size of the user-defined header field
};

struct netio_completion_stack
{
  struct netio_completion_object* objects;
  struct netio_completion_object** stack;
  size_t num_objects;
  size_t available_objects;
  struct netio_buffer buf;
  uint64_t* key_array;
  uint8_t printed;
};

struct netio_unbuffered_publish_socket
{
  struct netio_context* ctx;
  struct netio_subscription_table subscription_table;
  struct netio_listen_socket lsocket;
  struct netio_buffer buf;

  struct netio_buffer* buf_array[NETIO_MAX_IOV_LEN];

  struct netio_completion_stack completion_stack;
  enum netio_fabric_mode tcp_fi_mode;

  void* usr; /* some reserved data for user data, e.g. other socket types */

  void (*cb_subscribe)(struct netio_unbuffered_publish_socket*, netio_tag_t, void*, size_t);
  void (*cb_unsubscribe)(struct netio_unbuffered_publish_socket*, netio_tag_t, void*, size_t);
  void (*cb_connection_established)(struct netio_unbuffered_publish_socket*);
  void (*cb_connection_closed)(struct netio_unbuffered_publish_socket*);
  void (*cb_msg_published)(struct netio_unbuffered_publish_socket*, uint64_t);
};


struct netio_unbuffered_subscribe_socket
{
  struct netio_context* ctx;
  struct netio_send_socket socket;
  netio_tag_t tags_to_subscribe[NETIO_INITIAL_SUBSCRIPTIONS]; // TODO
  size_t next_tag_to_subscribe;
  size_t total_tags;
  enum netio_subscribe_state state;
  enum netio_fabric_mode tcp_fi_mode;

  char const* remote_hostname;
  unsigned remote_port;

  struct netio_subscription_message msg;
  struct netio_buffer buf;

  struct netio_subscription_message msgs[NETIO_INITIAL_SUBSCRIPTIONS];
  struct netio_buffer bufs[NETIO_INITIAL_SUBSCRIPTIONS];

  struct netio_listen_socket recv_socket;

  void* usr; /* some reserved data for user data, e.g. other socket types */

  void (*cb_connection_established)(struct netio_unbuffered_subscribe_socket*);
  void (*cb_connection_closed)(struct netio_unbuffered_subscribe_socket*);
  void (*cb_msg_received)(struct netio_unbuffered_subscribe_socket*, netio_tag_t, void*, size_t);

  void (*cb_error_connection_refused)(struct netio_unbuffered_subscribe_socket*);
};

void handle_send_socket_shutdown(struct netio_send_socket* socket);
void handle_tcp_send_socket_shutdown(struct netio_send_socket* socket);
void handle_recv_socket_shutdown(struct netio_recv_socket* socket);
void handle_listen_socket_shutdown(struct netio_listen_socket* socket);

int netio_tcp_mode(const char* hostname);
const char* netio_hostname(const char* hostname);
const char* netio_protocol(const char* hostname);

void netio_init(struct netio_context* ctx);
void netio_set_debug_level(int level);
void netio_init_send_socket(struct netio_send_socket* socket, struct netio_context* ctx);
void netio_unbuffered_send_socket_init(struct netio_send_socket* socket, struct netio_context* ctx);
void netio_init_listen_socket(struct netio_listen_socket* socket, struct netio_context* ctx, struct netio_unbuffered_socket_attr* attr);
void netio_init_recv_socket(struct netio_recv_socket* socket, struct netio_listen_socket* lsocket);
void netio_close_socket(struct netio_eventloop* evloop, void* socket, enum socket_type type);
void netio_remove_recv_socket(struct netio_recv_socket* socket);

void netio_send_socket_init_and_connect(struct netio_send_socket* socket, struct netio_context* ctx, const char* hostname, unsigned port);
void netio_listen_socket_init_and_listen(struct netio_listen_socket* socket, struct netio_context* ctx, const char* hostname, unsigned port, struct netio_unbuffered_socket_attr* attr);

void netio_connect(struct netio_send_socket* socket, const char* hostname, unsigned port);
void netio_connect_domain(struct netio_send_socket* socket, const char* hostname, unsigned port, struct netio_domain* domain);
void netio_connect_rawaddr(struct netio_send_socket* socket, void* addr, size_t addrlen);
void netio_connect_rawaddr_domain(struct netio_send_socket* socket, void* addr, size_t addrlen, struct netio_domain* domain);
void netio_disconnect(struct netio_send_socket* socket);
void netio_listen(struct netio_listen_socket* socket, const char* hostname, unsigned port);

size_t netio_listen_socket_endpoint(struct netio_listen_socket* socket, struct sockaddr_storage *sa);

void netio_eventloop_init(struct netio_eventloop* evloop);
void netio_timer_init(struct netio_eventloop* evloop, struct netio_timer* timer);
void netio_timer_start_s(struct netio_timer* timer, unsigned long long seconds);
void netio_timer_start_ms(struct netio_timer* timer, unsigned long long milliseconds);
void netio_timer_start_us(struct netio_timer* timer, unsigned long long microseconds);
void netio_timer_start_ns(struct netio_timer* timer, unsigned long long nanoseconds);
void netio_timer_stop(struct netio_timer* timer);
void netio_timer_close(struct netio_eventloop* evloop, struct netio_timer* timer);
void netio_signal_init(struct netio_eventloop* evloop, struct netio_signal* signal);
void netio_signal_no_semaphore_init(struct netio_eventloop* evloop, struct netio_signal* signal);
void netio_signal_close(struct netio_eventloop* evloop, struct netio_signal* signal);
void netio_signal_fire(struct netio_signal* signal);
void netio_error_connection_refused_fire(struct netio_send_socket* socket);
void netio_error_bind_refused_fire(struct netio_listen_socket* socket);
void netio_register_read_fd(struct netio_eventloop* evloop, struct netio_event_context* ctx);
void netio_run(struct netio_eventloop* evloop);
void netio_terminate(struct netio_eventloop* evloop);
void netio_terminate_signal(struct netio_eventloop* evloop);

void netio_semaphore_init(struct netio_semaphore* sem, unsigned threshold);
void netio_semaphore_increment(struct netio_semaphore* sem, unsigned n);
void netio_semphore_set_threshold(struct netio_semaphore* sem, unsigned t);

void netio_register_recv_buffer(struct netio_recv_socket* socket, struct netio_buffer* buf, uint64_t flags);
void netio_register_send_buffer(struct netio_send_socket* socket, struct netio_buffer* buf, uint64_t flags);

void netio_bufferstack_send_init(struct netio_buffer_stack* stack, struct netio_send_socket* socket, size_t num, size_t bufsize, uint64_t flags);
int netio_bufferstack_pop(struct netio_buffer_stack* stack, struct netio_buffer** buffer);
int netio_bufferstack_push(struct netio_buffer_stack* stack, struct netio_buffer* buffer);
void netio_bufferstack_close(struct netio_buffer_stack* stack, size_t num);

void netio_post_recv(struct netio_recv_socket* socket, struct netio_buffer* buf);
int netio_send_buffer(struct netio_send_socket* socket, struct netio_buffer* buf);
int netio_send_inline_buffer(struct netio_send_socket* socket, struct netio_buffer* buf);
int netio_send(struct netio_send_socket* socket, struct netio_buffer* buf, void* addr, size_t size, uint64_t key);
int netio_send_imm(struct netio_send_socket* socket, struct netio_buffer* buf, void* addr, size_t size, uint64_t key, uint64_t imm);
int netio_sendv(struct netio_send_socket* socket, struct netio_buffer** buf, struct iovec* iov, size_t count, uint64_t key);
int netio_sendv_imm(struct netio_send_socket* socket, struct netio_buffer** buf, struct iovec* iov, size_t count, uint64_t key, uint64_t imm);

void netio_buffered_send_socket_init(struct netio_buffered_send_socket* socket, struct netio_context* ctx, struct netio_buffered_socket_attr* attr);
void netio_buffered_listen_socket_init(struct netio_buffered_listen_socket* socket, struct netio_context* ctx, struct netio_buffered_socket_attr* attr);
void netio_buffered_recv_socket_init(struct netio_buffered_recv_socket* socket, struct netio_buffered_listen_socket* lsocket);
void netio_buffered_listen(struct netio_buffered_listen_socket* socket, const char* hostname, unsigned port);
void netio_buffered_connect(struct netio_buffered_send_socket* socket, const char* hostname, unsigned port);
void netio_buffered_connect_rawaddr(struct netio_buffered_send_socket* socket, void* addr, size_t addrlen);
int netio_buffered_send(struct netio_buffered_send_socket* socket, void* data, size_t size);
int netio_buffered_sendv(struct netio_buffered_send_socket* socket, struct iovec* iov, size_t num);
void netio_buffered_flush(struct netio_buffered_send_socket* socket);
void netio_buffered_remove_recv_socket(struct netio_buffered_recv_socket* socket);

void netio_buffered_send_socket_init_and_connect(struct netio_buffered_send_socket* socket, struct netio_context* ctx, const char* hostname, unsigned port, struct netio_buffered_socket_attr* attr);
void netio_buffered_listen_socket_init_and_listen(struct netio_buffered_listen_socket* socket, struct netio_context* ctx, const char* hostname, unsigned port, struct netio_buffered_socket_attr* attr);

void netio_subscription_table_init(struct netio_subscription_table* table);
void netio_subscription_cache_init(struct netio_subscription_cache* cache);
void netio_publish_socket_init(struct netio_publish_socket* socket, struct netio_context* ctx, const char* hostname, unsigned port, struct netio_buffered_socket_attr* attr);
void netio_subscribe_socket_init(struct netio_subscribe_socket* socket,
                                 struct netio_context* ctx,
                                 struct netio_buffered_socket_attr* attr,
                                 const char* hostname,
                                 const char* remote_host,
                                 unsigned remote_port);
int netio_subscribe(struct netio_subscribe_socket* socket, netio_tag_t tag);
int netio_unsubscribe(struct netio_subscribe_socket* socket, netio_tag_t tag);
int netio_buffered_publish(struct netio_publish_socket* socket, netio_tag_t tag, void* data, size_t len, int flags, struct netio_subscription_cache* cache);
void netio_buffered_publish_flush(struct netio_publish_socket* socket, netio_tag_t tag, struct netio_subscription_cache* cache);

void netio_completion_stack_init(struct netio_completion_stack* stack, size_t num);
void netio_completion_stack_register_send_socket(struct netio_completion_stack* stack, struct netio_send_socket* socket);
int netio_completion_stack_pop(struct netio_completion_stack* stack, struct netio_completion_object** object);
int netio_completion_stack_push(struct netio_completion_stack* stack, struct netio_completion_object* object);

void push_back_subscription(struct deferred_subscription** list, netio_tag_t tag);
void pop_subscription(struct deferred_subscription** list);

void netio_unbuffered_publish_socket_init(struct netio_unbuffered_publish_socket* socket, struct netio_context* ctx, const char* hostname, unsigned port, struct netio_buffer* buf);
int netio_unbuffered_publishv(struct netio_unbuffered_publish_socket* socket,
                              netio_tag_t tag,
                              struct iovec* iov,
                              size_t count,
                              uint64_t* key,
                              int flags,
                              struct netio_subscription_cache* cache);
int netio_unbuffered_publishv_usr(struct netio_unbuffered_publish_socket* socket,
                                  netio_tag_t tag,
                                  struct iovec* iov,
                                  size_t count,
                                  uint64_t* key,
                                  int flags,
                                  struct netio_subscription_cache* cache,
                                  uint64_t usr,
                                  uint8_t usr_size);
void netio_unbuffered_subscribe_socket_init(struct netio_unbuffered_subscribe_socket* socket,
                                            struct netio_context* ctx,
                                            const char* hostname,
                                            const char* remote_host,
                                            unsigned remote_port,
                                            size_t buffer_size,
                                            size_t count);
int netio_unbuffered_subscribe(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag);
int netio_unbuffered_unsubscribe(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag);

char* netio_domain_name_lookup(const char* domain_name);

#ifdef __cplusplus
}
#endif
