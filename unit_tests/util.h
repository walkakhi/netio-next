#pragma once

#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <ifaddrs.h>

#include <catch2/catch_test_macros.hpp>

inline unsigned
port_from_sockaddr(struct sockaddr *sa)
{
  in_port_t port;
  if (sa->sa_family == AF_INET) {
    port = ((struct sockaddr_in*)sa)->sin_port;
  } else if (sa->sa_family == AF_INET6) {
    port = ((struct sockaddr_in6*)sa)->sin6_port;
  } else {
    return 0;
  }

  return ntohs(port);
}


inline unsigned
port_of_listen_socket(struct netio_listen_socket* ls)
{
  struct sockaddr_storage sa;
  netio_listen_socket_endpoint(ls, &sa);
  unsigned port = port_from_sockaddr((struct sockaddr*)&sa);
  return port;
}


inline unsigned
port_of_buffered_listen_socket(struct netio_buffered_listen_socket* ls)
{
  return port_of_listen_socket(&ls->listen_socket);
}


inline const char*
test_iface_addr()
{
    const char* addr = std::getenv("NETIO_TEST_IFACE");
    if (addr) return addr;

    struct ifaddrs *ifaddr, *ifa;
    int family, s;
    static char ip[NI_MAXHOST];

    if (getifaddrs(&ifaddr) == -1) {
        UNSCOPED_INFO("getifaddrs problem");
        exit(EXIT_FAILURE);
    }

    char ifaces[] = "eth0";
    char seps[] = ", ";
    char* iface;

    iface = strtok(ifaces, seps);
    while (iface != NULL) {
        // printf("Looking for %s\n", iface);
        // Look through all existing interfaces
        for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
            if (ifa->ifa_addr == NULL)
                continue;

            s = getnameinfo(ifa->ifa_addr, sizeof(struct sockaddr_in), ip, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
            if (s != 0)
                continue;

            // printf("Trying %s\n", ifa->ifa_name);
            if (((strcmp(ip, iface) == 0) || (strcmp(ifa->ifa_name, iface) == 0)) && (ifa->ifa_addr->sa_family==AF_INET)) {
                UNSCOPED_INFO("Interface: <" << ifa->ifa_name << ">");
                UNSCOPED_INFO("Address: <" << ip << ">");
                freeifaddrs(ifaddr);
                return ip;
            }
        }

        // next on the list
        iface = strtok(NULL, seps);
    }

    // not found
    UNSCOPED_INFO("NETIO_TEST_IFACE is not set, using 127.0.0.1 as loopback address.");
    UNSCOPED_INFO("If fi_endpoint fails, consider setting NETIO_TEST_IFACE to the RDMA device's IP address.");
    addr = "127.0.0.1";
    freeifaddrs(ifaddr);
    return addr;
}
