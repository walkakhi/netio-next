#include <string>

#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

TEST_CASE("Hostname netio_tcp_mode", "[hostname]")
{
    REQUIRE(netio_tcp_mode("127.0.0.1") == false);
    REQUIRE(netio_tcp_mode("libfabric:127.0.0.1") == false);
    REQUIRE(netio_tcp_mode("tcp:127.0.0.1") == true);
}

TEST_CASE("Hostname netio_hostname", "[hostname]")
{
    REQUIRE(std::string(netio_hostname("127.0.0.1")) == "127.0.0.1");
    REQUIRE(std::string(netio_hostname("libfabric:127.0.0.1")) == "127.0.0.1");
    REQUIRE(std::string(netio_hostname("tcp:127.0.0.1")) == "127.0.0.1");
}


TEST_CASE("Hostname netio_protocol", "[hostname]")
{
    REQUIRE(std::string(netio_protocol("127.0.0.1")) == "libfabric");
    REQUIRE(std::string(netio_protocol("libfabric:127.0.0.1")) == "libfabric");
    REQUIRE(std::string(netio_protocol("tcp:127.0.0.1")) == "tcp");
}
