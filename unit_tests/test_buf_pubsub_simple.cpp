#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

#include <cstring>
#include "util.h"

namespace buf_big_tag {

unsigned long TAG = 1UL << 42;

struct State {
  bool initialized = false;
  unsigned num_subscriptions = 0;
  unsigned num_connections = 0;
  unsigned num_msg_received = 0;
} state;

char pdata[1024];
char sdata1[1024];

uint64_t key;

struct netio_context ctx;
struct netio_publish_socket pub;
struct netio_subscribe_socket sub;

void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);

void on_init(void* data) {
  state.initialized = true;
  unsigned port = port_of_listen_socket(&pub.lsocket);
  REQUIRE(port > 0);

  struct netio_buffered_socket_attr attr;
  attr.num_pages = 16;
  attr.pagesize = 64*1024;
  attr.watermark = 63*1024;

  netio_subscribe_socket_init(&sub, &ctx, &attr, test_iface_addr(), test_iface_addr(), port);
  sub.cb_msg_received = on_msg_received;

  netio_subscribe(&sub, TAG);
}

void on_subscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  state.num_subscriptions++;
}

void on_connection_established(struct netio_publish_socket* socket)
{
  state.num_connections++;
  memcpy(pdata, "foobar", 7);

  REQUIRE( NETIO_STATUS_OK == netio_buffered_publish(
                              &pub,       // publish socket
                              TAG,        // tag
                              &pdata,     // data
                              7,          // data length
                              0,          // flags
                              NULL));     // subscription cache
  netio_buffered_publish_flush(
                              &pub,       // publish socket
                              TAG,        // tag
                              NULL);      // subscription cache
}

void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  REQUIRE( size == 7 );
  REQUIRE( tag == TAG );
  REQUIRE( memcmp(data, "foobar", 7) == 0 );
  state.num_msg_received ++;
  netio_terminate(&ctx.evloop);
}

TEST_CASE( "buf publish with a 64-bit tag", "[buf_pubsub]" ) {
  state = State();
  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;

  struct netio_buffered_socket_attr attr;
  attr.num_pages = 16;
  attr.pagesize = 64*1024;
  attr.watermark = 63*1024;

  netio_publish_socket_init(&pub, &ctx, test_iface_addr(), 0, &attr);
  pub.cb_subscribe = on_subscribe;
  pub.cb_connection_established = on_connection_established;

  netio_run(&ctx.evloop);
  REQUIRE( state.num_subscriptions == 1 );
  REQUIRE( state.num_msg_received == 1 );
}

}

namespace pubsub_buffered_connection_error_test {
  unsigned long TAG = 1UL << 42;

  struct netio_context ctx;
  struct netio_subscribe_socket sub;

  struct State {
    bool initialized = false;
    bool connection_refused = false;
    int fi_errno = 0;
    char* fi_message = NULL;
  } state;

  void on_init(void* data) {
    state.initialized = true;
  }

  void on_error_connection_refused(struct netio_subscribe_socket* socket) {
    state.connection_refused = true;
    state.fi_errno = socket->socket.fi_errno;
    state.fi_message = (socket->socket.fi_message) ? strdup(socket->socket.fi_message) : NULL;
    netio_terminate(&ctx.evloop);
  }

  TEST_CASE( "subscribe buffered to non-existing remote", "[buf_pubsub]" ) {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    struct netio_buffered_socket_attr attr;
    attr.num_pages = 16;
    attr.pagesize = 64*1024;
    attr.watermark = 63*1024;

    SECTION( "subscribe buffered non-existing node" ) {
      netio_subscribe_socket_init(&sub, &ctx, &attr, test_iface_addr(), "unknown", 111);  // non-existing node -> -61 getinfo: No data available, FI_ENODATA
      sub.cb_error_connection_refused = on_error_connection_refused;

      netio_subscribe(&sub, TAG);
      netio_run(&ctx.evloop);
      REQUIRE(state.connection_refused);
      REQUIRE(state.fi_errno == 61);
      REQUIRE(!strcmp(state.fi_message, "fi_getinfo failed"));
    }

    SECTION( "subscribe buffered non-existing port" ) {
      netio_subscribe_socket_init(&sub, &ctx, &attr, test_iface_addr(), "www.cern.ch", 111); // non-existing port -> -111 fi_connect: Connection refused, FI_ECONNREFUSED
      sub.cb_error_connection_refused = on_error_connection_refused;

      netio_subscribe(&sub, TAG);
      netio_run(&ctx.evloop);
      REQUIRE(state.connection_refused);
      REQUIRE(state.fi_errno == 111);
      REQUIRE(!strcmp(state.fi_message, "fi_connect failed"));
    }

    // SECTION( "subscribe buffered non-existing interface" ) {
    //   netio_subscribe_socket_init(&sub, &ctx, &attr, test_iface_addr(), "129.0.0.1", 111); // non-existing interface -> -110 fi_connect: Connection timed out, FI_ETIMEDOUT
    //   sub.cb_error_connection_refused = on_error_connection_refused;

    //   netio_subscribe(&sub, TAG);
    //   netio_run(&ctx.evloop);
    //   REQUIRE(state.connection_refused);
    //   REQUIRE(state.fi_errno == 110);
    //   REQUIRE(!strcmp(state.fi_message, "fi_connect failed"));
    // }

    SECTION( "subscribe buffered known node, non-existing port" ) {
      netio_subscribe_socket_init(&sub, &ctx, &attr, test_iface_addr(), test_iface_addr(), 111); // non-existing port
      sub.cb_error_connection_refused = on_error_connection_refused;

      netio_subscribe(&sub, TAG);
      netio_run(&ctx.evloop);
      REQUIRE(state.connection_refused);
      REQUIRE(state.fi_errno == 0);
      REQUIRE(state.fi_message == NULL);
    }
  }
}


namespace pubsub_buffered_bind_error_test {

  struct netio_context ctx;
  struct netio_publish_socket pub;

  struct State {
    bool initialized = false;
    bool bind_refused = false;
    int fi_errno = 0;
    char* fi_message = NULL;
  } state;

  void on_init(void* data) {
    state.initialized = true;
  }

  void on_error_bind_refused(struct netio_listen_socket* socket) {
    state.bind_refused = true;
    state.fi_errno = socket->fi_errno;
    state.fi_message = (socket->fi_message) ? strdup(socket->fi_message) : NULL;
    netio_terminate(&ctx.evloop);
  }

  TEST_CASE( "buf publish bind error", "[buf_pubsub]" ) {
    state = State();
    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    struct netio_buffered_socket_attr attr;
    attr.num_pages = 16;
    attr.pagesize = 64*1024;
    attr.watermark = 63*1024;

    SECTION( "non-existing node" ) {
      netio_publish_socket_init(&pub, &ctx, "unknown", 80, &attr);
      pub.lsocket.cb_error_bind_refused = on_error_bind_refused;
      netio_run(&ctx.evloop);
      REQUIRE(state.bind_refused);
      REQUIRE(state.fi_errno == FI_ENODATA);
      REQUIRE(!strcmp(state.fi_message, "fi_getinfo failed"));
    }

    SECTION( "non-existing port" ) {
      netio_publish_socket_init(&pub, &ctx, "www.cern.ch", 0, &attr);
      pub.lsocket.cb_error_bind_refused = on_error_bind_refused;
      netio_run(&ctx.evloop);
      REQUIRE(state.bind_refused);
      REQUIRE(state.fi_errno == FI_EINVAL);
      REQUIRE(!strcmp(state.fi_message, "fi_listen failed"));
    }

    SECTION( "known node, non-existing port" ) {
      netio_publish_socket_init(&pub, &ctx, test_iface_addr(), 80, &attr);
      pub.lsocket.cb_error_bind_refused = on_error_bind_refused;
      netio_run(&ctx.evloop);
      REQUIRE(state.bind_refused);
      REQUIRE(state.fi_errno == FI_EINVAL);
      REQUIRE(!strcmp(state.fi_message, "fi_listen failed"));
    }
  }
}
