#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <numeric>

extern "C" {
#include "netio/netio.h"
}

bool initialized;
bool fired;
std::vector<uint64_t> fired_counters = {0,0,0};
struct netio_context ctx;
struct netio_signal signal;
struct netio_timer timer;

namespace signal_test{

  struct netio_signal signal0;
  struct netio_signal signal1;
  struct netio_signal signal2;

  void on_ev_start(void* data){
    netio_timer_start_ms(&timer, 5000);
    for(uint64_t i = 0; i < 1000; i++){
      netio_signal_fire(&signal0);
    }
    for(uint64_t i = 0; i < 10000; i++){
      netio_signal_fire(&signal1);
    }

    for(uint64_t i = 0; i < 1000000; i++){
      netio_signal_fire(&signal2);
    }
  }

  void on_signal_fire(void* data)
  {
    //REQUIRE((uint64_t)data == 5);
    if ((uint64_t)data == 5){
      fired_counters.at(0)++;
    } else if ((uint64_t)data == 7){
      fired_counters.at(1)++;
    } else if ((uint64_t)data == 9){
      fired_counters.at(2)++;
    } else {
      REQUIRE_FALSE(true);
    }
  }

  void on_timer_fire(void* data){
    netio_terminate(&ctx.evloop);
    std::cout << "First signal: " << fired_counters.at(0) << "/1000, second: " << fired_counters.at(1) << "/10000, third: " << fired_counters.at(2) << "/1000000." << std::endl;
  }



  TEST_CASE( "fire many signals (stresstest)", "[eventloop]" )
  {
    netio_init(&ctx);
    ctx.evloop.cb_init = on_ev_start;

    netio_signal_init(&ctx.evloop, &signal0);
    signal0.cb = on_signal_fire;
    signal0.data = (void*)5;

    netio_signal_init(&ctx.evloop, &signal1);
    signal1.cb = on_signal_fire;
    signal1.data = (void*)7;

    netio_signal_init(&ctx.evloop, &signal2);
    signal2.cb = on_signal_fire;
    signal2.data = (void*)9;

    timer.cb = on_timer_fire;
    netio_timer_init(&ctx.evloop, &timer);

    netio_run(&ctx.evloop);

    REQUIRE(std::accumulate(fired_counters.begin(), fired_counters.end(),0) == 1011000);
  }


}




void on_init(void* data)
{
  initialized = true;
  netio_terminate(&ctx.evloop);
}

TEST_CASE( "eventloop setup", "[eventloop]" )
{

  initialized = false;

  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;
  netio_run(&ctx.evloop);

  REQUIRE( initialized == true );
}

void on_signal_init(void* data)
{
  netio_signal_fire(&signal);
}




void on_signal_fire(void* data)
{
  REQUIRE((uint64_t)data == 5);
  fired = true;
  netio_terminate(&ctx.evloop);
}

TEST_CASE( "fire a signal", "[eventloop]" )
{
  fired = false;

  netio_init(&ctx);
  netio_signal_init(&ctx.evloop, &signal);
  ctx.evloop.cb_init = on_signal_init;
  signal.cb = on_signal_fire;
  signal.data = (void*)5;

  netio_run(&ctx.evloop);

  REQUIRE( fired == true );
}



void on_timer_init(void* data)
{
  netio_timer_start_ms(&timer, 100);
}

TEST_CASE( "wait for a timer", "[eventloop]" )
{
  fired = false;

  netio_init(&ctx);
  netio_timer_init(&ctx.evloop, &timer);
  ctx.evloop.cb_init = on_timer_init;
  timer.cb = on_signal_fire;
  timer.data = (void*)5;
  netio_run(&ctx.evloop);

  REQUIRE( fired == true );
}
