#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestUnbufferedUnsubscribe(FelixTestCase):

    def setUp(self):
        self.start('unbuf_pubflood-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('unbuf_pubflood-' + FelixTestCase.netio_protocol)

    def test_publish_subscribe(self):
        try:
            timeout = 90
            elink_start = FelixTestCase.fid
            elink_end = FelixTestCase.fid_10
            port = FelixTestCase.port
            ip = FelixTestCase.ip
            subprocess.check_output(' '.join(("./unbuf_subscribe_unsubscribe", FelixTestCase.netio_protocol + ":" + ip, ip, port, elink_start, elink_end)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)


if __name__ == '__main__':
    unittest.main()
