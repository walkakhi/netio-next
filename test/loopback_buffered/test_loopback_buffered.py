#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestLoopbackBuffered(FelixTestCase):

    def setUp(self):
        self.start('loopback-buffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('loopback-buffered-' + FelixTestCase.netio_protocol)

    def test_loopback_buffered(self):
        try:
            timeout = 20
            fid = FelixTestCase.fid
            fid_toflx = FelixTestCase.fid_toflx
            port = FelixTestCase.port
            alt_port = FelixTestCase.alt_port
            ip = FelixTestCase.ip
            subprocess.check_output(' '.join(("./subscribe_send", FelixTestCase.netio_protocol + ":" + ip, FelixTestCase.netio_protocol + ":" + ip, alt_port, fid_toflx, port, fid)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
