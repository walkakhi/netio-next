#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestPubfloodMTUnbuffered(FelixTestCase):

    def setUp(self):
        self.start('unbuf-pubfloodmt')

    def tearDown(self):
        self.stop('unbuf-pubfloodmt')

    def test_pubfloodmt_subscribemt(self):
        try:
            timeout = 30
            elink_start = FelixTestCase.fid
            threads = "10"
            port = FelixTestCase.port
            msgs = "50000000"
            ip = FelixTestCase.ip
            subprocess.check_output(' '.jon(("./unbuf-subscribemt", ip, ip, port, elink_start, threads, msgs)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)


if __name__ == '__main__':
    unittest.main()
