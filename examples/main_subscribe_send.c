#include <stdio.h>
#include <string.h>
#include <math.h>

#include "netio/netio.h"

#include "felixtag.h"

#define NUM_PAGES (256)
#define WATERMARK (63*1024)
#define PAGESIZE (64*1024)
#define TIMEOUT (1000)

struct {
  char* local_hostname;
  char* remote_hostname;
  unsigned subscribe_port;
  unsigned send_port;
  netio_tag_t subscribe_tag;
  netio_tag_t send_tag;

  struct netio_context ctx;
  struct netio_subscribe_socket subscribe_socket;
  struct netio_buffered_send_socket send_socket;
  struct netio_buffered_socket_attr send_attr;
} config;

const char message[] = "Hello Felix!";
size_t message_size = sizeof(message);

void on_subscribe_connection_closed(struct netio_subscribe_socket* socket);
void on_subscribe_connection_established(struct netio_subscribe_socket* socket);
void on_subscribe_connection_established(struct netio_subscribe_socket* socket);
void on_subscribe_error_connection_refused(struct netio_subscribe_socket* socket);
void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);

void on_send_connection_established(struct netio_buffered_send_socket*);
void on_send_error_connection_refused(struct netio_buffered_send_socket* socket);
void on_send_connection_closed(struct netio_buffered_send_socket* socket);

void on_init()
{
  printf("1. on_init\n");
  struct netio_buffered_socket_attr attr;
  attr.num_pages = NUM_PAGES;
  attr.pagesize = PAGESIZE;
  attr.watermark = WATERMARK;
  attr.timeout_ms = TIMEOUT;

  printf("Opening subscribe socket from %s on %s:%u\n",
         config.local_hostname, config.remote_hostname, config.subscribe_port);
  netio_subscribe_socket_init(&config.subscribe_socket, &config.ctx, &attr, config.local_hostname, config.remote_hostname, config.subscribe_port);

  config.subscribe_socket.cb_connection_closed = on_subscribe_connection_closed;
  config.subscribe_socket.cb_connection_established = on_subscribe_connection_established;
  config.subscribe_socket.cb_error_connection_refused = on_subscribe_error_connection_refused;
  config.subscribe_socket.cb_msg_received = on_msg_received;

  printf("attempting to subscribe to remote on %s:%u\n", config.remote_hostname, config.subscribe_port);
  printf("subscribing to tag 0x%lx\n", config.subscribe_tag);
  netio_subscribe(&config.subscribe_socket, config.subscribe_tag);
}


void on_subscribe_connection_closed(struct netio_subscribe_socket* socket)
{
  printf("subscribe_connection to publisher closed from %s:%u\n", socket->remote_hostname, socket->remote_port);
}


void on_subscribe_connection_established(struct netio_subscribe_socket* socket) {
  printf("2. on_subscribe_connection_established to %s:%u\n", socket->remote_hostname, socket->remote_port);

  config.send_attr.num_pages = NUM_PAGES;
  config.send_attr.pagesize = PAGESIZE;
  config.send_attr.watermark = WATERMARK;
  config.send_attr.timeout_ms = TIMEOUT;

  netio_buffered_send_socket_init_and_connect(&config.send_socket, &config.ctx, config.remote_hostname, config.send_port, &config.send_attr);
  config.send_socket.cb_connection_closed = on_send_connection_closed;
  config.send_socket.cb_connection_established = on_send_connection_established;
  config.send_socket.cb_error_connection_refused = on_send_error_connection_refused;
}


void on_subscribe_error_connection_refused(struct netio_subscribe_socket* socket)
{
  printf("subscribe_connection refused for subscribing from %s:%u\n", socket->remote_hostname, socket->remote_port);
}


void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  printf("4. on_msg_received for tag %lu with size %zu\n", tag, size);

  printf("Message: %s\n", (char *)data);

  if (size == message_size && (strncmp(message, (char *)data, message_size) == 0)) {
    netio_terminate(&config.ctx.evloop);
  }
}


void on_send_connection_established(struct netio_buffered_send_socket* socket)
{
  printf("3. on_send_connection_established. Sending message of size %zu.\n", message_size);

  netio_buffered_send(socket, (void*)message, message_size);
  netio_buffered_flush(socket);
}

void on_send_connection_closed(struct netio_buffered_send_socket* socket) {
  printf("on_send_connection_closed\n");
}

void on_send_error_connection_refused(struct netio_buffered_send_socket* socket)
{
  printf("on_send_error_connection_refused\n");
}

int main(int argc, char** argv)
{

  if(argc < 7) {
    fprintf(stderr, "Usage: %s <local_hostname> <hostname> <send-port> <send-tag> <subscribe-port> <subscribe-tag>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.local_hostname = argv[1];
  config.remote_hostname = argv[2];
  config.send_port = atoi(argv[3]);
  config.send_tag = strtol(argv[4], NULL, 0);
  config.subscribe_port = atoi(argv[5]);
  config.subscribe_tag = strtol(argv[6], NULL, 0);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return 0;
}
