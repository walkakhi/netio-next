#include <stdio.h>
#include <string.h>
#include <math.h>

#include "netio/netio.h"
#include "felixtag.h"
#include <argp.h>

#define MAX_LINKS 1024

const char *argp_program_version = "pileup 0.1";
const char *argp_program_bug_address = "<https://its.cern.ch/jira/projects/FLXUSERS>";

/* Program documentation. */
static char doc[] = "Tool to measure the pileup in buffer transmission.";

/* A description of the arguments we accept. */
static char args_doc[] = "";

/* The options we understand. */
static struct argp_option options[] = {
  {"local",   'H', "LOCAL",  0,   "Local host. Default: localhost" },
  {"remote",  'R', "REMOTE", 0,   "Remote host. Default: localhost" },
  {"port",    'p', "PORT",   0,   "Connect to the given port. Default: 53100" },
  {"busfile", 'j', "JSON",   0,   "Path to the felix-bus-fs json file printed by felix-star" },
  {"pagesize",'b', "SIZE",   0,   "NetIO page size in Byte. Default: 64kB" },
  {"pages",   'B', "SIZE",   0,   "Number of NetIO pages. Default: 256" },
  { 0 }
};

struct {
  const char* local_hostname;
  const char* remote_hostname;
  const char* bus;
  unsigned port;
  unsigned pagesize;
  unsigned num_pages;

  int num_tags;
  netio_tag_t tags[MAX_LINKS];

  struct netio_context ctx;
  struct netio_subscribe_socket socket;

  struct netio_timer subscribe_timer;
  int subrequest_sent;
} config;

int return_code = 0;

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char* end;

  switch (key)
    {
    case 'H':
      config.local_hostname = strdup(arg);
      break;

    case 'R':
      config.remote_hostname = strdup(arg);
      break;

    case 'p':
      config.port = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'j':
      config.bus = strdup(arg);
      break;

    case 'b':
      config.pagesize = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'B':
      config.num_pages = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case ARGP_KEY_ARG:
    case ARGP_KEY_END:
      if (state->arg_num >= 1)
        /* Too many arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

void cli_parse(int argc, char **argv)
{
  // set default values
  config.local_hostname  = "127.0.0.1";
  config.remote_hostname = "127.0.0.1";
  config.port = 53100;
  config.bus = "./bus";
  config.num_pages = 256;
  config.pagesize = 64*1024;
  // parse arguments
  argp_parse (&argp, argc, argv, 0, 0, NULL);
}



typedef union felix_id_1 {
  uint64_t        fid;       // Felix Id
  // NOTE: all types same width to avoid some warning about bitfields crossing width of types.
  struct __attribute__((__packed__)) {
    uint64_t      sid :  8;  // Stream Id
    uint64_t      tid : 14;  // Transport Id, 6 bits used for elink, bit 7 set if is_to_flx
    uint64_t      lid : 14;  // Link Id
    uint64_t      cid : 16;  // Connector Id
    uint64_t      did :  8;  // Detector Id
    uint64_t      vid :  4;  // Version Id
  } b;
  struct __attribute__((__packed__)) {
    uint64_t      gid : 36;  // Generic ID
    uint64_t      co  : 28;  // Connector Offset
  } o;
  struct __attribute__((__packed__)) {
    uint64_t      xsid      : 8;  // Unused
    uint64_t      protocol  : 7;  // Protocol
    uint64_t      direction : 1;  // Virtual
    uint64_t      elink     : 19; // Elink
    uint64_t      vlink     : 1;  // Virtual
    uint64_t      xco       : 28; // Unused
  } e;
} felix_id_1_t;


uint32_t get_elink(uint64_t fid) {
  felix_id_1_t felix_id;
  felix_id.fid = fid;
  return felix_id.e.elink;
}


struct {
    struct netio_timer timer;
    struct timespec t0;
    int subscribed;
    int report;
    uint64_t tag_recorder[MAX_LINKS];
    uint64_t tag_present_in_buffer[MAX_LINKS];
    uint64_t tag_partial_recorder[MAX_LINKS];
    uint64_t received_buffers;
    uint64_t tags_per_buffer;
    uint64_t received_buffers_rate;
} stats;

void reset_counters(){
  for(unsigned int i=0; i<MAX_LINKS; ++i){
    stats.tag_recorder[i]=0;
    stats.tag_partial_recorder[i]=0;
    stats.tag_present_in_buffer[i]=0;
    stats.received_buffers=0;
    stats.tags_per_buffer=0;
  }
}

void try_subscribe()
{
  if (!config.subrequest_sent) {
    printf("attempting to subscribe to remote on %s:%u\n", config.socket.remote_hostname, config.socket.remote_port);
    for(unsigned int t=0; t<config.num_tags; t++) {
      printf("subscribing to tag 0x%lx\n", config.tags[t]);
      netio_subscribe(&config.socket, config.tags[t]);
      config.subrequest_sent = 1;
    }
  }
}


void on_connection_closed(struct netio_subscribe_socket* socket)
{
  printf("connection to publisher closed from %s:%u\n", socket->remote_hostname, socket->remote_port);
  stats.subscribed = 0;
  config.subrequest_sent = 0;
  // try to reconnect periodically
  netio_timer_start_s(&config.subscribe_timer, 1);
}


void on_connection_established(struct netio_subscribe_socket* socket) {
  printf("connection established to %s:%u\n", socket->remote_hostname, socket->remote_port);
  stats.subscribed = 1;
  config.subrequest_sent = 0;
  netio_timer_stop(&config.subscribe_timer);
}


void on_error_connection_refused(struct netio_subscribe_socket* socket)
{
  printf("connection refused for subscribing from %s:%u\n", socket->remote_hostname, socket->remote_port);
  config.subrequest_sent = 0;
  // retry to connect periodically
  netio_timer_start_s(&config.subscribe_timer, 1);
}


void on_buf_received(struct netio_subscribe_socket* socket, struct netio_buffer* buf, size_t len)
{
  for(unsigned int i=0; i<1024; ++i){stats.tag_partial_recorder[i]=0;}
  size_t pos = 0;
  while(pos < len) {
    uint32_t* s = (uint32_t*)((char *)buf->data + pos);
    pos += sizeof(uint32_t);
    void* single_msg_data = (char *)buf->data + pos;
    uint64_t fid = *((netio_tag_t*)single_msg_data);
    uint32_t elink = get_elink(fid);
    if(elink<MAX_LINKS){
        stats.tag_partial_recorder[elink]++;
    }
    else(printf("tag out of bounds %u\n", elink));
    pos += *s;
  }
  for(unsigned int i=0; i<MAX_LINKS; ++i){
    stats.tag_recorder[i]+=stats.tag_partial_recorder[i];
    stats.tag_present_in_buffer[i] += (stats.tag_partial_recorder[i] ? 1 : 0);
    stats.tags_per_buffer += (stats.tag_partial_recorder[i] ? 1 : 0);
  }
  stats.received_buffers++;
  stats.received_buffers_rate++;
}


void on_stats(void* ptr) {
  stats.report++;
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
  double seconds = t1.tv_sec - stats.t0.tv_sec
                   + 1e-9*(t1.tv_nsec - stats.t0.tv_nsec);
  double rate = stats.received_buffers_rate/1000./seconds;
  stats.received_buffers_rate=0;
  stats.t0 = t1;

  printf("===================================================================\n");
  printf("e-link \t actual pileup \t avg pileup | report #%d\n", stats.report);
  printf("-------------------------------------------------------------------\n");
  int printed=0;
  for(unsigned int i=0; i<MAX_LINKS; ++i){
    if(stats.tag_recorder[i]>0){
      printf("0x%x\t%lu\t %lu \n", i, stats.tag_recorder[i]/stats.tag_present_in_buffer[i],  stats.tag_recorder[i]/stats.received_buffers);
      printed++;
    }
  }
  printf("-------------------------------------------------------------------\n");
  printf("Info reported for %d e-links.\n", printed);
  printf("Buffer rate %2f KHz\n", rate);
  if(stats.received_buffers>0){
    printf("A buffer on average contains %lu e-links \n", stats.tags_per_buffer/stats.received_buffers);
  }
  printf("===================================================================\n");
  reset_counters();
}



void on_init()
{
  reset_counters();
  stats.report=0;

  struct netio_buffered_socket_attr attr;
  attr.num_pages = config.num_pages;
  attr.pagesize = config.pagesize;
  attr.watermark = 56*1024;
  attr.timeout_ms = 1000;

  printf("Opening subscribe socket from %s on %s:%u\n", config.local_hostname, config.remote_hostname, config.port);
  netio_subscribe_socket_init(&config.socket, &config.ctx, &attr, config.local_hostname, config.remote_hostname, config.port);
  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_error_connection_refused = on_error_connection_refused;
  config.socket.cb_buf_received = on_buf_received;

  netio_timer_init(&config.ctx.evloop, &stats.timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &stats.t0);
  stats.timer.cb = on_stats;
  netio_timer_start_s(&stats.timer, 4);

  // init time but don't start it now
  netio_timer_init(&config.ctx.evloop, &config.subscribe_timer);
  config.subscribe_timer.cb = try_subscribe;
  // attempt to subscribe
  try_subscribe();
}

void parse_bus(const char* bus_json)
{
  config.num_tags=0;
  char cmd[200];
  strcpy(cmd, "cat ");
  strcat(cmd, bus_json);
  strcat(cmd, " | grep -Po '\"fid\":.*?[^\\\\]\"' | awk -F':' '{print $2}' | tr -d ',' | tr -d '\"' > bus_tmp.txt\n");
  system(cmd);
  printf(cmd);
  FILE * in_file = fopen("bus_tmp.txt", "r");
  if (NULL == in_file){
    printf("Cannot open bus file, exit\n");
    return_code = 42;
    return;
  }
  unsigned int l = 0;
  uint64_t buf=0;
  while ( fscanf(in_file, "%lu", &buf) != EOF ){
    config.tags[l] = buf;
    printf("Adding link %u: %lu - 0x%16lx \n",  l, config.tags[l], config.tags[l]);
    config.num_tags++;
    l+=1;
  }
  fclose(in_file);
  printf("End of bus parsing\n");
}


int main(int argc, char** argv)
{
  cli_parse(argc, argv);
  parse_bus(config.bus);
  if(return_code){
    return return_code;
  }

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return return_code;
}
