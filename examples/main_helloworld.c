#include <stdio.h>
#include "netio/netio.h"

// NetIO context object that encapsulates the event loop
struct netio_context ctx;

void on_init() {
  // this callback is executed one time at the start of the event loop
  puts("Hello, NetIO!");
  netio_terminate(&ctx.evloop);
}

int main(int argc, char** argv) {
  // initialize netio context
  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;

  // run event loop
  netio_run(&ctx.evloop);

  return 0;
}
