#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "netio/netio.h"

#include "felixtag.h"

void __gcov_dump(void);

void my_handler(int signum) {
  printf("received signal\n");
#ifdef FELIX_COVERAGE
  __gcov_dump(); /* dump coverage data on receiving SIGINT */
#endif
}

#define MAX_MESSAGES_PER_FLOOD (200)

struct {
  char* hostname;
  unsigned port;
  int use_fid;
  netio_tag_t first_tag;
  netio_tag_t last_tag;

  struct netio_context ctx;
  struct netio_unbuffered_publish_socket socket;
  struct netio_buffer buf;
  struct netio_signal signal;

  int again;
  char* data;
  size_t datasize;
} config;

struct {
  struct netio_timer timer;
  struct timespec t0;
  uint64_t messages_sent;
  uint64_t total_messages_sent;
  uint64_t bytes_sent;
  uint64_t again;
  int nosub;
} stats;


static void
dump_fabric_info(struct fi_info* info)
{
  /* const char* info_str = */ fi_tostr(info, FI_TYPE_INFO);

  unsigned prov_version = info->fabric_attr->prov_version;
  unsigned api_version = info->fabric_attr->api_version;

  printf("libfabric provider: %s (version: %u.%u), api version: %u.%u \n",
           info->fabric_attr->prov_name,
           FI_MAJOR(prov_version), FI_MINOR(prov_version),
           FI_MAJOR(api_version), FI_MINOR(api_version));
  printf("fabric: %s ", info->fabric_attr->name);
}

uint64_t key = 0;
uint32_t counter = 0;

void flood() {
    for(int i=0; i<MAX_MESSAGES_PER_FLOOD; ++i){
      for(netio_tag_t tag=config.first_tag; tag<=config.last_tag; tag += config.use_fid ? 0x10000 : 1){
      struct iovec iov;
      iov.iov_base = config.data;
      iov.iov_len = config.datasize;
      // unsigned netio_flags = 0;

      if(config.again) {
        // netio_flags |= NETIO_REENTRY;
      }
      else{
        ++counter;
        key=(((uint64_t)tag) << 32)|counter;
      }
      int ret = netio_unbuffered_publishv(&config.socket,
                                tag,
                                &iov,
                                1,    // iov count
                                &key, // key
                                0,    // flags
                                NULL  // subscription cache
                              );
      if(ret == NETIO_STATUS_OK) {
        config.again = 0;
        stats.messages_sent += 1;
        stats.bytes_sent += config.datasize;
      } else if (ret == NETIO_STATUS_AGAIN) {
        config.again = 1;
      } else if (ret == NETIO_STATUS_OK_NOSUB) {
        config.again = 0;
      }
    }
  }
  netio_signal_fire(&config.signal);
}

void on_stats(void* ptr) {
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);

  stats.total_messages_sent += stats.messages_sent;
  double seconds = t1.tv_sec - stats.t0.tv_sec
                   + 1e-9*(t1.tv_nsec - stats.t0.tv_nsec);
  printf("data rate: %2.2f Gb/s  message rate: %2.2f kHz  total messages sent %lu\n",
         stats.bytes_sent*8/1024./1024./1024./seconds,
         stats.messages_sent/1000./seconds,
         stats.total_messages_sent
         );
  stats.bytes_sent = 0;
  stats.messages_sent = 0;
  stats.t0 = t1;
}

void on_subscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  printf("remote subscribed to tag 0x%lx\n", tag);
}

void on_unsubscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  printf("remote unsubscribed from tag 0x%lx\n", tag);
}

void on_connection_established(struct netio_unbuffered_publish_socket* socket)
{
  puts("connection to subscriber established");
  netio_signal_fire(&config.signal);
}

void on_connection_closed(struct netio_unbuffered_publish_socket* socket)
{
  puts("connection to subscriber closed");
}

void on_init()
{
  config.buf.size = 256*64*1024;
  config.buf.data = malloc(config.buf.size);

  printf("Opening publish socket on %s:%u\n", config.hostname, config.port);
  netio_unbuffered_publish_socket_init(&config.socket, &config.ctx, config.hostname, config.port, &config.buf);

  config.socket.cb_subscribe = on_subscribe;
  config.socket.cb_unsubscribe = on_unsubscribe;
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;

  if (!netio_tcp_mode(config.hostname))
  {
    dump_fabric_info(config.socket.lsocket.fi);
  }
  netio_signal_init(&config.ctx.evloop, &config.signal);
  config.signal.cb = flood;

  config.datasize = 8*1024;
  config.data = config.buf.data;
  for(unsigned i=0; i<config.datasize; i++) {
    config.data[i] = 'x';
  }

  //stats
  netio_timer_init(&config.ctx.evloop, &stats.timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &stats.t0);
  stats.timer.cb = on_stats;
  stats.bytes_sent = 0;
  stats.messages_sent = 0;
  stats.total_messages_sent = 0;
  stats.nosub = 0;
  config.again = 0;
  netio_timer_start_s(&stats.timer, 1);
}


int main(int argc, char** argv)
{
  if(argc != 5) {
    fprintf(stderr, "Usage: %s <hostname> <port> <first_tag> <last_tag>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  // Add ctrl-c handler
  struct sigaction new_action, old_action;
  new_action.sa_handler = my_handler;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_flags = 0;
  sigaction(SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN) {
    sigaction (SIGINT, &new_action, NULL);
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);
  config.first_tag = strtol(argv[3], NULL, 0);
  config.last_tag = strtol(argv[4], NULL, 0);
  config.use_fid = (config.first_tag & 0x1000000000000000) > 0L;
  printf("Publishing on links 0x%lx to 0x%lx \n", config.first_tag, config.last_tag);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;

  netio_run(&config.ctx.evloop);

  return 0;
}
