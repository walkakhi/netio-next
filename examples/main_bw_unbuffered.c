#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <argp.h>

#include "netio/netio.h"

const char *argp_program_version = "bw_unbuffered 0.1";
const char *argp_program_bug_address = "<https://its.cern.ch/jira/projects/FLX>";

static char doc[] = "Netio-next unbuffered point-to-point bandwidth test.\n"
                    "The sender registers a number of buffers, one per message."
                    " As the connection is established, the sender tries to send"
                    " all messages/buffers until EAGAIN. Sending resumes as a"
                    " buffer is returned by a completion object."
                    " The receiver posts a number of buffers equal to the sender.";

/* A description of the arguments we accept. */
static char args_doc[] = "";

/* The options we understand. */
static struct argp_option options[] = {
  {"client",   'c', 0,          0,  "Run in client mode. Default: server mode)" },
  {"hostname", 'H', "HOSTNAME", 0,  "Connect to/listen on the given examples/main_netio_performance.c. Default: localhost" },
  {"port",     'p', "PORT",     0,  "Connect to/listen on the given port. Default: 12345" },
  {"messages", 'n', "SIZE",     0,  "Number of NetIO messages to iterate over. Default: 256" },
  {"msgsize",  'm', "SIZE",     0,  "Message size to be used in the benchmark. Default: 4kB" },
  {"timeout",  'T', "SECONDS",  0,  "Number of seconds to run the benchmark. Default: 10" },
  { 0 }
};


struct {
  const char* hostname;
  unsigned port;

  unsigned max_iterations;
  unsigned num_messages;

  enum { SEND, RECV } mode;

  struct netio_context ctx;
  struct netio_send_socket send_socket;
  struct netio_listen_socket recv_socket;
  struct netio_timer timer;

  struct netio_buffer* buf;
  char* data_buf;
  size_t data_size;
} config;


struct {
  unsigned iterations;
  uint64_t messages;
  uint64_t bytes;
  uint64_t total_messages;
  uint64_t total_bytes;
  struct timespec t_start;
  struct timespec t0;
} counters;

int return_code = 55;

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char* end;

  switch (key)
    {
    case 'c':
      config.mode = SEND;
      break;

    case 'p':
      config.port = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'H':
      config.hostname = strdup(arg);
      break;

    case 'n':
      config.num_messages = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'T':
      config.max_iterations = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'm':
      config.data_size = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case ARGP_KEY_ARG:
    case ARGP_KEY_END:
      if (state->arg_num >= 1)
        /* Too many arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

void cli_parse(int argc, char **argv)
{
  // set default values
  config.hostname = "127.0.0.1";
  config.port = 12345;
  config.mode = RECV;
  config.num_messages = 256;
  config.data_size = 4096;
  config.max_iterations = 10;

  // parse arguments
  argp_parse (&argp, argc, argv, 0, 0, NULL);
}

// Forward Declarations
void on_timer(void* p);
void on_send_init();
void on_send_connection_established(struct netio_send_socket* socket);
void on_send_completed(struct netio_send_socket* socket, uint64_t key);
void on_recv_init();
void on_recv_connection_established(struct netio_recv_socket* socket);
void on_recv_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size);


void flood()
{
  for(unsigned i=0; i<config.num_messages; i++) {
    int ret = netio_send_buffer(&config.send_socket, &config.buf[i]);
    if (ret == NETIO_STATUS_AGAIN){
      break;
    }
  }
}

void init_timer()
{
  clock_gettime(CLOCK_MONOTONIC_RAW, &counters.t0);
  counters.t_start = counters.t0;
  netio_timer_start_ms(&config.timer, 1000);
}

// Callbacks
void on_timer(void* p)
{
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
  double seconds = t1.tv_sec - counters.t0.tv_sec
                   + 1e-9*(t1.tv_nsec - counters.t0.tv_nsec);

  double gbps = counters.bytes/(1000.*1000*1000)*8/seconds;
  double msgrate = counters.messages/(1024.*1024)/seconds;

  counters.total_messages += counters.messages;
  counters.total_bytes += counters.bytes;
  counters.messages = 0;
  counters.bytes = 0;

  printf("%u: timedelta: %f seconds   datarate: %f Gbps   msgrate: %f MHz\n", counters.iterations, seconds, gbps, msgrate);

  counters.t0 = t1;
  counters.iterations ++;

  if(counters.iterations >= config.max_iterations) {
    seconds = t1.tv_sec - counters.t_start.tv_sec
                     + 1e-9*(t1.tv_nsec - counters.t_start.tv_nsec);
    gbps = counters.total_bytes/(1000.*1000*1000)*8/seconds;
    msgrate = counters.total_messages/(1024.*1024)/seconds;
    printf("SUM: total time: %f seconds   average datarate: %f Gbps   average msgrate: %f MHz\n", seconds, gbps, msgrate);
    return_code = 0;
  }
}

void on_send_init()
{
  netio_init_send_socket(&config.send_socket, &config.ctx);
  netio_connect(&config.send_socket, config.hostname, config.port);
  config.send_socket.cb_connection_established = on_send_connection_established;
  config.send_socket.cb_send_completed = on_send_completed;
}

void on_send_connection_established(struct netio_send_socket* socket)
{
  for(unsigned b=0; b<config.num_messages; b++) {
    netio_register_send_buffer(socket, &config.buf[b], 0);
  }
  init_timer();
  flood();
}

void on_send_completed(struct netio_send_socket* socket, uint64_t key)
{
  struct netio_buffer* buf = (struct netio_buffer*)(key);
  counters.messages++;
  counters.bytes += config.data_size;
  netio_send_buffer(socket, buf);
}

void on_recv_init()
{    
  struct netio_unbuffered_socket_attr attr = {config.num_messages, config.data_size};
  netio_init_listen_socket(&config.recv_socket, &config.ctx, &attr);
  netio_listen(&config.recv_socket, config.hostname, config.port);
  config.recv_socket.cb_connection_established = on_recv_connection_established;
  config.recv_socket.cb_msg_received = on_recv_msg_received;
}

void on_recv_connection_established(struct netio_recv_socket* socket)
{
  init_timer();
}

void on_recv_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size)
{
  counters.messages ++;
  counters.bytes += size;
  netio_post_recv(socket, buf);
}


int main(int argc, char** argv)
{
  cli_parse(argc, argv);

  config.data_buf = malloc(config.data_size * config.num_messages);
  config.buf = malloc(config.num_messages * sizeof(struct netio_buffer));
  for(unsigned b=0; b < config.num_messages; b++) {
    for(unsigned i=0; i < config.data_size; i++) {
      config.data_buf[b*config.data_size+i] = 'a' + (i%26);
    }
    config.buf[b].data = &config.data_buf[b*config.data_size];
    config.buf[b].size = config.data_size;
  }

  netio_init(&config.ctx);
  if(config.mode == SEND) {
    config.ctx.evloop.cb_init = on_send_init;
  } else {
    config.ctx.evloop.cb_init = on_recv_init;
  }

  netio_timer_init(&config.ctx.evloop, &config.timer);
  config.timer.cb = on_timer;
  netio_run(&config.ctx.evloop);

  return return_code;
}
