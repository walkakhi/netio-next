/************************************************************************/
/*									*/
/* File: main_test_tcp.c						*/
/*									*/
/* This is the test program for the TCP support in netio-next		*/
/*									*/
/* 4. June. 21  M. Joos created						*/
/*									*/
/**************** C 2021 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "netio/netio.h"
#include "netio/netio_tcp.h"
#include "../src/log.h"

//Constants
# define MAX_BUFF_SIZE   1000000
# define BUFF_START_SIZE 1024

#define NUM_BUFFERS (256)
#define BUFFER_SIZE (1000000)

//Prototypes
void mainhelp(void);
void testtime(void);
void send_menu(void);
void recv_menu(void);
void on_send_init(void *ptr);
void print_delay(long int ts, long int tns, int size);
void on_recv_connection_established(struct netio_recv_socket* socket);
void on_recv_completed(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len);
void on_recv_completed2(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len);
void on_send_connection_established(struct netio_send_socket* socket);
void on_send_completed(struct netio_send_socket* socket, uint64_t key);
void on_send_completed2(struct netio_send_socket* socket, uint64_t key);
void on_send_connection_refused(struct netio_send_socket* socket);
void set_log(void);

//Constants
#define MAX_BUFFS 10

//Globals
static char hostname[] = "10.193.170.193";
static int port = 61617, mess_count = 0;
struct netio_context ctx;
struct netio_send_socket send_socket;
struct netio_listen_socket listen_socket;
struct netio_buffer send_databuf[MAX_BUFFS], recv_databuf[MAX_BUFFS];
int *send_databuffer[MAX_BUFFS], *recv_databuffer[MAX_BUFFS], mmode, marker = 0x10000000, log_level;
struct timespec time_data, time_data2;


/*******************************/
void main(int argc, char *argv[])
/*******************************/
{
  int fun = 1, loop;

  if(argc != 2) 
  {
    fprintf(stderr, "Usage: %s <log_level>\n", argv[0]);
    fprintf(stderr, "%d = LOG_DEBUG\n", LOG_DEBUG);
    fprintf(stderr, "%d = LOG_WARN\n", LOG_WARN);
    fprintf(stderr, "%d = LOG_INFO\n", LOG_INFO);
    fprintf(stderr, "%d = LOG_WARN\n", LOG_WARN);
    fprintf(stderr, "%d = LOG_ERROR\n", LOG_ERROR);
    fprintf(stderr, "%d = LOG_FATAL\n", LOG_FATAL);
    fprintf(stderr, "9 = Turn logging off\n");
    
    return;
  }
  
  log_level = atoi(argv[1]);
  
  if (log_level == 9)
  {
    log_set_quiet(true);
    printf("Logging is off\n");
  }  
  else 
    log_set_level(log_level);
  
  
  for(loop = 0; loop < MAX_BUFFS; loop++)
  {
    send_databuf[loop].size = BUFF_START_SIZE;
    send_databuffer[loop]   = malloc(MAX_BUFF_SIZE);
    send_databuf[loop].data = (void *)send_databuffer[loop];
  }

  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                           2 Test time measurement\n");
    printf("   3 Send menu                      4 Receive menu\n");      
    printf("   5 Set log level\n");      
    printf("   0 Exit\n");
    printf("Your choice: ");
    scanf("%d", &fun);
    if (fun == 1) mainhelp();
    if (fun == 2) testtime();
    if (fun == 3) send_menu();
    if (fun == 4) recv_menu();
    if (fun == 5) set_log();
  }  
    
  for(loop = 0; loop < MAX_BUFFS; loop++)
  {
    free(send_databuffer[loop]);
  }
}


/****************/
void set_log(void)
/****************/
{
  int level;
  
  printf("%d = LOG_TRACE\n", LOG_TRACE);
  printf("%d = LOG_DEBUG\n", LOG_DEBUG);
  printf("%d = LOG_WARN\n", LOG_WARN);
  printf("%d = LOG_INFO\n", LOG_INFO);
  printf("%d = LOG_WARN\n", LOG_WARN);
  printf("%d = LOG_ERROR\n", LOG_ERROR);
  printf("%d = LOG_FATAL\n", LOG_FATAL);
  printf("9 = Turn logging off\n");
  printf("Your choice: ");
  scanf("%d", &level);

  if (level == 9)
    log_set_quiet(true);
  else 
  {
    log_set_quiet(false);
    log_set_level(level);
  }
}


/*****************/
void mainhelp(void)
/*****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
}


/**************************/
void on_recv_init(void *ptr)
/**************************/
{
  struct netio_unbuffered_socket_attr attr = {NUM_BUFFERS, BUFFER_SIZE};
  netio_init_listen_tcp_socket(&listen_socket, &ctx, &attr);
  netio_listen_tcp(&listen_socket, hostname, port);
  
  listen_socket.cb_connection_established = on_recv_connection_established;
  
  if(mmode == 1)
    listen_socket.cb_msg_received = on_recv_completed;
  else
    listen_socket.cb_msg_received = on_recv_completed2;  

  printf("on_recv_init done\n");
}


/********************************************************************************************************/
void on_recv_completed(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len)
/********************************************************************************************************/
{
  int *maker_ptr;
  
  mess_count++;
  
  maker_ptr = (int *)buf[0].data;
  //printf("Message %d has a length of %d bytes. First word = 0x%08x\n", mess_count, (int)len, maker_ptr[0]);         

  if (maker_ptr[0] != marker + mess_count - 1)
  {
    printf("Receiver 1, Message %d has wrong first word 0x%08x (0x%08x expected)\n", mess_count, maker_ptr[0], marker + mess_count - 1);         
    exit(-1);
  }

  if (maker_ptr[len / 4 - 1] != marker + 0x10000000 + mess_count - 1)
  {
    printf("Receiver 1, Message %d has wrong last word 0x%08x\n", mess_count, maker_ptr[0]);         
    exit(-1);
  }
  
  clock_gettime(CLOCK_REALTIME, &time_data);
  netio_post_recv(socket, &buf[0]);
  clock_gettime(CLOCK_REALTIME, &time_data2);
  printf("Delay of netio_post_recv = %ld ns\n", time_data2.tv_nsec - time_data.tv_nsec);

  if (mess_count == 30)
  {
    printf("%d messages received\n", mess_count);
    netio_terminate(&ctx.evloop);
  }
}


/*********************************************************************************************************/
void on_recv_completed2(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len)
/*********************************************************************************************************/
{
  int *maker_ptr;
  static long int time_s = 0, time_n = 0;

  maker_ptr = (int *)buf[0].data;
  if (maker_ptr[0] != marker + mess_count)
  {
    printf("Receiver 1, Message %d has wrong first word 0x%08x (0x%08x expected)\n", mess_count, maker_ptr[0], marker + mess_count);         
    exit(-1);
  }

  if (maker_ptr[len / 4 - 1] != marker + 0x10000000 + mess_count)
  {
    printf("Receiver 1, Message %d has wrong last word 0x%08x\n", mess_count, maker_ptr[0]);         
    exit(-1);
  }
  
  //printf("Message %d received. length = %d bytes, marker = 0x%08x\n", mess_count, (int)len, maker_ptr[0]);         

  clock_gettime(CLOCK_REALTIME, &time_data);
  if (mess_count > 1)
    print_delay(time_s, time_n, len);

  time_s = time_data.tv_sec;
  time_n = time_data.tv_nsec;
  
  netio_post_recv(socket, &buf[0]);
  
  mess_count++;
  if (mess_count == 300)
  {
    printf("%d messages received\n", mess_count);
    netio_terminate(&ctx.evloop);
  }
}


/*******************************************************************/
void on_recv_connection_established(struct netio_recv_socket *socket)
/*******************************************************************/
{
  printf("on_recv_connection_established called\n");
}


/******************/
void send_menu(void)
/******************/
{
  printf("=========================================================================\n\n");
 
  mess_count = 0;
   
  printf("1 = measure delay of netio_send_buffer\n");
  printf("2 = measure total message delay\n");
  printf("3 = test multiple calls to netio_send\n");
  printf("Your choice: ");
  scanf("%d", &mmode);
        
  netio_init(&ctx);
  ctx.evloop.cb_init = on_send_init;
  
  netio_run(&ctx.evloop);
  printf("=========================================================================\n\n");
}


/******************/
void recv_menu(void)
/******************/
{  
  printf("=========================================================================\n\n");

  mess_count = 0;
  
  printf("1 = measure delay of netio_post_recv\n");
  printf("2 = measure total message delay\n");
  printf("3 = test multiple calls to netio_post_recv\n");
  printf("Your choice: ");
  scanf("%d", &mmode);  
  
  netio_init(&ctx);  
  ctx.evloop.cb_init = on_recv_init;
  netio_run(&ctx.evloop);

  printf("=========================================================================\n\n");
}


/*****************/
void testtime(void)
/*****************/
{
  int loop;

  clock_getres(CLOCK_REALTIME, &time_data);
  printf("CLOCK_REALTIME resolution = %ld ns\n", time_data.tv_nsec);
  
  for (loop = 0; loop < 10; loop++)
  {
    clock_gettime(CLOCK_REALTIME, &time_data);
    clock_gettime(CLOCK_REALTIME, &time_data2);
    printf("Time elapsed = %ld ns\n", time_data2.tv_nsec - time_data.tv_nsec);
  }
}

/**************************/
void on_send_init(void *ptr)
/**************************/
{
  netio_init_send_tcp_socket(&send_socket, &ctx);
  netio_connect_tcp(&send_socket, hostname, port);
  send_socket.cb_connection_established = on_send_connection_established;
  
  if(mmode == 1)
    send_socket.cb_send_completed = on_send_completed;
  else
    send_socket.cb_send_completed = on_send_completed2;
  
  send_socket.cb_error_connection_refused = on_send_connection_refused;
}


/*******************************************************************/
void on_send_connection_established(struct netio_send_socket* socket)
/*******************************************************************/
{
  struct netio_buffer test_databuf[10];

  if (mmode == 3)
  {
    for(int loop = 0; loop < 10; loop++)
    {
      test_databuf[loop].size = BUFF_START_SIZE;
      test_databuf[loop].data = (void *)malloc(MAX_BUFF_SIZE);
      netio_send_buffer(socket, &test_databuf[loop]);
    }
    exit(0);
  }
  
  send_databuffer[0][0]                            = 0x10000000;
  send_databuffer[0][send_databuf[0].size / 4 - 1] = 0x20000000;
  netio_send_buffer(socket, &send_databuf[0]);
  mess_count++;
  
  send_databuffer[1][0]                            = 0x10000001;
  send_databuffer[1][send_databuf[1].size / 4 - 1] = 0x20000001;
  netio_send_buffer(socket, &send_databuf[1]);
  mess_count++;
  
  send_databuffer[2][0]                            = 0x10000002;
  send_databuffer[2][send_databuf[2].size / 4 - 1] = 0x20000002;
  netio_send_buffer(socket, &send_databuf[2]);
  mess_count++;
}


/********************************************************************/
void on_send_completed(struct netio_send_socket* socket, uint64_t key)   //MJ: what is the "key" / what should it be?
/********************************************************************/
{
  //MJ: Note: in order to fully send a message, the on_send_completed callback has to be executed. Therefore this callback must not block
  
  if (mess_count == 31)
  {
    printf("%d messages sent.\n", mess_count - 1);
    netio_terminate(&ctx.evloop);
  }
  else
  {
    static int buff_index = 0;
    send_databuffer[3 + buff_index][0]                         = 0x10000000 + mess_count;
    send_databuffer[3 + buff_index][send_databuf[3 + buff_index].size / 4 - 1] = 0x20000000 + mess_count;

    clock_gettime(CLOCK_REALTIME, &time_data);
    netio_send_buffer(socket, &send_databuf[3 + buff_index]);
    clock_gettime(CLOCK_REALTIME, &time_data2);
    printf("Delay of netio_send_buffer = %ld ns\n", time_data2.tv_nsec - time_data.tv_nsec);

    buff_index++;
    if(buff_index == 3)
      buff_index = 0;   //Use buffers 3,4,5 in a round robin fashion. 

    mess_count++;
  }
}


/*********************************************************************/
void on_send_completed2(struct netio_send_socket* socket, uint64_t key)
/*********************************************************************/
{
  static int buff_index = 0;
  static long int time_s = 0, time_n = 0;
  
  if ((mess_count % 10) == 0)
  {
    static long int msize = BUFF_START_SIZE;
    msize = msize * 2;
    
    if (msize > (128 * BUFF_START_SIZE))
    {
      printf("%d messages sent\n", mess_count);
      netio_terminate(&ctx.evloop);
    }
        
    printf("mess_count = %d, Setting buffer size to %ld bytes\n", mess_count, msize);
    send_databuf[3 + buff_index].size = msize;
  }
  
  send_databuffer[3 + buff_index][0]                         = 0x10000000 + mess_count;
  send_databuffer[3 + buff_index][send_databuf[3 + buff_index].size / 4 - 1] = 0x20000000 + mess_count;
  
  clock_gettime(CLOCK_REALTIME, &time_data);
  if (mess_count > 1)
    print_delay(time_s, time_n, send_databuf[3 + buff_index].size);

  time_s = time_data.tv_sec;
  time_n = time_data.tv_nsec;

  netio_send_buffer(socket, &send_databuf[3 + buff_index]);

  buff_index++;
  if(buff_index == 3)
    buff_index = 0;   //Use buffers 3,4,5 in a round robin fashion. 

  mess_count++;
}


/***************************************************************/
void on_send_connection_refused(struct netio_send_socket* socket)
/***************************************************************/
{
  printf(">>>>>>>>>>>>connection refused<<<<<<<<<<<<<<<<<");
}


/***************************************************/
void print_delay(long int ts, int long tns, int size)
/***************************************************/
{
  long int total_time, ds, dns;
  float mbps;
  
  clock_gettime(CLOCK_REALTIME, &time_data);
  ds  = time_data.tv_sec  - ts;
  dns = time_data.tv_nsec - tns;
  total_time = ds * 1000000000 + dns;

  mbps = (float)size / (float)total_time * 1000.0;

  if (total_time > 10000000)
    printf("Delay of message = %6ld milli seconds. size = %8d bytes, transfer performance = %7.1f Mbytes/s\n", total_time / 1000000, size, mbps);
  else if (total_time > 10000)
    printf("Delay of message = %6ld micro seconds. size = %8d bytes, transfer performance = %7.1f Mbytes/s\n", total_time / 1000, size, mbps);
  else
    printf("Delay of message = %6ld nano seconds. size = %8d bytes, transfer performance = %7.1f Mbytes/s\n", total_time, size, mbps);
}














