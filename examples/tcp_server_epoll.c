/****************************************************************/
/*								*/
/*  This is a stand-alone TCP-IP server and serves as a 	*/
/*  playgroud for testing ideas that may later end up in 	*/
/*  netio-next							*/
/*								*/
/*  5. Feb. 21  M. Joos  created				*/
/*								*/
/***********C 2021 - A nickel program worth a dime***************/

/*
TO-DO:
- Add IPv6 support, if required
*/


#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <sys/socket.h>       


#define MAXLINE   4096     //max text line length
#define SERV_PORT 3001     //port
#define LISTENQ   8        //maximum number of client connections
#define MAXEVENTS 4        //for epoll


/************************************/
void make_socket_non_blocking(int sfd)
/************************************/
{
  int flags, s;

  flags = fcntl(sfd, F_GETFL, 0);
  if (flags == -1)
  {
    perror ("fcntl");
    abort();
  }

  flags |= O_NONBLOCK;
  s = fcntl(sfd, F_SETFL, flags);
  if (s == -1)
  {
    perror ("fcntl");
    abort();
  }
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int do_echo = 0, loop, listenfd, ret, efd, infd, done = 0, i;
  socklen_t in_len;
  char buf[MAXLINE], hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
  struct sockaddr_in servaddr;
  struct sockaddr in_addr;
  struct epoll_event event, *events;

  if (argc == 2)
  {
    int acti;
    if ((acti = getopt(argc, argv, "e")) == -1)
    {
      printf("Invalid option\n");
      exit(-1);
    }
  
    if (acti == 'e')
    {
      do_echo = 1;
      printf("Received data will be echo-ed back to sender\n");
    }
  }

  //Create a socket
  if ((listenfd = socket (AF_INET, SOCK_STREAM, 0)) < 0) 
  {
    perror("Problem in creating the socket");
    exit(2);
  }

  //preparation of the socket address
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port        = htons(SERV_PORT);
  
  printf("SERV_PORT        = %d\n", SERV_PORT);
  printf("htons(SERV_PORT) = %d\n", htons(SERV_PORT));
 
  bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));  //bind the socket
  
  in_len = sizeof servaddr;
  ret = getnameinfo((struct sockaddr *) &servaddr, in_len, hbuf, sizeof hbuf, sbuf, sizeof sbuf, 0);
  if (ret == 0)
    printf("Accepted connection on descriptor %d (host=%s, service=%s)\n", listenfd, hbuf, sbuf);
  
  make_socket_non_blocking(listenfd);  //Make the socket non-blocking
  
  listen(listenfd, LISTENQ);                                        //listen to the socket by creating a connection queue, then wait for clients
  printf("Server running...waiting for connections.\n");
  
  //Create the epoll file descriptor and connect it to the TCP/IP socket
  efd = epoll_create1(0);   //Instead of "0" we could pass "EPOLL_CLOEXEC" to the function. Check later which one makes more sense
  if (efd == -1)
  {
    perror ("epoll_create");
    abort();
  }
  printf("epoll created\n");

  event.data.fd = listenfd;
  event.events = EPOLLIN | EPOLLET;                    //MJ: Do we want an edge trigger (EPOLLET) or a level trigger?
  ret = epoll_ctl(efd, EPOLL_CTL_ADD, listenfd, &event);
  if (ret == -1)
  {
    perror ("epoll_ctl");
    abort ();
  }

  events = calloc(MAXEVENTS, sizeof event);    // Buffer where events are returned
  
  printf("entering while loop\n"); 
  while(1)
  {
    int n = epoll_wait(efd, events, MAXEVENTS, -1);   
    printf("epoll_wait returns n = %d\n", n); 
    for (i = 0; i < n; i++)
    {
      if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP) || (!(events[i].events & EPOLLIN)))
      {
	fprintf(stderr, "epoll error\n");
	close(events[i].data.fd);
	continue;
      }
      else if (events[i].data.fd == listenfd)    // We have a notification on the listening socket, which means one or more incoming connections.
      {
        printf("Epoll has signalled listenfd\n");
	while(1)  //MJ: why do we need the while loop? 
	{
	  in_len = sizeof in_addr;
	  infd = accept(listenfd, &in_addr, &in_len);
	  if (infd == -1)
	  {
	    if ((errno == EAGAIN) || (errno == EWOULDBLOCK))   	  // We have processed all incoming connections.
  	      break;
	    else
	    {
	      perror ("accept");
	      break;
	    }
	  }

	  ret = getnameinfo(&in_addr, in_len, hbuf, sizeof hbuf, sbuf, sizeof sbuf, 0);
	  if (ret == 0)
   	    printf("Accepted connection on descriptor %d (host=%s, service=%s)\n", infd, hbuf, sbuf);

	  make_socket_non_blocking(infd);            // Make the incoming socket non-blocking and add it to the list of fds to monitor

	  event.data.fd = infd;                      // This seems to overwrite listenfd (that we don't need anymore?)
	  event.events = EPOLLIN | EPOLLET;
	  ret = epoll_ctl(efd, EPOLL_CTL_ADD, infd, &event);
	  if (ret == -1)
	  {
	    perror ("epoll_ctl");
	    abort();
	  }
	}
	continue;
      }
      else    // We have data on the fd waiting to be read. Read and display it. We must read whatever data is available completely, as we are running in edge-triggered mode
              // and won't get a notification again for the same data
      {
        printf("Epoll has signalled infd\n");

	while ((n = recv(events[i].data.fd, buf, MAXLINE, 0)) > 0)  
	{
	  printf("String of %d characters received from the client:\n", n);
	  puts(buf);
	  if (do_echo)
          {
            printf("String of %d characters sent to the client:\n", n);
            send(events[i].data.fd, buf, n, 0);
          }
	}

	for(loop = 0; loop < MAXLINE; loop++)
	  buf[loop] = 0;

	if (n < 0)	    
	  printf("Waiting for more data to arrive...\n");

	if (done)
	{
	  printf ("Closed connection on descriptor %d\n", events[i].data.fd);

	  // Closing the descriptor will make epoll remove it from the set of descriptors which are monitored
	  close (events[i].data.fd);
	}
      }
    }
  }

  free(events);
}























/*
=======================================================
Instructions for running tcp_server / tcp_client 


$# first run the server 

$ ./tcp_server
	$ Server running...waiting for connections.

$ #Now connect using the localhost address 127.0.0.1 and then type something
$ # the control C out of the client and ps and kill the server

$ ./tcp_client pcepese48.cern.ch
	Received request...
	Hello CS23!
	String received from and resent to the client:Hello CS23!

String received from the server: Hello CS23!
*/








//epoll example (https://web.archive.org/web/20120504033548/https://banu.com/blog/2/how-to-use-epoll-a-complete-example-in-c/)
