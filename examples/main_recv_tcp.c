#include <stdio.h>
#include <time.h>
#include "netio/netio.h"
#include "netio/netio_tcp.h"

//#define TEST_IT
#ifdef TEST_IT
# define DEBUG_LOG( ... ) do { printf("[recv@%s:%3d] ", __FILE__, __LINE__); printf(__VA_ARGS__); printf("\n"); fflush(stdout); } while(0)
#else
# define DEBUG_LOG( ... )
#endif

#define NUM_BUFFERS 1 //MJ: Does it make sense for TCP to have more than 1 buffer?
#define BUFFER_SIZE 1000000 


struct 
{
  int senders;
  char* hostname;
  unsigned port;

  struct netio_context ctx;
  struct netio_listen_socket socket;
  struct netio_listen_socket socket2;
} config;

struct 
{
  struct netio_timer timer;
  struct timespec t0;
  uint64_t messages_received;
  uint64_t bytes_received;
} statistics;


// Forward declarations
void on_connection_established(struct netio_recv_socket*);
void on_connection_established2(struct netio_recv_socket*);
void on_msg_received(struct netio_recv_socket*, struct netio_buffer* buf, void* data, size_t len);
void on_msg_received2(struct netio_recv_socket*, struct netio_buffer* buf, void* data, size_t len);
void on_stats(void* ptr);


//Globals
int mess_count;


// Callbacks
/*********************/
void on_init(void *ptr)
/*********************/
{
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  on_init called\n");
  struct netio_unbuffered_socket_attr attr = {NUM_BUFFERS, BUFFER_SIZE};

  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Calling netio_init_listen_tcp_socket\n");
  netio_init_listen_tcp_socket(&config.socket, &config.ctx, &attr);
  
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Calling netio_listen_tcp\n");
  netio_listen_tcp(&config.socket, config.hostname, config.port);
  
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Registering callbacks\n");
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_msg_received           = on_msg_received;

  if(config.senders == 2)
  {
    printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>222  Calling netio_init_listen_tcp_socket\n");
    netio_init_listen_tcp_socket(&config.socket2, &config.ctx, &attr);
  
    printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>222  Calling netio_listen_tcp\n");
    netio_listen_tcp(&config.socket2, config.hostname, config.port + 1);
  
    printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>222  Registering callbacks\n");
    config.socket2.cb_connection_established = on_connection_established2;
    config.socket2.cb_msg_received           = on_msg_received2;
  }

  //print provider so we can easy check if using TCP or RDMA
  //printf("Listening connection at %s:%d. Attributes: name=%s; prov_name=%s \n", config.hostname, config.port, config.socket.fi->fabric_attr->name, config.socket.fi->fabric_attr->prov_name);

  //mj temporary clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
  //mj temporary statistics.timer.cb = on_stats;
  //mj temporary netio_timer_start_ms(&statistics.timer, 1000);
  
  mess_count = 0;
  
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  on_init done\n");
}


/**************************************************************/
void on_connection_established(struct netio_recv_socket* socket)
/**************************************************************/
{
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  on_connection_established called\n");
}


/***************************************************************/
void on_connection_established2(struct netio_recv_socket* socket)
/***************************************************************/
{
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>222  on_connection_established called\n");
}



/******************************************************************************************************/
void on_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len)
/******************************************************************************************************/
{
  int *maker_ptr;
  mess_count++;
  
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  on_msg_received called with len = %d for message %d\n", (int)len, mess_count);
  statistics.messages_received++;
  statistics.bytes_received += len;         

  maker_ptr = (int *)buf[0].data;
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  Markers are 0x%08x and 0x%08x\n", maker_ptr[0], maker_ptr[(len / 4) - 1]);

  netio_post_recv(socket, &buf[0]);
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  on_msg_received done\n");
}



/*******************************************************************************************************/
void on_msg_received2(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len)
/*******************************************************************************************************/
{
  int *maker_ptr;

  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>222  on_msg_received called with len = %d\n", (int)len);
  statistics.messages_received++;
  statistics.bytes_received += len;

  maker_ptr = (int *)buf[0].data;
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>222  Markers are 0x%08x and 0x%08x\n", maker_ptr[0], maker_ptr[(len / 4) - 1]);

  netio_post_recv(socket, &buf[0]);
  printf(">>>>>>>>>>>>>>>>>>>>>>>>>>>>222  on_msg_received done\n");
}


/**********************/
void on_stats(void* ptr)
/**********************/
{
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
  double seconds = t1.tv_sec - statistics.t0.tv_sec + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
  printf("data rate: %2f Gb/s   message rate: %2f kHz\n",
  statistics.bytes_received*8/1024./1024./1024./seconds,
  statistics.messages_received/1000./seconds);
  statistics.bytes_received = 0;
  statistics.messages_received = 0;
  statistics.t0 = t1;
}


/*****************************/
int main(int argc, char** argv)
/*****************************/
{
  if(argc != 4) 
  {
    fprintf(stderr, "Usage: %s <hostname> <port> <#senders>\n", argv[0]);
    return 1;
  }

  config.hostname = argv[1];
  config.port     = atoi(argv[2]);
  config.senders  = atoi(argv[3]);
  
  netio_init(&config.ctx);  
  config.ctx.evloop.cb_init = on_init;
  netio_timer_init(&config.ctx.evloop, &statistics.timer);
  netio_run(&config.ctx.evloop);

  return 0;
}
