#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <argp.h>

#include "netio/netio.h"

const char *argp_program_version = "bw_buffered 0.1";
const char *argp_program_bug_address = "<https://its.cern.ch/jira/projects/FLX>";

static char doc[] = "Netio-next buffered point-to-point bandwidth test.\n"
                    "The sender sends full buffers until ibverbs returns EAGAIN"
                    " because all buffers have been exhausted. Sending resumes"
                    " once a buffer becomes available until the next EAGAIN.\n"
                    "NOTE: messages are coalesced into buffers.";

/* A description of the arguments we accept. */
static char args_doc[] = "";

/* The options we understand. */
static struct argp_option options[] = {
  {"client",       'c', 0,         0,  "Run in client mode. Default: server mode)" },
  {"hostname",     'H', "HOSTNAME",0,  "Connect to/listen on the given IP. Default: localhost" },
  {"port",         'p', "PORT",    0,  "Connect to/listen on the given port. Default: 12345" },
  {"pages",        'B', "SIZE",    0,  "Number of netio pages. Default: 256" },
  {"pagesize",     'b', "SIZE",    0,  "Netio page size in Byte. Default: 64kB" },
  {"pagemark",     'w', "SIZE",    0,  "Netio page watermark in Byte for sender. Default: 63kB." },
  {"pagetimeout",  't', "SIZE",    0,  "Netio page timeout for sender in ms. Default: no timeout." },
  {"msgsize",      'm', "SIZE",    0,  "Message size to be used in the benchmark (client side). Default: 4kB" },
  {"timeout",      'T', "SECONDS", 0,  "Number of seconds to run the benchmark. Default: 10" },
  { 0 }
};

struct {
  const char* hostname;
  unsigned port;

  unsigned max_iterations;

  enum { SEND, RECV } mode;

  struct netio_context ctx;
  struct netio_buffered_send_socket send_socket;
  struct netio_buffered_listen_socket recv_socket;
  struct netio_buffered_socket_attr attr;
  struct netio_timer timer;

  char* data_buf;
  size_t data_size;
} config;


struct {
  unsigned iterations;
  uint64_t messages;
  uint64_t bytes;
  uint64_t total_messages;
  uint64_t total_bytes;
  struct timespec t_start;
  struct timespec t0;
} counters;

int return_code = 55;

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char* end;

  switch (key)
    {
    case 'c':
      config.mode = SEND;
      break;

    case 'p':
      config.port = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'H':
      config.hostname = strdup(arg);
      break;

    case 'b':
      config.attr.pagesize = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'B':
      config.attr.num_pages = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'w':
      config.attr.watermark = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 't':
      config.attr.timeout_ms = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;      

    case 'T':
      config.max_iterations = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'm':
      config.data_size = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case ARGP_KEY_ARG:
    case ARGP_KEY_END:
      if (state->arg_num >= 1)
        /* Too many arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

void cli_parse(int argc, char **argv)
{
  // set default values
  config.hostname = "127.0.0.1";
  config.port = 12345;
  config.mode = RECV;
  config.attr.num_pages = 256;
  config.attr.pagesize = 64*1024;
  config.attr.watermark = 63*1024;
  config.attr.timeout_ms = 0;
  config.data_size = 4096;
  config.max_iterations = 10;

  // parse arguments
  argp_parse (&argp, argc, argv, 0, 0, NULL);
  if (config.data_size  > config.attr.pagesize){
    printf("Message size %zu too large, setting to maximum value %zu equal to buffer size.\n", config.data_size, config.attr.pagesize);
    config.data_size = config.attr.pagesize;
  }
}

// Forward Declarations
void on_timer(void* p);
void on_send_init();
void on_send_connection_established(struct netio_buffered_send_socket* socket);
void on_send_buffer_available(void* p);
void on_recv_init();
void on_recv_connection_established(struct netio_buffered_recv_socket* socket);
void on_recv_msg_received(struct netio_buffered_recv_socket* socket, void* data, size_t size);


void flood()
{
  while(1) {
    int result = netio_buffered_send(&config.send_socket, config.data_buf, config.data_size);
    if(result == NETIO_STATUS_OK) {
      counters.messages++;
      counters.bytes += config.data_size;
    } else {
      if(result == NETIO_STATUS_AGAIN) {
        return;
      } else {
        fprintf(stderr, "error: send failed\n");
        return_code = 1;
        return;
      }
    }

  }
}

void init_timer()
{
  clock_gettime(CLOCK_MONOTONIC_RAW, &counters.t0);
  counters.t_start = counters.t0;
  netio_timer_start_ms(&config.timer, 1000);
}

// Callbacks
void on_timer(void* p)
{
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
  double seconds = t1.tv_sec - counters.t0.tv_sec
                   + 1e-9*(t1.tv_nsec - counters.t0.tv_nsec);

  double gbps = counters.bytes/(1000.*1000*1000)*8/seconds;
  double msgrate = counters.messages/(1024.*1024)/seconds;

  counters.total_messages += counters.messages;
  counters.total_bytes += counters.bytes;
  counters.messages = 0;
  counters.bytes = 0;

  printf("%u: timedelta: %f seconds   datarate: %f Gbps   msgrate: %f MHz\n", counters.iterations, seconds, gbps, msgrate);

  counters.t0 = t1;
  counters.iterations ++;

  if(counters.iterations >= config.max_iterations) {
    seconds = t1.tv_sec - counters.t_start.tv_sec
                     + 1e-9*(t1.tv_nsec - counters.t_start.tv_nsec);
    gbps = counters.total_bytes/(1000.*1000*1000)*8/seconds;
    msgrate = counters.total_messages/(1024.*1024)/seconds;
    printf("SUM: total time: %f seconds   average datarate: %f Gbps   average msgrate: %f MHz\n", seconds, gbps, msgrate);
    return_code = 0;
    return;
  }
}

void on_send_init()
{
  netio_buffered_send_socket_init(&config.send_socket, &config.ctx, &config.attr);
  netio_buffered_connect(&config.send_socket, config.hostname, config.port);
  config.send_socket.cb_connection_established = on_send_connection_established;
  config.send_socket.signal_buffer_available.cb = on_send_buffer_available;
}

void on_send_connection_established(struct netio_buffered_send_socket* socket)
{
  init_timer();
  flood();
}

void on_send_buffer_available(void* p)
{
  flood();
}

void on_recv_init()
{
  netio_buffered_listen_socket_init(&config.recv_socket, &config.ctx, &config.attr);
  netio_buffered_listen(&config.recv_socket, config.hostname, config.port);
  config.recv_socket.cb_connection_established = on_recv_connection_established;
  config.recv_socket.cb_msg_received = on_recv_msg_received;
}

void on_recv_connection_established(struct netio_buffered_recv_socket* socket)
{
  init_timer();
}

void on_recv_msg_received(struct netio_buffered_recv_socket* socket, void* data, size_t size)
{
  counters.messages ++;
  counters.bytes += size;
}


int main(int argc, char** argv)
{

  cli_parse(argc, argv);

  netio_init(&config.ctx);
  if(config.mode == SEND) {
    config.ctx.evloop.cb_init = on_send_init;
    config.data_buf = (char*)malloc(config.data_size);
    for(unsigned i=0; i < config.data_size; i++) {
      config.data_buf[i] = 'a' + (i%26);
    }
  } else {
    config.ctx.evloop.cb_init = on_recv_init;
  }

  config.attr.watermark = config.attr.pagesize-config.data_size-1;

  netio_timer_init(&config.ctx.evloop, &config.timer);
  config.timer.cb = on_timer;
  netio_run(&config.ctx.evloop);

  return return_code;
}
