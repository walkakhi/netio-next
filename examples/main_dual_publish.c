#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "netio/netio.h"
#include "netio/netio_tcp.h"


#include "felixtag.h"

struct {
  char* hostname;
  unsigned data_port;
  unsigned dcs_port;
  netio_tag_t data_tag;
  netio_tag_t dcs_tag;
  unsigned delay;
  unsigned long timeout;
  unsigned long messages;
  unsigned long sent_messages;

  struct netio_context ctx;
  struct netio_publish_socket dcs_socket;
  struct netio_publish_socket data_socket;

  struct netio_timer timer;

  char* data;
  size_t datasize;
} config;

int data_connected;
int dcs_connected;

void on_subscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  if (tag==config.data_tag) {
    data_connected=1;
  }
  else if (tag==config.dcs_tag) {
    dcs_connected=1;
  }
  printf("Remote subscribed to tag 0x%lx\n", tag);
}


void on_connection_established(struct netio_publish_socket* socket)
{
  printf("connection to subscriber established\n");
  printf("Starting to publish every %u milliseconds\n", config.delay);
  netio_timer_start_ms(&config.timer, config.delay);
}


void on_connection_closed(struct netio_publish_socket* socket)
{
  printf("connection to subscriber closed\n");
}


void on_timer(void* ptr)
{
  config.data[0] = (char)(config.sent_messages+1);
  if(data_connected) {
     netio_buffered_publish(&config.data_socket, config.data_tag, config.data, config.datasize, 0, NULL);
     if(!config.timeout){
       netio_buffered_publish_flush(&config.data_socket, 0, NULL);
     }
  }
  if (dcs_connected) {
     netio_buffered_publish(&config.dcs_socket, config.dcs_tag, config.data, config.datasize, 0, NULL);
     if(!config.timeout){
       netio_buffered_publish_flush(&config.dcs_socket, 0, NULL);
     }
  }
  ++config.sent_messages;
  if(config.messages){
    if(config.messages == config.sent_messages){
      printf("%lu messages sent.\n", config.messages);
      netio_timer_stop(&config.timer);
    }
  }
}


void on_init()
{
  struct netio_buffered_socket_attr attr;
  attr.num_pages = 256;
  attr.pagesize = 64*1024;
  attr.watermark = 56*1024;
  attr.timeout_ms =  config.timeout;

  printf("Opening tcp publish socket on %s:%u\n", config.hostname, config.dcs_port);
  netio_publish_tcp_socket_init(&config.dcs_socket, &config.ctx, config.hostname, config.dcs_port, &attr);
  config.dcs_socket.cb_subscribe = on_subscribe;
  config.dcs_socket.cb_connection_established = on_connection_established;
  config.dcs_socket.cb_connection_closed = on_connection_closed;

  printf("Opening libfabric publish socket on %s:%u\n", config.hostname, config.data_port);
  netio_publish_socket_init(&config.data_socket, &config.ctx, config.hostname, config.data_port, &attr);
  config.data_socket.cb_subscribe = on_subscribe;
  config.data_socket.cb_connection_established = on_connection_established;
  config.data_socket.cb_connection_closed = on_connection_closed;

  config.datasize = 40;
  config.data = (char*)malloc(config.datasize);
  for(unsigned i=0; i<config.datasize; i++) {
    config.data[i] = 'x';
  }
}


int main(int argc, char** argv)
{

  if(argc < 7) {
    fprintf(stderr, "Usage: %s <hostname> <data-port> <dcs-port> <data-tag> <dcs-tag> <delay ms> [<timeout> <messages>]\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.hostname = argv[1];
  config.data_port = atoi(argv[2]);
  config.dcs_port = atoi(argv[3]);
  config.data_tag = strtol(argv[4], NULL, 0);
  config.dcs_tag = strtol(argv[5], NULL, 0);
  config.delay = atoi(argv[6]);
  config.timeout = 0;
  if(argc>7){
    config.timeout = atoi(argv[7]);
    printf("Setting the timeout to %lu\n", config.timeout);
  }
  config.messages = 0;
  config.sent_messages = 0;
  if(argc>8){
    config.messages = atoi(argv[8]);
    printf("Sending %lu\n", config.messages);
  }

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;

  netio_timer_init(&config.ctx.evloop, &config.timer);
  config.timer.cb = on_timer;

  netio_run(&config.ctx.evloop);

  return 0;
}
