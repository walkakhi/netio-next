#include <stdio.h>

#include "netio/netio.h"

#include "felixtag.h"

struct netio_context ctx;
struct netio_timer timer;

void on_timer(void* ptr) {
  int* ctr = (int*)ptr;
  printf("%d\n", (*ctr)--);
  if(*ctr == 0) {
    netio_terminate(&ctx.evloop);
  }
}

int main(int argc, char** argv) {
  int counter = 10;

  netio_init(&ctx);
  netio_timer_init(&ctx.evloop, &timer);
  timer.cb = on_timer;
  timer.data = &counter;
  netio_timer_start_s(&timer, 1);

  // run event loop
  netio_run(&ctx.evloop);

  return 0;
}
