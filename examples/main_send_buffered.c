#include <stdio.h>
#include <string.h>
#include <math.h>

#include "netio/netio.h"

#include "felixtag.h"

#define NUM_PAGES (256)
#define WATERMARK (63*1024)
#define PAGESIZE (64*1024)

struct {
  char* hostname;
  unsigned port;

  struct netio_context ctx;
  struct netio_buffered_send_socket socket;
  struct netio_buffered_socket_attr attr;

  struct netio_timer timer;
  const char* data;
  size_t len;

  uint64_t messages;
} config;

struct {
    uint64_t total_messages_sent;
} statistics;

// Forward declarations
void on_connection_established(struct netio_buffered_send_socket*);
void on_connection_closed(struct netio_buffered_send_socket*);
void on_error_connection_refused(struct netio_buffered_send_socket*);
void on_timer(void* ptr);

// Callbacks
void on_init(void* ptr)
{
  printf("on_init\n");
  config.data = "Helloworld";
  config.len = strlen(config.data);

  config.attr.num_pages = NUM_PAGES;
  config.attr.pagesize = PAGESIZE;
  config.attr.watermark = WATERMARK;
  config.attr.timeout_ms = 1000;

  netio_buffered_send_socket_init_and_connect(&config.socket, &config.ctx, config.hostname, config.port, &config.attr);
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_error_connection_refused = on_error_connection_refused;

  config.timer.cb = on_timer;
}

void on_connection_established(struct netio_buffered_send_socket* socket)
{
  printf("connection established\n");
  netio_timer_start_us(&config.timer, 1000);
}

void on_connection_closed(struct netio_buffered_send_socket* socket) {
  printf("on_connection_closed\n");
  if ((statistics.total_messages_sent >= config.messages) && (config.messages > 0)) {
    netio_terminate(&config.ctx.evloop);
  }
}

void on_error_connection_refused(struct netio_buffered_send_socket* socket) {
  printf("on_error_connection_refused\n");
}

void on_timer(void *ptr) {
  // printf("on_timer\n");
  for (int i=0; i<1000000; i++) {
    netio_buffered_send(&config.socket, (void*)config.data, config.len);
    statistics.total_messages_sent++;
    if ((statistics.total_messages_sent >= config.messages) && (config.messages > 0)) {
      break;
    }
  }
  netio_buffered_flush(&config.socket);

  if ((statistics.total_messages_sent >= config.messages) && (config.messages > 0)) {
    printf("Done sending\n");
    netio_timer_stop(&config.timer);
  }
}

int main(int argc, char** argv)
{
  if (argc < 3 || argc > 4) {
    fprintf(stderr, "Usage: %s <hostname> <port> [messages>=0]\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);

  config.messages = 0;
  if (argc >= 4) {
    config.messages = atoi(argv[3]);
  }

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_timer_init(&config.ctx.evloop, &config.timer);
  netio_run(&config.ctx.evloop);

  return 0;
}
