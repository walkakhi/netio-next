#include "netio.h"
#include "log.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>


int is_socket_list_empty(struct netio_socket_list* list)
{
    if (list == NULL) {
        return 1;
    } else {
        return 0;
    }
}


struct netio_socket_list* find_socket_by_address(struct netio_socket_list* list, void* addr, size_t addrlen, int port)
{
    if (is_socket_list_empty(list) == 1){
        return NULL;
    }
    struct netio_socket_list* iterator  = list;
    while(iterator){
        if( (iterator->addrlen == addrlen) && (iterator->port == port) && (memcmp(iterator->addr, addr, addrlen) == 0) ){
            return iterator;
        } else {
            iterator = iterator->next;
        }
    }
    return NULL;
}


struct netio_socket_list* find_socket(struct netio_socket_list* list, void* socket)
{
    if (is_socket_list_empty(list) == 1){
        return NULL;
    }
    struct netio_socket_list* iterator  = list;
    while(iterator){
        if(iterator->socket == socket){
            return iterator;
        } else {
            iterator = iterator->next;
        }
    }
    return NULL;
}


struct netio_socket_list* add_socket(struct netio_socket_list** list, int type)
{
    return add_socket_with_address(list, type, NULL, 0, 0);
}


struct netio_socket_list* add_socket_with_address(struct netio_socket_list** list, int type, void* addr, size_t addrlen, int port)
{
    //check if entry already exists
    if(addrlen > 0){
        struct netio_socket_list* item = find_socket_by_address(*list, addr, addrlen, port);
        if (item != NULL){
            return item;
        }
    }

    struct netio_socket_list* new_item = malloc(sizeof(struct netio_socket_list));

    switch( type )
    {
        case BSEND:
            new_item->socket = malloc(sizeof(struct netio_buffered_send_socket));
            break;
        case USEND:
            new_item->socket = malloc(sizeof(struct netio_send_socket));
            break;
        case BRECV:
            new_item->socket = malloc(sizeof(struct netio_buffered_recv_socket));
            break;
        case URECV:
            new_item->socket = malloc(sizeof(struct netio_recv_socket));
            break;
        case TESTSOCKET:
            new_item->socket = malloc(sizeof(uint64_t));
            break;
        default :
            log_error("Type of socket %d not supported by socket list", type);
            free(new_item);
            return NULL;
    }
    //address
    if(addrlen > 0){
        new_item->addr = malloc(addrlen);
        memcpy(new_item->addr, addr, addrlen);
        new_item->addrlen = addrlen;
        new_item->port = port;
    } else {
        new_item->addr = NULL;
        new_item->addrlen = 0;
        new_item->port = 0;
    }
    new_item->next = *list;
    *list = new_item;
    return new_item;
}


int remove_socket(struct netio_socket_list** list, void* socket)
{
    int ret = 1;
    struct netio_socket_list* iterator  = *list;
    struct netio_socket_list* previous  = NULL;

    while(iterator){
        if(iterator->socket == socket){
            ret = 0;
            if(previous){
                previous->next = iterator->next;
            } else { //in this case we remove the first element
                *list = iterator->next;
            }
            if(iterator->addrlen > 0){
                free(iterator->addr);
                iterator->addr = NULL;
            }
            free(iterator->socket);
            iterator->socket = NULL;
            free(iterator);
            break;
        }
        else{
            previous = iterator;    
            iterator = iterator->next;
        }
    }
    return ret;
}


int free_socket_list(struct netio_socket_list** list)
{
    struct netio_socket_list* iterator  = *list;
    int count = 0;
    while (iterator) {
        if(iterator->addrlen > 0){
            free(iterator->addr);
            iterator->addr = NULL;
        }
        free(iterator->socket);
        iterator->socket = NULL;
        struct netio_socket_list* tmp = iterator;
        iterator = iterator->next;
        free(tmp);
        ++count;
    }
    *list = NULL;
    return count;
}


int print_sockets(struct netio_socket_list* list)
{
    int counter = 0;
    struct netio_socket_list* iterator = list;
    while(iterator){
        log_info("Element %d socket addr %p \n", counter, iterator->socket);
        iterator = iterator->next;
        ++counter;
    }
    return counter;
}
