#include <string.h>
#include "netio/netio.h"


static void
check_and_trigger(struct netio_semaphore* sem)
{
  if(sem->threshold > 0 && sem->current >= sem->threshold) {
    if(sem->cb) {
      sem->cb(sem->data);
    }
    sem->threshold = 0; // semaphores only fire once
  }
}

void netio_semaphore_init(struct netio_semaphore* sem, unsigned threshold)
{
  memset(sem, 0, sizeof(*sem));
  sem->threshold = threshold;
  sem->current = 0;
}


void netio_semaphore_increment(struct netio_semaphore* sem, unsigned n)
{
  sem->current += n;
  check_and_trigger(sem);
}


void netio_semphore_set_threshold(struct netio_semaphore* sem, unsigned t)
{
  sem->threshold = t;
  check_and_trigger(sem);
}
