#pragma once

void on_send_socket_libfabric_cm_event(int fd, void* ptr);
void on_send_socket_tcp_cm_event(int fd, void* ptr);

void on_listen_socket_libfabric_cm_event(int fd, void* ptr);
void on_listen_socket_tcp_cm_event(int fd, void* ptr);

void on_recv_socket_libfabric_cm_event(int fd, void* ptr);
void on_recv_socket_tcp_cm_event(int fd, void* ptr);

void on_buffered_listen_socket_libfabric_cm_event(int fd, void* ptr);
void on_buffered_listen_socket_tcp_cm_event(int fd, void* ptr);

void on_buffered_recv_socket_libfabric_cm_event(int fd, void* ptr);
void on_buffered_recv_socket_tcp_cm_event(int fd, void* ptr);
