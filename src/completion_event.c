#include <stdio.h>
#include "log.h"
#include "netio/netio.h"
#include "netio/netio_tcp.h"
#include <assert.h>

#include "completion_event.h"
#include "log.h"

#if defined DEBUG || defined DEBUG_CQ
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define log_dbg(...) log_log(LOG_DEBUG, __FILENAME__, __LINE__, __VA_ARGS__)
#define log_trc(...) log_log(LOG_TRACE, __FILENAME__, __LINE__, __VA_ARGS__)
#else
#define log_dbg(...)
#define log_trc(...)
#endif

// PRIVATE FUNCTIONS ///////////////////////////////////////////////////////////

void
on_recv_socket_cq_event(int fd, void* ptr)
{
  struct netio_recv_socket* socket = (struct netio_recv_socket*)ptr;

  if(socket->cqfd < 0) {
    log_info("on_recv_socket_cq_event called for unconnected socket.");
    return;
  }

  log_info("recv socket CQ max %lu", socket->cq_size);
  struct fi_cq_data_entry completion_entry[NETIO_MAX_CQ_EVENTS];
  int ret = fi_cq_read(socket->cq, &completion_entry, socket->cq_size);
  log_info("recv socket fd %d: %d completion events", fd, ret);

  if(ret < 0)
  {
    if(ret == -FI_EAGAIN){
      struct fid* fp = &socket->cq->fid;
      fi_trywait(socket->lsocket->fabric, &fp, 1);
      return;
    }
    else if(ret == -FI_EAVAIL){
      int r;
      struct fi_cq_err_entry err_entry;
      if((r = fi_cq_readerr(socket->cq, &err_entry, 0)) < 0)
      {
        log_error("Failed to retrieve details on Completion Queue error of recv socket, error %d: %s", r, fi_strerror(-r));
      }
      log_error("code %d reading Completion Queue of recv socket: %s", err_entry.err, fi_strerror(err_entry.err));
      log_error("Provider-specific error %d: %s", err_entry.prov_errno, fi_cq_strerror(socket->cq, err_entry.prov_errno, err_entry.err_data, NULL, 0));
      if(err_entry.err == FI_EIO) {
        // I/O error, the CM event code can handle this
        log_error("Send socket CQ I/O error, connection possibly closed: ignored");
        return;
      }
      if(err_entry.err == FI_ECANCELED) {
        // Operation Cancelled
        log_error("Send socket CQ operation cancelled.");
        return;
      }
    }
    else{
      log_error("Recv socket unhandled Completion Queue error %d: %s", ret, fi_strerror(-ret));
    }
  }
  else{
    for(unsigned int i = 0; i < ret; ++i){
      struct netio_buffer* buffer = (struct netio_buffer*)completion_entry[i].op_context;
      void* data = buffer->data; //completion_entry.buf;
      size_t size = completion_entry[i].len;
      if(completion_entry[i].flags & FI_REMOTE_CQ_DATA) {
        log_info("Completion has remote CQ data");
        uint64_t imm = completion_entry[i].data;
        log_info("recv completion immediate data: 0x%lx", imm);
        if(socket->lsocket->cb_msg_imm_received) {
          socket->lsocket->cb_msg_imm_received(socket, buffer, data, size, imm);
        } else if(socket->lsocket->cb_msg_received) {
          socket->lsocket->cb_msg_received(socket, buffer, data, size);
        }
      } else {
        log_info("Completion has NO remote CQ data");
        if(socket->lsocket->cb_msg_received) {
          socket->lsocket->cb_msg_received(socket, buffer, data, size);
        }
      }
      /*if(completion_entry.flags & FI_MULTI_RECV) {
        DEBUG_LOG("FI_MULTI_RECV was set, buffer has to be reposted");
      }*/
      // FLX-1194, posting buffers to be done by USER !!
      // netio_post_recv(socket, buffer);
    }
  }
}

void
on_send_socket_cq_event(int fd, void* ptr)
{
    struct netio_send_socket* socket = (struct netio_send_socket*)ptr;
    if(socket->state != CONNECTED || socket->cqfd < 0) {
      log_info("on_send_socket_cq_event called for unconnected socket.");
      return;
    }
    log_info("send socket CQ max %lu", socket->cq_size);
    struct fi_cq_data_entry completion_entry[NETIO_MAX_CQ_EVENTS];
    int ret = fi_cq_read(socket->cq, &completion_entry, socket->cq_size);
    log_info("send socket fd %d: %d completion events", fd, ret);

    if(ret < 0)
    {
      if(ret == -FI_EAGAIN){ //If no completions are available to return from the CQ, -FI_EAGAIN will be returned.
        struct fid* fp = &socket->cq->fid;
        fi_trywait(socket->domain->fabric, &fp, 1);
        return;
      }
      else if(ret == -FI_EAVAIL)
      {
        int r;
        struct fi_cq_err_entry err_entry;
        if((r = fi_cq_readerr(socket->cq, &err_entry, 0)) < 0)
        {
            log_error("Failed to retrieve details on Completion Queue error of send socket, error %d: %s", r, fi_strerror(-r));
        }
        log_error("Completion Queue read error %d of send socket: %s", err_entry.err, fi_strerror(err_entry.err));
        log_error("Provider-specific error %d: %s", err_entry.prov_errno, fi_cq_strerror(socket->cq, err_entry.prov_errno, err_entry.err_data, NULL, 0));
        if(err_entry.err == FI_EIO) {
          // I/O error, the CM event code can handle this
          log_error("Send socket CQ I/O error, connection possibly closed: ignored");
          return;
        }
        if(err_entry.err == FI_ECANCELED) {
          // Operation Cancelled
          log_error("Send socket CQ operation cancelled.");
          return;
        }
        log_error("Send socket Completion Queue unhandled specific read error %d: %s", err_entry.err, fi_strerror(err_entry.err));
      }
      else{
        log_error("Send socket Completion Queue unhandled read error %d: %s", ret, fi_strerror(-ret));
      }
    }
    else{
      for(unsigned int i=0; i < ret; ++i){
        uint64_t key = (uint64_t)completion_entry[i].op_context;
        log_info("Send completed. Immediate data 0x%lx key 0x%lx", (uint64_t)completion_entry[i].data, key);
        if(socket->cb_send_completed) {
          socket->cb_send_completed(socket, key);
        }
      }
    }
}


// TCP equivalent of on_send_socket_cq_event()
// Called when epoll_wait is unblocked because the TCP socket has something to say
void
on_send_socket_tcp_cq_event(int fd, void *ptr)
{  
  log_info("signal from the POSIX FD %d received, ptr 0x%08x. Calling netio_tcp_send_on_signal", fd, ptr);
  netio_tcp_send_on_signal(ptr);

  log_info("done");
}


// TCP equivalent of on_recv_socket_cq_event
void
on_recv_socket_tcp_cq_event(int fd, void* ptr)
{
  log_info("signal from the POSIX FD %d received. Calling netio_tcp_recv_on_signal", fd);
  netio_tcp_recv_on_signal(ptr);
  log_info("done");
}
