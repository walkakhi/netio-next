#include <stdio.h>
#include <unistd.h>
#include "log.h"
#include "netio/netio.h"
#include "netio/netio_tcp.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <assert.h>

#include "connection_event.h"
#include "completion_event.h"
#include "log.h"

#if defined DEBUG || defined DEBUG_CM
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define log_dbg(...) log_log(LOG_DEBUG, __FILENAME__, __LINE__, __VA_ARGS__)
#define log_trc(...) log_log(LOG_TRACE, __FILENAME__, __LINE__, __VA_ARGS__)
#else
#define log_dbg(...)
#define log_trc(...)
#endif

#define FATAL(msg, c) \
do { \
    log_fatal("%s %d: %s", msg, c, fi_strerror(-c)); \
    exit(2); \
} while(0);


// STATIC FUNCTIONS ////////////////////////////////////////////////////////////

static int
read_cm_event(struct fid_eq* eq, struct fi_info** info, struct fi_eq_err_entry* err_entry)
{
    uint32_t event;
    struct fi_eq_cm_entry entry;

    ssize_t rd = fi_eq_sread(eq, &event, &entry, sizeof entry, 0, 0);
    if(rd < 0)
    {
        if(rd == -FI_EAGAIN)
        {
            return rd;
        }
        if(rd == -FI_EAVAIL)
        {
            int r;
            if((r = fi_eq_readerr(eq, err_entry, 0)) < 0)
            {
                FATAL("Failed to retrieve details on Event Queue error", r);
            }
            log_error("Event Queue error: %s (code: %d), provider specific: %s (code: %d)",
                fi_strerror(err_entry->err), err_entry->err,
                fi_eq_strerror(eq, err_entry->prov_errno, err_entry->err_data, NULL, 0),
                err_entry->prov_errno);
            return rd;
        }
    }
    if (rd != sizeof entry)
    {
        FATAL("Failed to read from Event Queue: %d", (int)rd);
    }

    if(info != NULL)
        *info = entry.info;

    return event;
}


static void
handle_connreq(struct netio_recv_socket* rsocket, struct netio_listen_socket* lsocket, struct fi_info *info, void (*cb)(int,void*), void* cbdata)
{
    int ret;
    struct fi_eq_attr eq_attr = {10000, 0, FI_WAIT_FD, 0, 0};

    if((ret = fi_domain(lsocket->fabric, info, &rsocket->domain, NULL)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot open fabric, error ", ret);
    }

    if((ret = fi_endpoint(rsocket->domain, info, &rsocket->ep, NULL)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot open endpoint, error ", ret);
    }

    /* Create a new event queue for the new active socket */
    if((ret = fi_eq_open(lsocket->fabric, &eq_attr, &rsocket->eq, NULL)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot open Event Queue, error ", ret);
    }

    if((ret = fi_ep_bind((rsocket->ep), &rsocket->eq->fid, 0)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot bind Event Queue to endpoint, error ", ret);
    }

    if((ret = fi_control(&rsocket->eq->fid, FI_GETWAIT, &rsocket->eqfd)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket failed to obtain Event Queue wait object", ret);
    }
    rsocket->eq_ev_ctx.fd = rsocket->eqfd;
    rsocket->eq_ev_ctx.data = cbdata;
    rsocket->eq_ev_ctx.cb = cb;

    log_info("Adding RECV EQ polled fid %d %p", rsocket->eqfd, &rsocket->eq->fid);
    add_polled_fid(&rsocket->ctx->evloop.pfids, lsocket->fabric, &rsocket->eq->fid, rsocket->eqfd, &rsocket, cb);
    add_open_fd(&rsocket->ctx->evloop.openfds, rsocket->eqfd, NETIO_EQ, URECV, &rsocket);
    netio_register_read_fd(&rsocket->ctx->evloop, &rsocket->eq_ev_ctx);
    log_info("recv_socket: EQ fd %d waiting for connection", rsocket->eqfd);

    struct fi_cq_attr cq_attr;
    cq_attr.size = NETIO_MAX_CQ_ENTRIES;      /* # entries for CQ */
    cq_attr.flags = 0;     /* operation flags */
    cq_attr.format = FI_CQ_FORMAT_DATA;    /* completion format */
    cq_attr.wait_obj= FI_WAIT_FD;  /* requested wait object */
    cq_attr.signaling_vector = 0; /* interrupt affinity */
    cq_attr.wait_cond = FI_CQ_COND_NONE; /* wait condition format */
    cq_attr.wait_set = NULL;  /* optional wait set */

    //FI_RECV CQ
    if((ret = fi_cq_open(rsocket->domain, &cq_attr, &rsocket->cq, NULL)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot open Completion Queue, error ", ret);
    }

    if((ret = fi_ep_bind((rsocket->ep), &rsocket->cq->fid, FI_RECV)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot bind Completion Queue to endpoint, error ", ret);;
    }

    //FI_TRANSMIT CQ - also necessary
    cq_attr.format = FI_CQ_FORMAT_UNSPEC;
    cq_attr.wait_obj= FI_WAIT_NONE;
    if((ret = fi_cq_open(rsocket->domain, &cq_attr, &rsocket->tcq, NULL)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot open Completion Queue, error ", ret);
    }

    if((ret = fi_ep_bind((rsocket->ep), &rsocket->tcq->fid, FI_TRANSMIT)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot bind Completion Queue to endpoint, error ", ret);;
    }

    if((ret = fi_enable(rsocket->ep)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, cannot enable recv socket endpoint, error ", ret);
    }

    if((ret = fi_accept(rsocket->ep, NULL, 0)))
    {
        fi_reject(lsocket->pep, info->handle, NULL, 0);
        FATAL("Listen socket, connection rejected, error ", ret);;
    }
    log_info("connection accepted. Lsocket EQ: %d with evloop %d, rsocket EQ %d with evloop %d", lsocket->eqfd, lsocket->ctx->evloop.epollfd, rsocket->eqfd, rsocket->ctx->evloop.epollfd);
}


void
handle_recv_socket_shutdown(struct netio_recv_socket* socket)
{
    if(socket->eqfd < 0){
        log_info("handle_recv_socket_shutdown on closed socket (eqfd %d)", socket->eqfd);
        return;
    }
    int ret = 0;
    struct epoll_event ep_event; /* needed only for kernel <2.6.9  */
    log_info("Handle_recv_socket_shutdown for socket %p, evloop: %d", socket, socket->ctx->evloop.epollfd);

    if(socket->ep && (ret = fi_close(&socket->ep->fid)))
    {
        log_error("Failed to close recv socket Endpoint %d: %s", ret, fi_strerror(-ret));
    }
    log_info("fi_close done for endpoint.");

    //EQ
    remove_polled_fid(&socket->ctx->evloop.pfids, socket->eq_ev_ctx.fd);
    ret = epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->eq_ev_ctx.fd, &ep_event);
    if ( ret ){ log_warn("netio_recv_socket: cannot remove EQ fd %d from evloop %d: %s", socket->eq_ev_ctx.fd, socket->ctx->evloop.epollfd, strerror(errno));}
    log_info("netio_recv_socket: removing EQ fd %d from evloop %d", socket->eq_ev_ctx.fd, socket->ctx->evloop.epollfd);
    if(socket->eq && (ret = fi_close(&socket->eq->fid)))
    {
        log_error("Failed to fi_close recv socket Event Queue %d: %s", ret, fi_strerror(-ret));
        ret = close(socket->eq_ev_ctx.fd);
        if ( ret ) {log_warn("Cannot close recv socket EQ fd %d: %s", socket->eq_ev_ctx.fd, strerror(errno));}
    }
    remove_open_fd(&socket->ctx->evloop, socket->eq_ev_ctx.fd);
    socket->eqfd = -1;

    //FI_RECV CQ
    remove_polled_fid(&socket->ctx->evloop.pfids, socket->cq_ev_ctx.fd);
    ret = epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->cq_ev_ctx.fd, &ep_event);
    if ( ret ){ log_warn("netio_recv_socket: cannot remove CQ fd %d from evloop %d: %s", socket->cq_ev_ctx.fd, socket->ctx->evloop.epollfd, strerror(errno));}
    log_info("netio_recv_socket: removing CQ fd %d from evloop %d", socket->cq_ev_ctx.fd, socket->ctx->evloop.epollfd);
    if(socket->cq && (ret = fi_close(&socket->cq->fid)))
    {
        log_error("Failed to close recv socket Completion Queue %d: %s", ret, fi_strerror(-ret));
        ret = close(socket->cq_ev_ctx.fd);
        if ( ret ) {log_warn("Cannot close recv socket CQ fd %d: %s", socket->cq_ev_ctx.fd, strerror(errno));}
    }
    remove_open_fd(&socket->ctx->evloop, socket->cq_ev_ctx.fd);
    socket->cqfd = -1;
    //FI_TRANSMIT CQ
    if((ret = fi_close(&socket->tcq->fid)))
    {
        log_error("Failed to close FI_TRANSMIT recv socket Completion Queue %d: %s", ret, fi_strerror(-ret));
    }

    //MR
    uint16_t mem_regions = socket->reg_mr;
    for(uint16_t i=0; i<mem_regions; ++i)
    {
        if ((ret = fi_close(socket->mr[i]))){
            log_warn("Failed to close recv Memory Region %d, error %d.", i, ret);
        }
        else{
            socket->reg_mr-=1;
        }
    }
    if(socket->reg_mr==0){free(socket->mr);socket->mr=NULL;}
    if((ret = fi_close(&socket->domain->fid)))
    {
        log_error("Failed to close recv socket Domain %d: %s", ret, fi_strerror(-ret));
    }
    if (socket->sub_msg_buffers != NULL){
        for(int i = 0; i < 32; i++){
            free(socket->sub_msg_buffers[i]->data);
            free(socket->sub_msg_buffers[i]);
        }
        free(socket->sub_msg_buffers);
        socket->sub_msg_buffers = NULL;
    }
}

void
handle_send_socket_shutdown(struct netio_send_socket* socket)
{
    // if(socket->state != CONNECTED){
    //     log_info("Nothing to do, send socket has already been freed....");
    //     return;
    // }
    int ret = 0;
    struct epoll_event ep_event; /* needed only for kernel <2.6.9  */
    log_info("Handle_send_socket_shutdown. Socket with EQ: %d", socket->eqfd);

    if(socket->ep && (ret = fi_close(&socket->ep->fid)))
    {
        log_error("Failed to close send socket Endpoint %d: %s", ret, fi_strerror(-ret));
    }
    socket->ep = NULL;

    //EQ
    log_info("netio_send_socket: removing EQ fd %d from evloop %d", socket->eqfd, socket->ctx->evloop.epollfd);
    remove_polled_fid(&socket->ctx->evloop.pfids, socket->eqfd);
    ret = epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->eqfd, &ep_event);
    if ( ret ){ log_warn("netio_send_socket: cannot remove EQ fd %d from evloop %d: %s", socket->eqfd, socket->ctx->evloop.epollfd, strerror(errno));}
    if(socket->eq && (ret = fi_close(&socket->eq->fid)))
    {
        log_error("Failed to close send socket Event Queue %d: %s", ret, fi_strerror(-ret));
        ret = close(socket->eq_ev_ctx.fd);
        if ( ret ) { log_warn("Cannot close send socket eq fd %d, %s", socket->eq_ev_ctx.fd, strerror(errno));}
    }
    remove_open_fd(&socket->ctx->evloop, socket->eqfd);
    socket->eqfd = -1;

    //FI_TRANSMIT CQ
    log_info("netio_send_socket: removing CQ fd %d from evloop %d", socket->cq_ev_ctx.fd, socket->ctx->evloop.epollfd);
    remove_polled_fid(&socket->ctx->evloop.pfids, socket->cqfd);
    ret = epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->cqfd, &ep_event);
    if ( ret ){ log_warn("netio_send_socket: cannot remove CQ fd %d from evloop %d: %s", socket->cqfd, socket->ctx->evloop.epollfd, strerror(errno));}
    if(socket->cq && (ret = fi_close(&socket->cq->fid)))
    {
        log_error("Failed to close send socket Completion Queue %d: %s", ret, fi_strerror(-ret));
        ret = close(socket->cq_ev_ctx.fd);
        if ( ret ){ log_warn("Could not close send socket cq fd %d: %s", socket->cq_ev_ctx.fd, strerror(errno));}
    }
    remove_open_fd(&socket->ctx->evloop, socket->cqfd);
    socket->cqfd = -1;
    //FI_RECV CQ
    if((ret = fi_close(&socket->rcq->fid)))
    {
        log_error("Failed to close FI_RECV senf socket Completion Queue %d: %s", ret, fi_strerror(-ret));
    }

    socket->domain->nb_sockets -= 1;
    if (socket->domain->nb_sockets == 0) {
        for(unsigned int i=0; i < socket->domain->reg_mr; ++i) {
            if ((ret = fi_close(socket->domain->mr[i]))) {
                log_warn("Failed to close send Memory Region %d, error %d.", i, ret);;
            }
        }
        if ((ret = fi_close(&socket->domain->domain->fid))) {
            log_error("Failed to close send socket Domain %d: %s", ret, fi_strerror(-ret));
        }

        if(socket->ctx->evloop.pfids.count == 0){
            if ((ret = fi_close(&socket->domain->fabric->fid))) {
                log_error("Failed to close send socket Fabric %d: %s", ret, fi_strerror(-ret));
            }
        }
        free(socket->domain->mr);
        socket->domain->mr = NULL;
        free(socket->domain);
        socket->domain = NULL;
    }
    if(socket->fi != NULL){
        fi_freeinfo(socket->fi);
        socket->fi = NULL;
    }
    socket->state = DISCONNECTED;
}


void
handle_tcp_send_socket_shutdown(struct netio_send_socket* socket)
{
    netio_signal_close(&socket->ctx->evloop, &socket->tcp_signal);
    epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->eq_ev_ctx.fd, NULL);
    int rc = close(socket->eq_ev_ctx.fd);
    if ( !rc ){ remove_open_fd(&socket->ctx->evloop, socket->eq_ev_ctx.fd); }
}


static void
handle_send_socket_shutdown_on_connection_refused(struct netio_send_socket* socket)
{
    int ret = 0;
    //struct fi_eq_err_entry err_entry;
    struct epoll_event ep_event; /* needed only for kernel <2.6.9  */
    if(socket->ep && (ret = fi_close(&socket->ep->fid)))
    {
        log_error("Failed to close send socket endpoint %d: %s", ret, fi_strerror(-ret));
    }

    //EQ
    remove_polled_fid(&socket->ctx->evloop.pfids, socket->eqfd);
    ret =  epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->eqfd, &ep_event);
    if (ret) {log_warn("Conn refused: cannot deregister send_socket EQ fd %d from evloop %d, %s", socket->eqfd, socket->ctx->evloop.epollfd, strerror(errno));}
    if(socket->eq && (ret = fi_close(&socket->eq->fid)))
    {
        log_error("Failed to close send socket Event Queue %d: %s", ret, fi_strerror(-ret));
        ret = close(socket->eq_ev_ctx.fd);
        if ( ret ) { log_warn("Cannot close send socket eq fd %d, %s", socket->eq_ev_ctx.fd, strerror(errno));}
    }
    remove_open_fd(&socket->ctx->evloop, socket->eqfd);
    socket->eqfd = -1;

    //FI_TRANSMIT CQ - wait object not retrieved yet
    if(socket->cq && (ret = fi_close(&socket->cq->fid)))
    {
        log_error("Failed to close FI_TRANSMIT send socket Completion Queue %d: %s", ret, fi_strerror(-ret));
    }
    //FI_RECV CQ
    if(fi_close(&socket->rcq->fid))
    {
        log_error("Failed to close FI_RECV send socket Completion Queue %d: %s", ret, fi_strerror(-ret));
    }

    socket->domain->nb_sockets -= 1;
    if (socket->domain->nb_sockets == 0) {
        for(unsigned int i=0; i < socket->domain->reg_mr; ++i) {
            if ((ret = fi_close(socket->domain->mr[i]))) {
                log_warn("Failed to close send Memory Region %d, error %d.", i, ret);
            }
        }
        if ((ret = fi_close(&socket->domain->domain->fid))) {
            log_error("Failed to close send socket Domain %d: %s", ret, fi_strerror(-ret));
        }
        if(socket->ctx->evloop.pfids.count == 0){
            if ((ret = fi_close(&socket->domain->fabric->fid))) {
                log_error("Failed to close send socket Fabric %d: %s", ret, fi_strerror(-ret));
            }
        }
        free(socket->domain->mr);
        socket->domain->mr = NULL;
        free(socket->domain);
        socket->domain = NULL;

    }
    if(socket->fi != NULL){
        fi_freeinfo(socket->fi);
        socket->fi = NULL;
    }
    socket->state = UNCONNECTED;
}

// WAIT OBJECT CALLBACKS ///////////////////////////////////////////////////////////

void
on_send_socket_libfabric_cm_event(int fd, void* ptr)
{
    struct netio_send_socket* socket = (struct netio_send_socket*)ptr;
    struct fi_eq_err_entry err_entry;
    log_info("send socket %p fd %d: connection event, evloop: %d", socket, fd, socket->ctx->evloop.epollfd);

    if(socket->state == DISCONNECTED || socket->eqfd != fd){
        log_info("discarding send_socket cm events: state disconnected or fd mismatch eqfd %d fd %d", socket->eqfd, fd);
        return;
    } //Check if event queue was already closed. TODO: Need to fix receiving messages after closing
    int event = read_cm_event(socket->eq, NULL, &err_entry);
    int ret = 0;

    log_info("event %d", event);

    switch(event)
    {
    case FI_SHUTDOWN:
        if (socket->eqfd < 0 ){
            log_info("Ignoring FI_SHUTDOWN on send_socket, invalid eqfd (socket already closed)");
            return;
        }
        log_info("fi_verbs_process_send_socket_cm_event: FI_SHUTDOWN");
        if(socket->cb_internal_connection_closed){
            socket->cb_internal_connection_closed(socket);
        }
        if(socket->cb_connection_closed) {
            socket->cb_connection_closed(socket);
        }
        return;

    case FI_CONNECTED:
        socket->cqfd = -1;
        if((ret = fi_control(&socket->cq->fid, FI_GETWAIT, &socket->cqfd)))
        {
            FATAL("Failed to retrieve wait object for send socket Completion Queue", ret);
        }

        socket->cq_ev_ctx.fd = socket->cqfd;
        socket->cq_ev_ctx.data = socket;
        socket->cq_ev_ctx.cb = on_send_socket_cq_event;

        log_info("send_socket: EQ fd %d connected, CQ fd %d", socket->eqfd, socket->cqfd);
        log_info("Adding SEND CQ polled fid %d %p", socket->cqfd, &socket->cq->fid);
        add_polled_fid(&socket->ctx->evloop.pfids, socket->domain->fabric, &socket->cq->fid, socket->cqfd, socket, on_send_socket_cq_event);
        add_open_fd(&socket->ctx->evloop.openfds, socket->cqfd, NETIO_CQ, USEND, socket);
        netio_register_read_fd(&socket->ctx->evloop, &socket->cq_ev_ctx);

        socket->state = CONNECTED;

        if(socket->cb_connection_established) {
          socket->cb_connection_established(socket);
        }

        return;

    case FI_MR_COMPLETE:
    case FI_AV_COMPLETE:
    case FI_JOIN_COMPLETE:
        // Not implemented
        break;


    case -FI_EAVAIL:

        switch(err_entry.err) {

            case FI_ECONNREFUSED:

                log_info("Connection refused (FI_ECONNREFUSED). Deallocating send_socket resources.");

                if(socket->eqfd < 0){
                    log_info("FI_ECONNREFUSED on send socket with EQ fd %d. Not clearing it.", socket->eqfd);
                    return;
                }

                handle_send_socket_shutdown_on_connection_refused(socket);

                if(socket->cb_error_connection_refused) {
                    socket->cb_error_connection_refused(socket);
                }

                if (socket->recv_socket != NULL){
                    if (socket->recv_socket->eqfd < 0 ){
                        log_info("Ignoring FI_ETIMEDOUT on recv_socket, invalid eqfd (socket already closed)");
                        return;
                    }
                    log_info("Shutting down receive socket on FI_ETIMEDOUT");
                    handle_recv_socket_shutdown(socket->recv_socket);
                    if(socket->recv_socket->lsocket->cb_connection_closed) {
                        socket->recv_socket->lsocket->cb_connection_closed(socket->recv_socket);
                    }
                    if (socket->recv_socket->lsocket->attr.num_buffers > 0){
                        for(int i = 0; i < socket->recv_socket->lsocket->attr.num_buffers; i++){
                            free(socket->recv_socket->recv_buffers[i].data);
                        }
                        free(socket->recv_socket->recv_buffers);
                    }
                }

                //print_openfds(&socket->ctx->evloop.openfds);
                break;

            case FI_ETIMEDOUT:
                log_info("fi_verbs_process_send_socket_cm_event: FI_ETIMEDOUT");
                if (socket->eqfd < 0 ){
                    log_info("Ignoring FI_SHUTDOWN on send_socket, invalid eqfd (socket already closed)");
                    break;
                }

                // Need to take care of receive socket as well
                if (socket->recv_socket != NULL){
                    if (socket->recv_socket->eqfd < 0 ){
                        log_info("Ignoring FI_ETIMEDOUT on recv_socket, invalid eqfd (socket already closed)");
                        return;
                    }
                    log_info("Shutting down receive socket on FI_ETIMEDOUT");
                    handle_recv_socket_shutdown(socket->recv_socket);
                    if(socket->recv_socket->lsocket->cb_connection_closed) {
                        socket->recv_socket->lsocket->cb_connection_closed(socket->recv_socket);
                    }
                    if (socket->recv_socket->lsocket->attr.num_buffers > 0){
                        for(int i = 0; i < socket->recv_socket->lsocket->attr.num_buffers; i++){
                            free(socket->recv_socket->recv_buffers[i].data);
                        }
                        free(socket->recv_socket->recv_buffers);
                    }
                }

                if(socket->cqfd < 0){ //cq not initalized yet
                    handle_send_socket_shutdown_on_connection_refused(socket);
                } else {
                    if(socket->cb_internal_connection_closed){
                        socket->cb_internal_connection_closed(socket);
                    }
                    if(socket->cb_connection_closed) {
                        socket->cb_connection_closed(socket);
                    }
                }

                break;

            default:

                log_error("Unhandled error in the Event Queue: %s (code: %d), provider specific: %s (code: %d)",
                        fi_strerror(err_entry.err), err_entry.err,
                        fi_eq_strerror(socket->eq, err_entry.prov_errno, err_entry.err_data, NULL, 0),
                        err_entry.prov_errno);
        }
        return;

    case -FI_EAGAIN:{
        struct fid* fp = &socket->eq->fid;
        fi_trywait(socket->domain->fabric, &fp, 1);
        break;
    }
    default:
        log_error("Unexpected event %d in send socket Event Queue", event);
        exit(2);
    }
}


void
on_listen_socket_libfabric_cm_event(int fd, void* ptr)
{
    struct netio_listen_socket* lsocket = (struct netio_listen_socket*)ptr;
    log_info("listen socket: connection event");
    if(lsocket->eqfd != fd){
        log_info("listen socket is already closed.");
        return;
    }

    struct fi_info *info = NULL;
    struct fi_eq_err_entry err_entry;
    int event = read_cm_event(lsocket->eq, &info, &err_entry);


    switch (event)
    {
        case FI_CONNREQ:
            log_info("fi_verbs_process_listen_socket_cm_event: FI_CONNREQ");
            struct netio_socket_list* new_entry = add_socket(&lsocket->recv_sockets, URECV);
            struct netio_recv_socket* rsocket = (struct netio_recv_socket*)new_entry->socket;
            netio_init_recv_socket(rsocket, lsocket);
            handle_connreq(rsocket, lsocket, info, on_recv_socket_libfabric_cm_event, rsocket);
            if(lsocket->recv_sub_msg == 1){
                rsocket->sub_msg_buffers = malloc(32*sizeof(struct netio_buffer*));
                for (int i = 0; i < 32; i++){
                    rsocket->sub_msg_buffers[i] = malloc(sizeof(struct netio_buffer));
                    rsocket->sub_msg_buffers[i]->size = sizeof(struct netio_subscription_message);
                    rsocket->sub_msg_buffers[i]->data = malloc(rsocket->sub_msg_buffers[i]->size);
                    netio_register_recv_buffer(rsocket, rsocket->sub_msg_buffers[i], 0);
                    netio_post_recv(rsocket, rsocket->sub_msg_buffers[i]);
                }
                log_info("Posted recv for subscription messages");
            } else if (lsocket->attr.num_buffers > 0) {
                log_info("Connection established, posting %lu buffers", lsocket->attr.num_buffers);
                rsocket->sub_msg_buffers = NULL;
                rsocket->recv_buffers = malloc(lsocket->attr.num_buffers * sizeof(struct netio_buffer));
                for(unsigned i=0; i<lsocket->attr.num_buffers; i++) {
                    log_info("registering buffer of size %lu", lsocket->attr.buffer_size);
                    rsocket->recv_buffers[i].size = lsocket->attr.buffer_size;
                    rsocket->recv_buffers[i].data = malloc(lsocket->attr.buffer_size);
                    netio_register_recv_buffer(rsocket, &rsocket->recv_buffers[i], 0);
                    netio_post_recv(rsocket, &rsocket->recv_buffers[i]);
                }
            } else {
                log_error("Something went wrong. Not allocating any buffers for recv socket.");
            }
            break;

        case FI_CONNECTED:
            log_fatal("FI_CONNECTED received on listen socket");
            exit(2);

        case FI_SHUTDOWN:
            log_fatal("FI_SHUTDOWN received on listen socket");
            exit(2);

        case -FI_EAGAIN:{
            struct fid* fp = &lsocket->eq->fid;
            fi_trywait(lsocket->fabric, &fp, 1);
            break;
        }
        case -FI_EAVAIL:
            log_error("Unhandled error in listen socket EQ code: %d, provider specific code: %d",
                err_entry.err, err_entry.prov_errno);
            break;
    }
    fi_freeinfo(info);
}


void
on_buffered_listen_socket_libfabric_cm_event(int fd, void* ptr)
{
    struct netio_buffered_listen_socket* lsocket = (struct netio_buffered_listen_socket*)ptr;
    log_info("buffered listen socket: connection event");
    if(lsocket->listen_socket.eqfd != fd){
        log_info("Buffered listen socket CM event: inconsistend fd. Ignoring event.");
        return;
    }

    struct fi_info *info = NULL;
    struct fi_eq_err_entry err_entry;
    int event = read_cm_event(lsocket->listen_socket.eq, &info, &err_entry);

    switch (event)
    {
        case FI_CONNREQ:
            log_info("FI_CONNREQ");
            struct netio_socket_list* new_entry = add_socket(&(lsocket->listen_socket.recv_sockets), BRECV);
            struct netio_buffered_recv_socket* rsocket = (struct netio_buffered_recv_socket*)new_entry->socket;
            netio_buffered_recv_socket_init(rsocket, lsocket);
            handle_connreq(&rsocket->recv_socket, &lsocket->listen_socket, info, on_buffered_recv_socket_libfabric_cm_event, rsocket);
            log_info("fi_verbs_process_recv_socket_cm_event: FI_CONNECTED, posting %d buffers", rsocket->num_pages);
            for(unsigned int i=0; i<rsocket->num_pages; i++) {
                netio_register_recv_buffer(&rsocket->recv_socket, &rsocket->pages[i], 0);
                netio_post_recv(&rsocket->recv_socket, &rsocket->pages[i]);
            }
            break;

        case FI_CONNECTED:
            log_fatal("FI_CONNECTED received on buffered listen socket");
            exit(2);

        case FI_SHUTDOWN:
            log_fatal("FI_SHUTDOWN received on buffered listen socket");
            exit(2);

        case -FI_EAGAIN:{
            struct fid* fp = &lsocket->listen_socket.eq->fid;
            fi_trywait(lsocket->listen_socket.fabric, &fp, 1);
            break;
        }
        case -FI_EAVAIL:
            log_error("Unhandled error in buffer listen socket EQ code: %d, provider specific code: %d",
                err_entry.err, err_entry.prov_errno);
            break;
    }
    fi_freeinfo(info);
}


void
on_recv_socket_libfabric_cm_event(int fd, void* ptr)
{
    struct netio_recv_socket* socket = (struct netio_recv_socket*)ptr;
    log_info("recv socket %d: connection event", socket->eqfd);
    if(socket->eqfd != fd){
        log_info("Recv socket CM event: inconsistend fd. Ignoring event.");
        return;
    }
    int ret;
    struct fi_eq_err_entry err_entry;
    uint32_t event = read_cm_event(socket->eq, NULL, &err_entry);

    switch (event)
    {
    case FI_CONNECTED:
        log_info("fi_verbs_process_recv_socket_cm_event: FI_CONNECTED");
        socket->cqfd = -1;
        if((ret = fi_control(&socket->cq->fid, FI_GETWAIT, &socket->cqfd)))
        {
            FATAL("Failed to retrieve recv socket Completion Queue wait object", ret);
        }

        socket->cq_ev_ctx.fd = socket->cqfd;
        socket->cq_ev_ctx.data = socket;
        socket->cq_ev_ctx.cb = on_recv_socket_cq_event;

        log_info("Adding recv CQ polled fid %d %p", socket->cqfd, &socket->cq->fid);
        add_polled_fid(&socket->ctx->evloop.pfids, socket->lsocket->fabric, &socket->cq->fid, socket->cqfd, socket, on_recv_socket_cq_event);
        add_open_fd(&socket->ctx->evloop.openfds, socket->cqfd, NETIO_CQ, URECV, socket);
        netio_register_read_fd(&socket->ctx->evloop, &socket->cq_ev_ctx);

        log_info("recv_socket: EQ fd %d CQ fd %d connected", socket->eqfd, socket->cqfd);
        if(socket->lsocket->cb_connection_established) {
            socket->lsocket->cb_connection_established(socket);
        }

        break;

    case FI_SHUTDOWN:
        if (socket->eqfd < 0 ){
            log_info("Ignoring FI_SHUTDOWN on recv_socket, invalid eqfd (socket already closed)");
            return;
        }
        log_info("fi_verbs_process_recv_socket_cm_event: FI_SHUTDOWN");
        handle_recv_socket_shutdown(socket);
        if(socket->lsocket->cb_connection_closed) {
            socket->lsocket->cb_connection_closed(socket);
        }

        if (socket->lsocket->attr.num_buffers > 0){
            for(int i = 0; i < socket->lsocket->attr.num_buffers; i++){
                free(socket->recv_buffers[i].data);
            }
            free(socket->recv_buffers);
        }

        int return_value = remove_socket(&socket->lsocket->recv_sockets, socket);
        log_info("Recv socket removed, result: %d", return_value);
        break;

    case FI_MR_COMPLETE:
    case FI_AV_COMPLETE:
    case FI_JOIN_COMPLETE:
        // Not implemented
        break;

    case -FI_EAGAIN:{
        struct fid* fp = &socket->eq->fid;
        fi_trywait(socket->lsocket->fabric, &fp, 1);
        break;
    }
    case -FI_EAVAIL:
        log_error("Unhandled error in recv socket EQ code: %d, provider specific code: %d",
            err_entry.err, err_entry.prov_errno);
        break;

    default:
        log_error("Unexpected event %d in recv socket Event Queue", event);
        exit(2);
        break;
    }
}


void
on_buffered_recv_socket_libfabric_cm_event(int fd, void* ptr)
{
    struct netio_buffered_recv_socket* socket = (struct netio_buffered_recv_socket*)ptr;
    log_info("buffered recv socket %d: connection event (FD = %d)", socket->recv_socket.eqfd, fd);
    if(socket->recv_socket.eqfd != fd){
        log_info("Buffered recv socket CM event: inconsistend fd. Ignoring event.");
        return;
    }
    int ret;
    struct fi_eq_err_entry err_entry;
    uint32_t event = read_cm_event(socket->recv_socket.eq, NULL, &err_entry);

    switch (event)
    {
    case FI_CONNECTED:
        socket->recv_socket.cqfd = -1;
        if((ret = fi_control(&socket->recv_socket.cq->fid, FI_GETWAIT, &socket->recv_socket.cqfd)))
        {
            FATAL("Failed to retrieve recv socket Completion Queue wait object", ret);
        }

        socket->recv_socket.cq_ev_ctx.fd = socket->recv_socket.cqfd;
        socket->recv_socket.cq_ev_ctx.data = &socket->recv_socket;
        socket->recv_socket.cq_ev_ctx.cb = on_recv_socket_cq_event;

        log_info("Adding BUFFERED RECV CQ polled fid %d %p", socket->recv_socket.cqfd, &socket->recv_socket.cq->fid);
        add_open_fd(&socket->recv_socket.ctx->evloop.openfds, socket->recv_socket.cqfd, NETIO_CQ, BRECV, &socket->recv_socket);
        add_polled_fid(&socket->recv_socket.ctx->evloop.pfids,
                    socket->recv_socket.lsocket->fabric,
                    &socket->recv_socket.cq->fid,
                    socket->recv_socket.cqfd,
                    &socket->recv_socket,
                    on_recv_socket_cq_event);

        netio_register_read_fd(&socket->recv_socket.ctx->evloop, &socket->recv_socket.cq_ev_ctx);
        log_info("buffered_recv_socket: registering CQ fd %d", socket->recv_socket.cqfd);

        if(socket->lsocket->cb_connection_established) {
            socket->lsocket->cb_connection_established(socket);
        }

        break;

    case FI_SHUTDOWN:
        log_info("recv socket shutdown");
        if (socket->recv_socket.eqfd < 0 ){
            log_info("Ignoring FI_SHUTDOWN on buffered recv_socket, invalid eqfd (socket already closed)");
            return;
        }
        handle_recv_socket_shutdown(&socket->recv_socket);
        for(unsigned int i=0; i<socket->num_pages; i++) {
            free(socket->pages[i].data);
        }
        free(socket->pages);
        if(socket->lsocket->cb_connection_closed) {
            socket->lsocket->cb_connection_closed(socket);
        }

        remove_socket(&socket->lsocket->listen_socket.recv_sockets, socket);
        break;

    case FI_MR_COMPLETE:
    case FI_AV_COMPLETE:
    case FI_JOIN_COMPLETE:
        // Not implemented
        break;

    case -FI_EAGAIN:{
        struct fid* fp = &socket->recv_socket.eq->fid;
        fi_trywait(socket->lsocket->listen_socket.fabric, &fp, 1);
        break;
    }
    case -FI_EAVAIL:
        // error was found
        log_error("Unhandled error in buffered recv socket EQ code: %d, provider specific code: %d",
            err_entry.err, err_entry.prov_errno);
        break;

    default:
        log_error("Unexpected event %d in buffered recv socket Event Queue", event);
        exit(2);
        break;
    }
}


// CALLBACKS FOR GARBAGE COLLECTION  //////////////////////////////////////////////////////////
void
close_send_socket(void* ptr)
{
    log_info("Close_send_socket. Not supported anymore");
    struct signal_data* sd = (struct signal_data*)ptr;

    struct netio_send_socket* socket = (struct netio_send_socket*)sd->ptr;
    if(socket->tcp_fi_mode == NETIO_MODE_TCP){
        shutdown(socket->cq_ev_ctx.fd, SHUT_WR);
        netio_signal_close(&socket->ctx->evloop, &socket->tcp_signal);
        epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->eq_ev_ctx.fd, NULL);
        int rc = close(socket->cq_ev_ctx.fd);
        if ( rc ){ log_warn("Cannot close TCP send socket fd %d: %s", socket->cq_ev_ctx.fd, strerror(errno));}
        if ( !rc ){ remove_open_fd(&socket->ctx->evloop, socket->eq_ev_ctx.fd); }
        socket->state=DISCONNECTED;
        if(socket->cb_connection_closed) {
          socket->cb_connection_closed(socket);
        }
    } else if (socket->tcp_fi_mode == NETIO_MODE_LIBFABRIC) {
        //commented out as closing libfabric sockets relies on exhanges of fi_shutdown
        //handle_send_socket_shutdown(socket);
    } else {
        log_error("Closing send socket with unknown netio mode %d", socket->tcp_fi_mode);
    }

    //delete the used signal
    netio_signal_close(sd->evloop, sd->signal);
    free(sd->signal);
    free(sd);
}

void
close_buffered_send_socket(void *ptr)
{
    log_info("Closing buffered_send_socket %p.", ptr);
    struct signal_data* sd = (struct signal_data*)ptr;

    struct netio_buffered_send_socket* socket = (struct netio_buffered_send_socket*)sd->ptr;
    struct netio_send_socket* ssocket = &socket->send_socket;
    if(ssocket->tcp_fi_mode == NETIO_MODE_TCP){
        netio_timer_close(&(ssocket->ctx->evloop), &socket->flush_timer);
        netio_signal_close(&(ssocket->ctx->evloop), &socket->signal_buffer_available);
        shutdown(ssocket->eq_ev_ctx.fd, SHUT_WR);
        netio_signal_close(&ssocket->ctx->evloop, &ssocket->tcp_signal);
        epoll_ctl(ssocket->ctx->evloop.epollfd, EPOLL_CTL_DEL, ssocket->eq_ev_ctx.fd, NULL);
        int rc = close(ssocket->eq_ev_ctx.fd);
        if ( !rc ){ remove_open_fd(&ssocket->ctx->evloop, ssocket->eq_ev_ctx.fd); }
        if(ssocket->cb_connection_closed) {
          ssocket->cb_connection_closed(ssocket);
        }
    } else if(ssocket->tcp_fi_mode == NETIO_MODE_LIBFABRIC){
        //commented out as closing libfabric sockets relies on exhanges of fi_shutdown
        //handle_send_socket_shutdown(ssocket);
    } else {
        log_error("Closing send socket with unkown netio mode %d", ssocket->tcp_fi_mode);
    }
    for(size_t i=0; i < socket->buffers.num_buffers; ++i ){
        free(socket->buffers.buffers[i]);
    }

    //delete the used signal
    netio_signal_close(sd->evloop, sd->signal);
    free(sd->signal);
    free(sd);
}

void
close_recv_socket(void* ptr)
{
    log_info("Closing recv_socket %p.", ptr);
    struct signal_data* sd = (struct signal_data*)ptr;
    struct netio_recv_socket* socket = (struct netio_recv_socket*)sd->ptr;
    if(socket->tcp_fi_mode == NETIO_MODE_TCP){
        shutdown(socket->eq_ev_ctx.fd, SHUT_RD);
        // netio_signal_close(&socket->ctx->evloop, &socket->tcp_signal); // causes bad file descriptor
        epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->eq_ev_ctx.fd, NULL);
        int rc = close(socket->eq_ev_ctx.fd);
        if ( !rc ){ remove_open_fd(&socket->ctx->evloop, socket->eq_ev_ctx.fd); }
        if(socket->lsocket->cb_connection_closed) {
          socket->lsocket->cb_connection_closed(socket);
        }
    } else if(socket->tcp_fi_mode == NETIO_MODE_LIBFABRIC){
        //commented out as closing libfabric sockets relies on exhanges of fi_shutdown
        //handle_recv_socket_shutdown(socket);
    } else {
        log_error("Closing recv socket with unkown netio mode %d", socket->tcp_fi_mode);
    }

    //clean up signal
    netio_signal_close(sd->evloop, sd->signal);
    free(sd->signal);
    free(sd);
}

void
close_buffered_recv_socket(void* ptr)
{
    log_info("Closing buffered_recv_socket %p. Not supported anymore", ptr);
    struct signal_data* sd = (struct signal_data*)ptr;
    /*
    struct netio_buffered_recv_socket* socket = (struct netio_buffered_recv_socket*)sd->ptr;
    struct netio_recv_socket* rsocket = &socket->recv_socket;
    if(rsocket->tcp_fi_mode == NETIO_MODE_TCP){
        shutdown(rsocket->eq_ev_ctx.fd, SHUT_RD);
        netio_signal_close(&rsocket->ctx->evloop, &rsocket->tcp_signal);
        epoll_ctl(rsocket->ctx->evloop.epollfd, EPOLL_CTL_DEL, rsocket->eq_ev_ctx.fd, NULL);
        int rc = close(rsocket->eq_ev_ctx.fd);
        if ( !rc ){ remove_open_fd(&rsocket->ctx->evloop.openfds, rsocket->eq_ev_ctx.fd); }
    } else if (rsocket->tcp_fi_mode == NETIO_MODE_LIBFABRIC){
        handle_recv_socket_shutdown(rsocket);
    } else {
        log_error("Closing buffered recv socket with unkown netio mode %d", rsocket->tcp_fi_mode);
    }
    free(socket->pages);
    */
    //clean up signal
    netio_signal_close(sd->evloop, sd->signal);
    free(sd->signal);
    free(sd);
}


void
close_listen_socket(void* ptr)
{
    log_info("close_listen_socket");
    struct signal_data* sd = (struct signal_data*)ptr;
    struct netio_listen_socket* socket = (struct netio_listen_socket*)sd->ptr;

    if (socket->tcp_fi_mode == NETIO_MODE_LIBFABRIC){
        if(socket->recv_sockets != NULL){
            struct netio_socket_list* entry = socket->recv_sockets;
            int still_open = 0;
            while(entry != NULL){
                struct netio_recv_socket* recv_socket = entry->socket;
                still_open += check_open_fd_exists(&socket->ctx->evloop.openfds, recv_socket->eqfd);
                entry = entry->next;
            }
            if(still_open){
                netio_signal_fire(sd->signal);
                return;
            }
            free_socket_list(&socket->recv_sockets);
        }
        handle_listen_socket_shutdown(socket);
    }
    else if (socket->tcp_fi_mode == NETIO_MODE_TCP){
        if(socket->recv_sockets != NULL){
            struct netio_socket_list* entry = socket->recv_sockets;
            while(entry != NULL){
                struct netio_recv_socket* recv_socket = entry->socket;
                shutdown(recv_socket->eq_ev_ctx.fd, SHUT_RDWR);
                close(recv_socket->eq_ev_ctx.fd);
                remove_open_fd(&socket->ctx->evloop, recv_socket->eq_ev_ctx.fd);
                entry = entry->next;
            }
            free_socket_list(&socket->recv_sockets);
        }
    }
    else {
        log_error("Connection mode unsupported");
    }

    //clean up signal
    netio_signal_close(sd->evloop, sd->signal);
    free(sd->signal);
    free(sd);
}



void
close_buffered_listen_socket(void* ptr)
{
    log_info("close_buffered_listen_socket");
    struct signal_data* sd = (struct signal_data*)ptr;
    struct netio_listen_socket* socket = (struct netio_listen_socket*)sd->ptr;

    if (socket->tcp_fi_mode == NETIO_MODE_LIBFABRIC){
        if(socket->recv_sockets != NULL){
            struct netio_socket_list* entry = socket->recv_sockets;
            int still_open = 0;
            while(entry != NULL){
                struct netio_buffered_recv_socket* recv_socket = entry->socket;
                still_open += check_open_fd_exists(&socket->ctx->evloop.openfds, recv_socket->recv_socket.eqfd);
                entry = entry->next;
            }
            if(still_open){
                netio_signal_fire(sd->signal);
                return;
            }
            free_socket_list(&socket->recv_sockets);
        }
        handle_listen_socket_shutdown(socket);
    }
    else if (socket->tcp_fi_mode == NETIO_MODE_TCP){
        if(socket->recv_sockets != NULL){
            struct netio_socket_list* entry = socket->recv_sockets;
            while(entry != NULL){
                struct netio_buffered_recv_socket* recv_socket = entry->socket;
                shutdown(recv_socket->recv_socket.eqfd, SHUT_RDWR);
                close(recv_socket->recv_socket.eqfd);
                remove_open_fd(&socket->ctx->evloop, recv_socket->recv_socket.eqfd);
                entry = entry->next;
            }
            free_socket_list(&socket->recv_sockets);
        }
    }
    else {
        log_error("Connection mode unsupported");
    }

    //clean up signal
    netio_signal_close(sd->evloop, sd->signal);
    free(sd->signal);
    free(sd);
}


void
handle_listen_socket_shutdown(struct netio_listen_socket* socket)
{
    if(socket->eqfd < 0){return;}//nothing to do
    struct epoll_event ep_event; /* needed only for kernel <2.6.9  */
    log_info("Handle_listen_socket_shutdown. Lsocket EQ: %d with evloop %d socket %p", socket->eqfd, socket->ctx->evloop.epollfd, socket);
    int ret = epoll_ctl(socket->ctx->evloop.epollfd, EPOLL_CTL_DEL, socket->eqfd, &ep_event);
    if(ret){ log_warn("Cannot deregister listen socket EQ %d from evloop %d", socket->eqfd, socket->ctx->evloop.epollfd); }

    if(socket->pep != NULL){
       fi_close(&socket->pep->fid);
       socket->pep = NULL;
    }

    log_info("netio_listen_socket: removing EQ fd %d from evloop %d, ret %d", socket->eqfd, socket->ctx->evloop.epollfd,  ret);
    if(socket->eq && (ret = fi_close(&socket->eq->fid)))
    {
        log_error("Failed to close listen socket %d: %s", ret, fi_strerror(-ret));
        ret = close(socket->eq_ev_ctx.fd);
        if ( ret ) {log_warn("Cannot close listen socket EQ fd %d: %s", socket->eq_ev_ctx.fd, strerror(errno));}
    }
    remove_open_fd(&socket->ctx->evloop, socket->eqfd);
    socket->eqfd = -1;

    if(socket->fi != NULL){
        fi_freeinfo(socket->fi);
        socket->fi = NULL;
    }
    //fi_close(&socket->fabric->fid);
}


//This function is the TCP equivalent of on_send_socket_cm_event()
void
on_send_socket_tcp_cm_event(int fd, void* ptr)
{
  int ret, retVal;
  socklen_t in_len;
  struct netio_send_socket* socket = (struct netio_send_socket*)ptr;

  log_info("connection event called with fd = %d", fd);

  in_len = sizeof(retVal);
  ret = getsockopt(fd, SOL_SOCKET, SO_ERROR, &retVal, &in_len);
  if (ret < 0)
  {
    log_error("on_send_socket_tcp_cm_event: getsockopt failed", ret);
  }

  if (retVal != 0)
  {
    log_error("on_send_socket_tcp_cm_event: connect did not work. retVal = %d, ret = %d, errno = %d", retVal, ret, errno);
  }

  //MJ: The log_error above sometimes returns "retVal = 111, ret = 0, errno = 115". This error indicates that the receiving side temporarily
  //MJ: cannot acknowledge the connection request.
  //MJ: Should we somehow retry the connection request in netio or leave it to the user to redo it. In the latter case we have to give a clear error back to the user
  //MJ: Experience shows that just waiting a few seconds and then retrying actually helps.

  log_info("Register the callback for on_send_completed. Using TCP socket %d", fd);
  socket->cq_ev_ctx.fd   = fd;
  socket->cq_ev_ctx.data = socket;
  socket->cq_ev_ctx.cb   = on_send_socket_tcp_cq_event;
  netio_register_write_tcp_fd(&socket->ctx->evloop, &socket->cq_ev_ctx);

  //MJ: In case of the TCP instruction chain "socket()->connect()->write()/recv()" one is always using the same FD.
  //MJ: therefore we don't know if a connect() or a write()/recv() has completed when we receive a trigger from this FD.
  //MJ: Therefore the code below may not work because cb_connection_established() will also get called on write()/recv()
  socket->state = CONNECTED;

  if(socket->cb_connection_established)
  {
    log_info("Calling the callback that is registered for cb_connection_established");
    socket->cb_connection_established(socket);
  }
}


//This function is the TCP equivalent of on_listen_socket_cm_event()
void
on_listen_socket_tcp_cm_event(int fd, void* ptr)
{
  int infd;
  socklen_t in_len;
  struct sockaddr in_addr;
  log_info("on_listen_socket_tcp_cm_event: called with fd = %d", fd);

  struct netio_listen_socket* lsocket = (struct netio_listen_socket*)ptr;
  in_len = sizeof in_addr;
  infd = accept(fd, &in_addr, &in_len);

  log_info("on_listen_socket_tcp_cm_event: listen socket: infd = %d", infd);
  if (infd == -1) {
    if ((errno == EAGAIN) || (errno == EWOULDBLOCK))      // We have processed all incoming connections.
      log_info("on_listen_socket_tcp_cm_event: nothing to do?");
    else
    {
      log_info("on_listen_socket_tcp_cm_event: ERROR. errno = %d", errno);
      exit(-1);
    }
  }

#if defined DEBUG || defined DEBUG_CM
  char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
  int ret = getnameinfo(&in_addr, in_len, hbuf, sizeof hbuf, sbuf, sizeof sbuf, 0);
  if (ret == 0) {
    log_info("on_listen_socket_tcp_cm_event: Accepted connection on descriptor %d (host=%s, service=%s)", infd, hbuf, sbuf);
  }
#endif

  make_socket_non_blocking(infd);            // Make the incoming socket non-blocking
  log_info("Adding TCP/IP recv socket to listen socket");
  struct netio_socket_list* new_entry = add_socket(&lsocket->recv_sockets, URECV);
  struct netio_recv_socket* rsocket = (struct netio_recv_socket*)new_entry->socket;
  netio_init_recv_tcp_socket(rsocket, lsocket);

  log_info("Adding receive socket %d to epoll", infd);
  rsocket->eq_ev_ctx.fd = infd;
  rsocket->eq_ev_ctx.data = rsocket;
  rsocket->eq_ev_ctx.cb = on_recv_socket_tcp_cm_event;
  netio_register_read_tcp_fd(&rsocket->ctx->evloop, &rsocket->eq_ev_ctx);

  if(lsocket->recv_sub_msg == 1){
    rsocket->sub_msg_buffers = malloc(32*sizeof(struct netio_buffer*));
    for (int i = 0; i < 32; i++){
      rsocket->sub_msg_buffers[i] = malloc(sizeof(struct netio_buffer));
      rsocket->sub_msg_buffers[i]->size = sizeof(struct netio_subscription_message);
      rsocket->sub_msg_buffers[i]->data = malloc(rsocket->sub_msg_buffers[i]->size);
      netio_post_recv(rsocket, rsocket->sub_msg_buffers[i]);
    }
    log_info("Posted recv for subscription messages");
  } else if (lsocket->attr.num_buffers > 0) {
    log_info("Connection established, posting %lu buffers", lsocket->attr.num_buffers);
    rsocket->sub_msg_buffers = NULL;
    rsocket->recv_buffers = malloc(lsocket->attr.num_buffers * sizeof(struct netio_buffer));
    for(unsigned i=0; i<lsocket->attr.num_buffers; i++) {
      log_info("registering buffer of size %lu", lsocket->attr.buffer_size);
      rsocket->recv_buffers[i].size = lsocket->attr.buffer_size;
      rsocket->recv_buffers[i].data = malloc(lsocket->attr.buffer_size);
      netio_post_recv(rsocket, &rsocket->recv_buffers[i]);
    }
  } else {
    log_error("Something went wrong. Not allocating any buffers for recv socket.");
  }

  on_recv_socket_tcp_cm_event(infd, (void*)rsocket);
  log_info("Done. connection accepted");
}


void
on_buffered_recv_socket_tcp_cm_event(int fd, void* ptr)
{
  log_info("Connection event on buffered TCP/IP recv  (FD = %d, ptr = %p)", fd, ptr);
    struct netio_buffered_recv_socket *socket = (struct netio_buffered_recv_socket*)ptr;

  socket->recv_socket.cq_ev_ctx.fd = fd;
  socket->recv_socket.cq_ev_ctx.data = socket;
  socket->recv_socket.cq_ev_ctx.cb = on_recv_socket_tcp_cq_event;
  netio_register_read_tcp_fd(&socket->recv_socket.ctx->evloop, &socket->recv_socket.cq_ev_ctx);

  log_info("posting %d buffers", socket->num_pages);
  for(unsigned int i=0; i<socket->num_pages; i++) {
     netio_post_recv(&socket->recv_socket, &socket->pages[i]);
  }

  if(socket->lsocket->cb_connection_established) {
    log_info("Calling user callback");
    socket->lsocket->cb_connection_established(socket);
  } else{
    log_warn("no callback available");
  }
}


//This function is the TCP equivalent of on_buffered_listen_socket_cm_event()
void
on_buffered_listen_socket_tcp_cm_event(int fd, void* ptr)
{
  int infd;
  socklen_t in_len;
  struct sockaddr in_addr;
  log_info("on_buffered_listen_socket_tcp_cm_event: called with fd = %d", fd);

  struct netio_buffered_listen_socket* lsocket = (struct netio_buffered_listen_socket*)ptr;
  in_len = sizeof in_addr;
  infd = accept(fd, &in_addr, &in_len);

  log_info("on_buffered_listen_socket_tcp_cm_event: listen socket: infd = %d", infd);
  if (infd == -1)
  {
    if ((errno == EAGAIN) || (errno == EWOULDBLOCK))      // We have processed all incoming connections.
      log_info("on_buffered_listen_socket_tcp_cm_event: nothing to do?");
    else
    {
      log_info("on_buffered_listen_socket_tcp_cm_event: ERROR. errno = %d", errno);
      exit(-1);
    }
  }

#if defined DEBUG || defined DEBUG_CM
  char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
  int ret = getnameinfo(&in_addr, in_len, hbuf, sizeof hbuf, sbuf, sizeof sbuf, 0);
  if (ret == 0) {
    log_info("on_buffered_listen_socket_tcp_cm_event: Accepted connection on descriptor %d (host=%s, service=%s)", infd, hbuf, sbuf);
  }
#endif

  make_socket_non_blocking(infd);            // Make the incoming socket non-blocking
  log_info("Adding TCP/IP recv socket to listen socket");
  struct netio_socket_list* new_entry = add_socket(&(lsocket->listen_socket.recv_sockets), BRECV);
  struct netio_buffered_recv_socket* rsocket = (struct netio_buffered_recv_socket*)new_entry->socket;
  netio_buffered_recv_tcp_socket_init(rsocket, lsocket);

  log_info("Adding receive socket %d to epoll", infd);
  rsocket->recv_socket.eq_ev_ctx.fd = infd;
  rsocket->recv_socket.eq_ev_ctx.data = rsocket;
  rsocket->recv_socket.eq_ev_ctx.cb = on_buffered_recv_socket_tcp_cm_event;
  netio_register_read_tcp_fd(&rsocket->recv_socket.ctx->evloop, &rsocket->recv_socket.eq_ev_ctx);

  for(unsigned int i=0; i<rsocket->num_pages; i++) {
    netio_post_recv(&rsocket->recv_socket, &rsocket->pages[i]);
  }

  on_buffered_recv_socket_tcp_cm_event(infd, (void*)rsocket);
  log_info("Connection accepted");
}


//This is the equivalent of on_recv_socket_cm_event
void
on_recv_socket_tcp_cm_event(int fd, void *ptr)
{
  log_info("on_recv_socket_tcp_cm_event: called with fd = %d and ptr = %p", fd, ptr);
  struct netio_recv_socket *socket = (struct netio_recv_socket*)ptr;
  socket->cq_ev_ctx.fd = fd;
  socket->cq_ev_ctx.data = socket;
  socket->cq_ev_ctx.cb = on_recv_socket_tcp_cq_event;
  netio_register_read_tcp_fd(&socket->ctx->evloop, &socket->cq_ev_ctx);
  if(socket->lsocket->cb_connection_established)
  {
    log_info("Calling user callback");
    socket->lsocket->cb_connection_established(socket);
  }  else {
    log_warn("no callback available");
  }
}
