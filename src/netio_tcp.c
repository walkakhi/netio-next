#include <unistd.h>
#include <stdio.h>
#include <sys/uio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <netinet/tcp.h>
#include <assert.h>

#include "connection_event.h"
#include "log.h"
#include "netio/netio.h"
#include "netio/netio_tcp.h"

#if defined DEBUG || defined DEBUG_TCP
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define log_dbg(...) log_log(LOG_DEBUG, __FILENAME__, __LINE__, __VA_ARGS__)
#define log_trc(...) log_log(LOG_TRACE, __FILENAME__, __LINE__, __VA_ARGS__)
#else
#define log_dbg(...)
#define log_trc(...)
#endif


int sigpipe_trapped=0;

// STATIC FUNCTIONS
static void _connect_tcp(struct netio_send_socket* socket, const char* hostname, unsigned port, void* addr, size_t addrlen);
void make_socket_non_blocking(int sfd);


//Parameters:
//- socket is still empty
//- hostname and port have valid values
//- addr and addrlen are empty
static void
_connect_tcp(struct netio_send_socket* niosocket, const char* hostname, unsigned port, void* addr, size_t addrlen)
{
  int ret, sockfd;
  char ip[100];
  struct addrinfo hints, *servinfo, *p;
  struct sockaddr_in servaddr;

  log_info("_connect_tcp called");

  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    log_fatal("Problem in creating the socket %d", errno);
    exit(2);
  }

  log_info("sockfd = %d", sockfd);

  if (isalpha(hostname[0]))
  {
     //Convert hostname to IP
     memset(&hints, 0, sizeof hints);
     hints.ai_family   = AF_UNSPEC; // use AF_INET6 to force IPv6
     hints.ai_socktype = SOCK_STREAM;

     int rv = getaddrinfo(hostname, "http", &hints, &servinfo);
     if (rv != 0)
     {
        log_fatal("getaddrinfo failed. getaddrinfo: %s", gai_strerror(rv));
        exit(2);
     }

     for(p = servinfo; p != NULL; p = p->ai_next)
     {
        struct sockaddr_in* h = (struct sockaddr_in *) p->ai_addr;
        strcpy(ip, inet_ntoa(h->sin_addr));
     }

     freeaddrinfo(servinfo);
     // End of hostname to IP conversion
  }
  else
  {
     strncpy(ip, hostname, sizeof(ip));
     ip[sizeof(ip)-1]=0;
  }
  log_info("%s resolved to %s\n" , hostname , ip);

  //Creation of the socket
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  ret = inet_pton(AF_INET, ip, &servaddr.sin_addr);  //IPv4
  if (ret < 1)
  {
    log_error("Problem with inet_pton. ret = %d\n", ret);
  }
  servaddr.sin_port = htons(port);       //convert to big-endian order

  make_socket_non_blocking(sockfd);

  /* int opt=1; */
  /* ret = setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(opt)); */
  /* if (ret != 0) */
  /* { */
  /*   log_error("Problem setting TCP_NODELAY, %s", strerror(errno)); */
  /* } */
  //#define CORK
#ifdef CORK
      int opt=1;
      ret = setsockopt(sockfd, IPPROTO_TCP, TCP_CORK, &opt, sizeof(opt));
      if (ret != 0)
        {
          log_error("Problem setting TCP_CORK, %s", strerror(errno));
        }
#endif

  //Connection of the client to the socket
  ret = connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
  if (ret == 0)
  {
    log_info("Already connected. Is this a problem for eopll?");
  }
  else
  {
    log_info("ret = %d, %s", ret, strerror(errno));

    if (errno == EINPROGRESS)
    {
      log_info("connect is EINPROGRESS. Let epoll handle it.");
    }
    else
    {
      log_error("Problem in connecting to the server. ret = %d", ret);
      log_error("TCP connect errno = %d (%s)\n", errno, strerror(errno));
      return;
    }
  }

  log_info("connect OK. Calling netio_register_write_tcp_fd");
  niosocket->eq_ev_ctx.fd   = sockfd;
  niosocket->eq_ev_ctx.data = niosocket;    //name "niosocket" used to avoid clash with the posix function "socket" (the compiler did not like it)
  niosocket->eq_ev_ctx.cb   = on_send_socket_tcp_cm_event;
  add_open_fd(&niosocket->ctx->evloop.openfds, sockfd, NETIO_TCP, USEND, niosocket);
  netio_register_write_tcp_fd(&niosocket->ctx->evloop, &niosocket->eq_ev_ctx);
  log_info("_connect_tcp done");
}


void
make_socket_non_blocking(int sfd)
{
  int flags, s;

  flags = fcntl(sfd, F_GETFL, 0);
  if (flags == -1)
  {
    log_error("fcntl cannot get fd");
  }

  flags |= O_NONBLOCK;
  s = fcntl(sfd, F_SETFL, flags);
  if (s == -1)
  {
    log_error("fcntl cannot set O_NONBLOCK");
  }
}


void
netio_init_send_tcp_socket(struct netio_send_socket *socket, struct netio_context *ctx)
{
  log_info("called with &ctx->evloop = %p", &ctx->evloop);

  memset(socket, 0, sizeof(*socket));
  socket->ctx                    = ctx;
  socket->tcp_fi_mode            = NETIO_MODE_TCP;
  socket->message_request_header = NULL;
  socket->state                  = UNCONNECTED;                         //MJ: relevant for TCP?

  netio_signal_init(&ctx->evloop, &socket->tcp_signal);

  //socket->tcp_signal.cb   = netio_tcp_send_on_signal_test;
  socket->tcp_signal.cb   = netio_tcp_send_on_signal;
  socket->tcp_signal.data = socket;
  /* log_debug("socket is at = %p", socket); */

  if (sigpipe_trapped==0)
  {
    struct sigaction action;
    action.sa_handler = SIG_IGN;
    int ret = sigaction(SIGPIPE, &action, NULL);
    if (ret)
    {
      perror ("sigaction");
    }
  }
  log_info("netio_init_send_tcp_socket done.");
}


void
netio_tcp_send_on_signal_test(void *ptr)
{
  log_error("testtesttesttest.");
  log_fatal("testtesttesttest.");
}


void
netio_init_listen_tcp_socket(struct netio_listen_socket* socket, struct netio_context* ctx, struct netio_unbuffered_socket_attr* attr)
{
  log_info("netio_init_listen_tcp_socket called");
  memset(socket, 0, sizeof(*socket));
  socket->ctx = ctx;
  socket->recv_sockets = NULL;
  socket->tcp_fi_mode = NETIO_MODE_TCP;
  if (attr == NULL){
    socket->attr.buffer_size = 0;
    socket->attr.num_buffers = 0;
  } else {
    socket->attr = *attr;
  }
  log_info("netio_init_listen_tcp_socket done");
}


void
netio_init_recv_tcp_socket(struct netio_recv_socket* socket, struct netio_listen_socket* lsocket)
{
  log_info("netio_init_recv_tcp_socket called");
  memset(socket, 0, sizeof(*socket));
  socket->ctx                    = lsocket->ctx;
  socket->lsocket                = lsocket;
  socket->message_request_header = NULL;
  socket->tcp_fi_mode            = NETIO_MODE_TCP;

  netio_signal_init(&socket->ctx->evloop, &socket->tcp_signal);
  socket->tcp_signal.cb   = netio_tcp_recv_on_signal;
  socket->tcp_signal.data = socket;
  log_info("socket is at = %p", socket);
  log_info("netio_init_recv_tcp_socket done");
}


void
netio_connect_tcp(struct netio_send_socket* socket, const char* hostname, unsigned port)
{
  log_info("connect_tcp with hostname = %s and port = %d", hostname, port);
  _connect_tcp(socket, hostname, port, NULL, 0);
  log_info("netio_connect_tcp done");
}


void
netio_listen_tcp(struct netio_listen_socket* niosocket, const char* hostname, unsigned port)
{
  int ret, listenfd;
  struct sockaddr_in servaddr;

  log_info("TCP/IP listening on %s:%d", hostname, port);

  if ((listenfd = socket (AF_INET, SOCK_STREAM, 0)) < 0)    //Create a socket
  {
    log_fatal("Problem in creating the socket, errno = %d", errno);
    exit(2);
  }

  const int opt=1;
  ret = setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
  if (ret)
  {
    perror("setsockopt");
  }

  char* _hostname = netio_domain_name_lookup(hostname);

  //preparation of the socket address
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(_hostname);
  servaddr.sin_port        = htons(port);

  free(_hostname);
  log_info("SERV_PORT           = %d", port);
  log_info("htons(SERV_PORT)    = %d", htons(port));
  log_info("hostname            = %s", hostname);
  log_info("inet_addr(hostname) = %d", inet_addr(hostname));
  log_info("servaddr.sin_addr.s_addr = %d", servaddr.sin_addr.s_addr);

  ret=bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));  //bind the socket
  if (ret && errno != EINPROGRESS)
  {
    perror("bind");
  }
  socklen_t addrlen=sizeof(servaddr);
  ret=getsockname(listenfd, &servaddr, &addrlen);
  if (ret) {
     perror("getsockname");
  }
  niosocket->port=ntohs(servaddr.sin_port);

  make_socket_non_blocking(listenfd);                               //Make the socket non-blocking

  #define LISTENQ   8                                               //maximum number of client connections. MJ: review
  listen(listenfd, LISTENQ);                                        //listen to the socket by creating a connection queue, then wait for clients
  log_info("Server running...waiting for connections.");

  niosocket->eq_ev_ctx.fd   = listenfd;
  niosocket->eq_ev_ctx.data = niosocket;                            //name "niosocket" used to avoid clash with the posix function "socket" (the compiler did not like it)
  niosocket->eq_ev_ctx.cb   = on_listen_socket_tcp_cm_event;
  niosocket->tcp_fi_mode = NETIO_MODE_TCP;

  add_open_fd(&niosocket->ctx->evloop.openfds, listenfd, NETIO_TCP, ULISTEN, niosocket);
  netio_register_read_tcp_fd(&niosocket->ctx->evloop, &niosocket->eq_ev_ctx);
  log_info("netio_listen_tcp done");
}


int
netio_tcp_send_imm(struct netio_send_socket* socket,
                   struct netio_buffer* buf, void* addr, size_t size, uint64_t key, uint64_t imm){

  uint32_t total_size=size;
  struct iovec msg_iov[2];
  msg_iov[0].iov_base=&total_size;
  msg_iov[0].iov_len=sizeof(uint32_t);
  msg_iov[1].iov_base=addr;
  msg_iov[1].iov_len=size;
  struct msghdr hdr={0,0,&msg_iov[0],2,0,0};
  int status=sendmsg(socket->eq_ev_ctx.fd,&hdr,0);
  if (status==total_size+sizeof(uint32_t)) {
    /* int opt=1; */
    /* if (setsockopt(socket->eq_ev_ctx.fd, IPPROTO_TCP, TCP_NODELAY, */
    /*                &opt, sizeof(opt)) != 0) { */
    /*   log_error("Problem setting TCP_NODELAY, %s", strerror(errno)); */
    /* } */
    if (socket->cb_send_completed != 0) {
      socket->cb_send_completed(socket, key);
    }
    log_info("netio_tcp_send_imm done");
    //printf("Sent %d bytes\n",status);
    return NETIO_STATUS_OK;
  }
  else if (status==-1) {
    if (errno!=EAGAIN && errno!=EWOULDBLOCK) {
      perror("Error from sendmsg: ");
      exit(1);
    }
  }
  else {
    printf("Error only sent %d bytes of message\n",status);
    exit(1);
  }

  char* mem=malloc(sizeof(struct netio_tcp_send_item)+
                   total_size+sizeof(uint32_t));
  if(mem == NULL)
  {
    log_fatal("cannot allocate memory for descriptor");
    exit(2);
  }

  printf("Queueing send for later\n");
  // Couldn't send immediately so add to queue for event loop
  struct netio_tcp_send_item* mrd = (struct netio_tcp_send_item *) mem;
  mrd->element_active = NETIO_TCP_NEW;
  mrd->socket         = socket;
  mrd->buffer         = buf;
  mrd->bytes_left     = sizeof(int);
  mrd->total_bytes    = total_size;
  mrd->key            = key;
  mrd->next_element   = NULL;

  //Append the descriptor to the list
  if(socket->message_request_header == NULL)
  {
    socket->message_request_header = (void *)mrd;
    log_info("List was empty. Descriptor linked to head of list");
  }
  else
  {
    struct netio_tcp_send_item* mrdq = (struct netio_tcp_send_item *)socket->message_request_header;
    while (mrdq->next_element != NULL)
    {
      mrdq = (struct netio_tcp_send_item *)mrdq->next_element;
    }
    mrdq->next_element = (void *)mrd;
  }

  log_info("Calling netio_signal_fire for signal at %p", &socket->tcp_signal);
  netio_signal_fire(&socket->tcp_signal);

  log_info("netio_tcp_send_imm done");
  return(NETIO_STATUS_OK);
}

int
netio_tcp_sendv_imm(struct netio_send_socket* socket,
                    uint32_t total_size,
                    struct iovec* iov,
                    size_t count,
                    uint64_t key,
                    uint64_t imm)
{
  // static int immediates=0;
  // static int partials=0;
  static int totalpackets=0;
  totalpackets++;

  // if (totalpackets%10000==0) printf("total packets %d, fully sent immediately %d partialy sent %d\n",totalpackets,immediates,partials);

  int bytes_sent=0;
  int send_size=total_size+sizeof(uint32_t);
  if(socket->message_request_header == NULL)
  {
    /* No message in progress, try sending immediately */
    struct iovec* msg_iov=(struct iovec*)malloc((count+1)*sizeof(struct iovec));
    msg_iov[0].iov_base=&total_size;
    msg_iov[0].iov_len=sizeof(uint32_t);
    for (int entry=0;entry<count;entry++) {
      msg_iov[entry+1].iov_base=iov[entry].iov_base;
      msg_iov[entry+1].iov_len=iov[entry].iov_len;
    }
    struct msghdr hdr={0,0,&msg_iov[0],count+1,0,0};
    bytes_sent=sendmsg(socket->eq_ev_ctx.fd,&hdr,0);
    free(msg_iov);
    if (bytes_sent==send_size) {
      // immediates++;
      /* int opt=1; */
      /* if (setsockopt(socket->eq_ev_ctx.fd, IPPROTO_TCP, TCP_NODELAY, */
      /*                &opt, sizeof(opt)) != 0) { */
      /*   log_error("Problem setting TCP_NODELAY, %s", strerror(errno)); */
      /* } */
      if (socket->cb_send_completed != 0) {
        socket->cb_send_completed(socket, key);
      }
      //printf("Sent %d bytes\n",bytes_sent);
      return NETIO_STATUS_OK;
    }
    else if (bytes_sent==-1) {
      if (errno!=EAGAIN && errno!=EWOULDBLOCK) {
        perror("Error from sendmsg: ");
        exit(1);
      }
    }
    // partials++;
  }

  /* ------------------------------------------------------------------ */
  /*  OK, we didn't manage to send the complete message, queue it for
      later send via the event loop */
  /* ------------------------------------------------------------------ */
  char* mem=malloc(sizeof(struct netio_tcp_send_item)+
                   sizeof(struct netio_buffer)+sizeof(uint32_t)+total_size+sizeof(uint32_t));
  if(mem == NULL)
  {
    log_fatal("cannot allocate memory for descriptor");
    exit(2);
  }

  struct netio_tcp_send_item* mrd = (struct netio_tcp_send_item *) mem;
  mrd->bytes_left=send_size-bytes_sent;
  //printf("bytes_sent=%d, bytes_left=%d\n",bytes_sent,mrd->bytes_left);
  mrd->element_active = NETIO_TCP_IN_PROGRESS;
  mrd->socket         = socket;
  mrd->buffer         = (struct netio_buffer*) &mem[sizeof(struct netio_tcp_send_item)];
  mrd->buffer->data   = (char*) &mrd->buffer[1];
  mrd->buffer->size   = send_size;
  mrd->total_bytes    = total_size;
  mrd->key            = key;
  mrd->next_element   = NULL;

  // Put length in front of data to send as one
  uint32_t* tlen_ptr=(uint32_t*) &mrd->buffer[1];
  *tlen_ptr=total_size;
  int offset=sizeof(uint32_t);
  for (int seg=0; seg<count; seg++)
  {
    memcpy((char*)mrd->buffer->data+offset, iov[seg].iov_base, iov[seg].iov_len);
    offset += iov[seg].iov_len;
  }
  //    log_debug("send descriptor allocated and initialized");

  //Append the descriptor to the list
  if(socket->message_request_header == NULL)
  {
    socket->message_request_header = (void *)mrd;
    //log_info("List was empty. Descriptor linked to head of list");
  }
  else
  {
    struct netio_tcp_send_item* mrdq = (struct netio_tcp_send_item *)socket->message_request_header;
    while (mrdq->next_element != NULL)
    {
      mrdq = (struct netio_tcp_send_item *)mrdq->next_element;
    }
    mrdq->next_element = (void *)mrd;
  }

  //log_info("Calling netio_signal_fire for signal at %p", &socket->tcp_signal);
  netio_signal_fire(&socket->tcp_signal);

  //log_info("netio_tcp_sendv_imm done");
  return(NETIO_STATUS_OK);
}


void
netio_tcp_send_on_signal(void *ptr)
{
  int goto_next_request, current_offset, n;
  void *next_mrd;
  struct netio_tcp_send_item *mrd;
  struct netio_send_socket *socket;

  //log_info("netio_tcp_send_on_signal called with ptr = %p", ptr);
  socket = (struct netio_send_socket *) ptr;
  goto_next_request = 1;

  while(goto_next_request)
  {
    //Take the first message request descriptor from the head of the list
    if (socket->message_request_header == NULL)
    {
      //log_debug("No message request pending. So, why are you here?");
      int retVal;
      socklen_t in_len = sizeof(retVal);
      int ret = getsockopt(socket->eq_ev_ctx.fd, SOL_SOCKET, SO_ERROR,
                           &retVal, &in_len);
      if (ret==0 && retVal !=0) {
        log_info("%s -- calling connection closed callback", strerror(retVal));
        if (socket->cb_connection_closed != NULL) {
          socket->cb_connection_closed(socket);
        }
      } else {
        // When a stream socket peer has performed an orderly shutdown, the return value will be 0
        if(!recv(socket->eq_ev_ctx.fd, NULL, 1, 0)) {
          log_info("0 bytes read from fd %d, calling cb_connection_closed", socket->eq_ev_ctx.fd);
          if (socket->cb_connection_closed != NULL) {
            socket->cb_connection_closed(socket);
          }
        }
      }
      break;
    }
    else
    {
      //Get access to the message request at the head of the list
      mrd = socket->message_request_header;
      /* log_info("element_active = %d", mrd->element_active); */
      /* log_info("bytes_left     = %d", mrd->bytes_left); */
      /* log_info("next_element   = %p", mrd->next_element); */
      /* log_info("buffer         = %p", mrd->buffer); */
      /* log_info("socket         = %p", mrd->socket); */
      /* log_info("mrd            = %p", mrd); */
    }

    if(mrd->element_active == NETIO_TCP_EMPTY)
    {
      log_fatal("netio_tcp_send_on_signal: element is not active");
      exit(2);
    }

    void* data_addr  = mrd->buffer->data;
    int size_total = (int)mrd->buffer->size;
    log_info("Marker = 0x%08x, size_total = %d, size_remaining = %d", *((int*)data_addr), size_total, mrd->bytes_left);
    int sockfd=mrd->socket->eq_ev_ctx.fd;
    if(mrd->element_active == NETIO_TCP_NEW)    //Send the message header (= message size)
    {
      log_info("start of new message of size %d", size_total);
      int nsent=sizeof(int)-mrd->bytes_left;
      unsigned char* s_ptr=(unsigned char*)&mrd->buffer->size;
      n = send(sockfd, &s_ptr[nsent], mrd->bytes_left, 0);
      if(n < 0)
      {
        if (errno == EWOULDBLOCK || errno == EAGAIN)
        {
          log_info("Too busy....................................... ");
        }
        else if (errno==ECONNRESET || errno==EBADF || errno==EPIPE)
        {
          log_info("socket fd %d %s",sockfd,
                    errno==ECONNRESET ? "Connection reset by peer" : "no longer open.");

          log_info("Calling connection closed callback");
          if (mrd->socket->cb_connection_closed != NULL)
          {
            mrd->socket->cb_connection_closed(mrd->socket);
          }
          mrd->socket->state=DISCONNECTED;
          /* Free all mrd entries associated with this socket */
          mrd->socket->message_request_header = NULL;
          while (mrd)
          {
            next_mrd = mrd->next_element;                                               //MJ: Do we need a mutex to protect concurrent access to the next_element pointer?
            free(mrd);
            mrd=next_mrd;
          }
        }
        else
        {
          log_warn("Error %d from send, giving up on sending header", errno);
        }
        return;
      }
      if(n != 4)
      {
        log_info("only sent %d bytes of length (%d)",n,mrd->buffer->size);
      }
      mrd->bytes_left-=n;
      if (mrd->bytes_left==0)
      {
        mrd->bytes_left=mrd->total_bytes;
        mrd->element_active = NETIO_TCP_IN_PROGRESS;  //header sent
      }
      else
      {
        return;
      }
    }

    if(mrd->element_active == NETIO_TCP_IN_PROGRESS)    //if we did not manage to send the header (EAGAIN) we must not send data!!
    {
      current_offset = size_total - mrd->bytes_left;
      log_info("send size = %d, current_offset = %d, fd = %d", mrd->bytes_left, current_offset, mrd->socket->eq_ev_ctx.fd);

      n = send(sockfd, (char*)data_addr + current_offset, mrd->bytes_left, 0);
      if(n < 0)
      {
        if (errno == EWOULDBLOCK || errno == EAGAIN)
        {
          /* log_debug("Too busy.........................................."); */
        }
        else if (errno==ECONNRESET || errno==EBADF || errno==EPIPE)
        {
          log_info("socket fd %d %s",sockfd,
                    errno==ECONNRESET ? "Connection reset by peer" : "no longer open.");

          log_info("Calling connection closed callback");
          if (mrd->socket->cb_connection_closed != NULL)
          {
            mrd->socket->cb_connection_closed(mrd->socket);
          }
          mrd->socket->message_request_header = NULL;
          mrd->socket->state=DISCONNECTED;
          /* Free all mrd entries associated with this socket */
          while (mrd)
          {
            next_mrd = mrd->next_element;                                               //MJ: Do we need a mutex to protect concurrent access to the next_element pointer?
            free(mrd);
            mrd=next_mrd;
          }
        }
        else
        {
          log_warn("send() failed errno = %d", errno);
        }
        return;
      }
/* #ifdef CORK */
/*       int opt=0; */
/*       int ret = setsockopt(sockfd, IPPROTO_TCP, TCP_CORK, &opt, sizeof(opt)); */
/*       if (ret != 0) */
/*         { */
/*           log_error("Problem clearing TCP_CORK, %s", strerror(errno)); */
/*         } */
/* #endif */
      mrd->bytes_left = mrd->bytes_left - n;
      if(mrd->bytes_left)
      {
        log_info("d of %d bytes sent (remaining = %d)", n, mrd->bytes_left + n, mrd->bytes_left);
        return;
      }
      else
      {
        log_info("Remaining %d bytes sent. Item done", n);
        if(mrd->socket->cb_send_completed)   //Call the user callback
        {
          /* log_debug("calling user callback"); */
          mrd->socket->cb_send_completed(mrd->socket, mrd->key);
        }
        else
        {
          //MJ Is this an error?
          //            log_warn("No user callback registered");
        }
      }
    }

    //Remove the message request descriptor from the list
    next_mrd = mrd->next_element;                                               //MJ: Do we need a mutex to protect concurrent access to the next_element pointer?
    /* log_debug("Processing next request at address = %p", next_mrd); */

    //Unlink the message request from the head of the list
    if(next_mrd != NULL)
    {
      mrd->socket->message_request_header = next_mrd;
      /* log_debug("Oldest request unlinked from head of list. The next younger request is at address %p", next_mrd); */
    }
    else
    {
      mrd->socket->message_request_header = NULL;
      goto_next_request = 0 ;
      /* log_debug("Request unlinked from head of list. List is now empty"); */
    }
    mrd->buffer->to_send = 0; // set buffer free to use
    free(mrd);                              //Release the memory held by the message request
    /* log_debug("Memory of mrd at %p freed", mrd); */
  }
}


void
netio_tcp_recv_on_signal(void *ptr)
{
  int goto_next_request, n, rest_size;
  void *next_mrd;
  struct netio_tcp_recv_item *mrd;
  struct netio_recv_socket *socket;
  struct netio_listen_socket *lsocket;

  log_info("netio_tcp_recv_on_signal called with ptr = %p", ptr);
  socket = (struct netio_recv_socket *) ptr;
  lsocket = socket->lsocket;
  lsocket->tcp_fi_mode = NETIO_MODE_TCP;  //??? should already be set!

  goto_next_request = 1;
  while(goto_next_request)
  {
    //Take the first message request descriptor from the head of the list
    if (socket->message_request_header == NULL)
    {
      log_warn("No message request pending. So, why are you here?");
      return;
    }

    //Get access to the message request at the head of the list
    mrd = socket->message_request_header;
    log_info("element_active = %d", mrd->element_active);
    log_info("bytes_received = %d", mrd->bytes_received);
    log_info("next_element   = %p", mrd->next_element);
    log_info("buffer         = %p", mrd->buffer);
    log_info("socket         = %p", mrd->socket);
    log_info("mrd            = %p", mrd);
    log_info("fd             = %d", mrd->socket->eq_ev_ctx.fd);
    log_info("msg size       = %d", mrd->message_size);


    if(mrd->element_active == NETIO_TCP_EMPTY)
    {
      log_fatal("element is not active");
      exit(2);
    }

    void* data_addr = mrd->buffer->data;
    int size = (int)mrd->buffer->size;
    /*log_debug("buffer->size = %d", size);*/
    //Receive the message header (= message size)
    if(mrd->element_active == NETIO_TCP_NEW)  //header not yet received
    {
      unsigned char* s_ptr=(unsigned char*)&mrd->message_size;
      n = recv(mrd->socket->eq_ev_ctx.fd, &s_ptr[mrd->bytes_received],
               sizeof(int)-mrd->bytes_received, 0);
      if (n == 0) {
        log_info("0 bytes read from fd %d, closing socket",mrd->socket->eq_ev_ctx.fd);
        netio_close_socket(&socket->ctx->evloop,socket,URECV);
      }
      if (n == -1)
      {
        if (errno == EWOULDBLOCK || errno == EAGAIN)
        {
          /* log_debug("Nothing to read yet"); */
        }
        else if (errno==ECONNRESET || errno==EBADF || errno==EPIPE)
        {
          log_info("socket no longer open.");
        }
        else
        {
          log_warn("send() failed errno = %d", errno);
        }
        return;
      }
      mrd->bytes_received+=n;
      if (mrd->bytes_received==4)
      {
        mrd->bytes_received = 0;
        mrd->element_active = NETIO_TCP_IN_PROGRESS;
        /* log_debug("Header received. n = %d, Message size = %d bytes", n, size_total); */
        if (mrd->message_size > size)
        {
          log_error("Message size = %d bytes larger than buffer size = %d bytes. Truncating.", mrd->message_size, size);
          mrd->message_size = size;
        }
      }
      else
      {
        return;
      }
    }

    if(mrd->element_active == NETIO_TCP_IN_PROGRESS)
    {
      //Receive message data
      rest_size = mrd->message_size - mrd->bytes_received;
      log_info("rest_size = %d", rest_size);

      while(rest_size)
      {
        n = recv(mrd->socket->eq_ev_ctx.fd, (char*)data_addr + mrd->bytes_received, rest_size, 0);
        if(n < 0)
        {
          if (errno == EWOULDBLOCK || errno == EAGAIN)
          {
            log_info("Out of data to read for now, returning to eventloop");
            return;
          }
          else if (errno==ECONNRESET || errno==EBADF || errno==EPIPE)
          {
            log_info("socket no longer open.");

            /* Clear what we've read??? */
            mrd->element_active = NETIO_TCP_NEW;
          }
          else
          {
            log_warn("recv() failed errno = %d", errno);
          }
          return;
        }

        /* iptr = (int *)(data_addr + mrd->bytes_received); */
        /* nn = n >> 2; */
        /* if (nn > 4) */
        /* { */
        /*   log_debug("word 0 = 0x%08x", iptr[0]); */
        /*   //DEBUG_LOG("netio_tcp_recv_on_signal (s=%d/m=%d): word 1 = 0x%08x", socket_loop, message_loop, iptr[1]); */
        /*   //DEBUG_LOG("netio_tcp_recv_on_signal (s=%d/m=%d): word %d = 0x%08x", socket_loop, message_loop, nn - 2, iptr[nn - 2]); */
        /*   //DEBUG_LOG("netio_tcp_recv_on_signal (s=%d/m=%d): word %d = 0x%08x", socket_loop, message_loop, nn - 1, iptr[nn - 1]); */
        /* } */

        rest_size = rest_size - n;
        mrd->bytes_received = mrd->bytes_received + n;
        if(rest_size)
        {
          log_info("%d of %d bytes received (remaining = %d)", n, mrd->message_size, rest_size);
        }
        else
        {
          log_info("remaining %d bytes received. Item done", n);
        }
      }

      //Call the user callback
      if(lsocket->cb_msg_received)
      {
        lsocket->cb_msg_received(mrd->socket, mrd->buffer, data_addr, mrd->message_size);
      } else {
        log_info("No user callback registered");
      }

      //Remove the message request descriptor from the list
      next_mrd = mrd->next_element;                                              //MJ: Do we need a mutex to protect concurrent access to the next_element pointer?
      log_info("Processing next request at address = %p", next_mrd);

      //Unlink the message request from the head of the list
      if(next_mrd != NULL)
      {
        mrd->socket->message_request_header = next_mrd;
        log_info("Oldest request unlinked from head of list. The next younger request is at address %p", next_mrd);
        // netio_signal_fire(&socket->tcp_signal);
        goto_next_request = 0 ;
      } else {
        mrd->socket->message_request_header = NULL;
        goto_next_request = 0 ;
        log_info("Request unlinked from head of list. List is now empty");
      }

      free(mrd);                              //Release the memory held by the message request
    }
  }
}
